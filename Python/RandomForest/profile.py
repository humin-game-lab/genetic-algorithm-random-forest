import time
import os
import psutil


def elapsed_since(start):
    return time.perf_counter() - start


def get_process_memory():
    process = psutil.Process(os.getpid())
    mem_info = process.memory_info()
    return mem_info.rss


def profile(func):
    def wrapper(*args, **kwargs):
        mem_before = get_process_memory()
        start = time.perf_counter()
        result = func(*args, **kwargs)
        elapsed_time = elapsed_since(start)
        mem_after = get_process_memory()
        # print("{}: memory before: {:,}, after: {:,}, consumed: {:,}; exec time: {}".format(
        print("{}, {}, {}, {}, {}".format(
            func.__name__,
            mem_before, mem_after, mem_after - mem_before,
            elapsed_time))
        return result

    return wrapper