import sys
import time

import pandas as pd
import numpy as np
import random
import math
import collections
from sklearn import metrics
from joblib import Parallel, delayed
from sklearn.utils import shuffle
import threading
import multiprocessing as mp
import gc

class Tree(object):
    def __init__(self):
        self.split_feature = None
        self.split_value = None
        self.leaf_value = None
        self.tree_left = None
        self.tree_right = None

    def calc_predict_value(self, dataset):
        if self.leaf_value is not None:
            return self.leaf_value
        elif dataset[self.split_feature] <= self.split_value:
            if self.tree_left is not None:
                return self.tree_left.calc_predict_value(dataset)
            else:
                return -1
        elif dataset[self.split_feature] > self.split_value:
            if self.tree_right is not None:
                return self.tree_right.calc_predict_value(dataset)
            else:
                return 1
        else:
            print("exception")
            return 0

    def describe_tree(self):
        if not self.tree_left and not self.tree_right:
            leaf_info = '{"leaf_value:"' + ':"' + str(self.leaf_value) + '"' + '}'
            return leaf_info
        left_info = self.tree_left.describe_tree()
        right_info = self.tree_right.describe_tree()
        tree_structure = '{"split_feature":' + '"' + str(self.split_feature) + '"' + \
                         ',"split_value":' + '"' + str(self.split_value) + '"' + \
                         ',"left_tree":' + left_info + \
                         ',"right_tree":' + right_info + "}"
        return tree_structure


class RandomForestClassifier(object):
    def __init__(self, n_estimators=10, max_depth=-1, min_samples_split=2, min_samples_leaf=1,
                 min_split_gain=0.0, colsample_bytree=None, subsample=0.8, random_state=None):

        self.n_estimators = n_estimators
        self.max_depth = max_depth if max_depth != -1 else float('inf')
        self.min_samples_split = min_samples_split
        self.min_samples_leaf = min_samples_leaf
        self.min_split_gain = min_split_gain
        self.colsample_bytree = colsample_bytree
        self.subsample = subsample
        self.random_state = random_state
        self.trees = None
        self.feature_importances_ = dict()

        self.best_trees = None
        self.worst_trees = None
        self.categories = None

    def fit(self, dataset, targets):
        # assert targets.unique().__len__() == 3, "There must be two class for targets!"
        self.categories = targets.unique()
        targets = targets.to_frame(name='label')

        if self.random_state:
            random.seed(self.random_state)
        random_state_stages = random.sample(range(self.n_estimators), self.n_estimators)

        if self.colsample_bytree == "sqrt":
            self.colsample_bytree = int(len(dataset.columns) ** 0.5)
        elif self.colsample_bytree == "log2":
            self.colsample_bytree = int(math.log(len(dataset.columns)))
        else:
            self.colsample_bytree = len(dataset.columns)

        # Building Trees in Parallel
        self.trees = Parallel(n_jobs=-1, verbose=0, backend="threading")(
            delayed(self._parallel_build_trees)(dataset, targets, random_state)
            for random_state in random_state_stages)

    def _parallel_build_trees(self, dataset, targets, random_state):
        """bootstrap to build up the decision tree"""
        subcol_index = random.sample(dataset.columns.tolist(), self.colsample_bytree)
        dataset_stage = dataset.sample(n=int(self.subsample * len(dataset)), replace=True,
                                       random_state=random_state).reset_index(drop=True)
        dataset_stage = dataset_stage.loc[:, subcol_index]
        targets_stage = targets.sample(n=int(self.subsample * len(dataset)), replace=True,
                                       random_state=random_state).reset_index(drop=True)

        tree = self._build_single_tree(dataset_stage, targets_stage, depth=0)
        # print(tree.describe_tree())
        return tree

    def _build_single_tree(self, dataset, targets, depth):
        if len(targets['label'].unique()) <= 1 or dataset.__len__() <= self.min_samples_split:
            tree = Tree()
            tree.leaf_value = self.calc_leaf_value(targets['label'])
            return tree

        if depth < self.max_depth:
            best_split_feature, best_split_value, best_split_gain = self.choose_best_feature(dataset, targets)
            left_dataset, right_dataset, left_targets, right_targets = \
                self.split_dataset(dataset, targets, best_split_feature, best_split_value)

            tree = Tree()
            if left_dataset.__len__() <= self.min_samples_leaf or \
                    right_dataset.__len__() <= self.min_samples_leaf or \
                    best_split_gain <= self.min_split_gain:
                tree.leaf_value = self.calc_leaf_value(targets['label'])
                return tree
            else:
                self.feature_importances_[best_split_feature] = \
                    self.feature_importances_.get(best_split_feature, 0) + 1

                tree.split_feature = best_split_feature
                tree.split_value = best_split_value
                tree.tree_left = self._build_single_tree(left_dataset, left_targets, depth + 1)
                tree.tree_right = self._build_single_tree(right_dataset, right_targets, depth + 1)
                return tree
        else:
            tree = Tree()
            tree.leaf_value = self.calc_leaf_value(targets['label'])
            return tree

    def choose_best_feature(self, dataset, targets):
        best_split_gain = 1
        best_split_feature = None
        best_split_value = None

        for feature in dataset.columns:
            if dataset[feature].unique().__len__() <= 100:
                unique_values = sorted(dataset[feature].unique().tolist())

            else:
                unique_values = np.unique([np.percentile(dataset[feature], x)
                                           for x in np.linspace(0, 100, 100)])


            for split_value in unique_values:
                left_targets = targets[dataset[feature] <= split_value]
                right_targets = targets[dataset[feature] > split_value]
                split_gain = self.calc_gini(left_targets['label'], right_targets['label'])

                if split_gain < best_split_gain:
                    best_split_feature = feature
                    best_split_value = split_value
                    best_split_gain = split_gain
        return best_split_feature, best_split_value, best_split_gain

    @staticmethod
    def calc_leaf_value(targets):

        label_counts = collections.Counter(targets)
        major_label = max(zip(label_counts.values(), label_counts.keys()))
        return major_label[1]

    @staticmethod
    def calc_gini(left_targets, right_targets):

        split_gain = 0
        for targets in [left_targets, right_targets]:
            gini = 1
            label_counts = collections.Counter(targets)
            for key in label_counts:
                prob = label_counts[key] * 1.0 / len(targets)
                gini -= prob ** 2
            split_gain += len(targets) * 1.0 / (len(left_targets) + len(right_targets)) * gini
        return split_gain

    @staticmethod
    def split_dataset(dataset, targets, split_feature, split_value):

        left_dataset = dataset[dataset[split_feature] <= split_value]
        left_targets = targets[dataset[split_feature] <= split_value]
        right_dataset = dataset[dataset[split_feature] > split_value]
        right_targets = targets[dataset[split_feature] > split_value]
        return left_dataset, right_dataset, left_targets, right_targets

    def predict(self, dataset):
        res = []
        for _, row in dataset.iterrows():
            pred_list = []
            for tree in self.trees:
                # print("---------------------------------------------------------------------")
                # print("Current Tree: ")
                # print(tree.describe_tree())

                result = tree.calc_predict_value(row)
                if result == -1 or result == 1:
                    result = random.choice(self.categories)
                    print("Invalid Tree: Random Select Prediction as ", result)
                    pred_list.append(random.choice(result))
                else:
                    pred_list.append(result)

            pred_label_counts = collections.Counter(pred_list)
            pred_label = max(zip(pred_label_counts.values(), pred_label_counts.keys()))
            res.append(pred_label[1])
        return np.array(res)

accuracies, forest = [], []

def Run(fileName, features, target_feature, targets, id):
    df = pd.read_csv(fileName)
    df = df[df[target_feature].isin(targets)].sample(frac=1, random_state=66).reset_index(drop=True)
    df = shuffle(df)
    train_count = int(0.8 * len(df))
    X_train = df.loc[:train_count, features]
    y_train = df.loc[:train_count, target_feature]
    X_test = df.loc[train_count:, features]
    y_test = df.loc[train_count:, target_feature]
    clf = RandomForestClassifier(n_estimators=10,
                                            max_depth=10,
                                            min_samples_split=6,
                                            min_samples_leaf=2,
                                            min_split_gain=0.0,
                                            colsample_bytree="sqrt",
                                            subsample=0.8,
                                            random_state=66)

    clf.fit(X_train, y_train)
    result = clf.predict(X_test)
    accuracy = metrics.accuracy_score(y_test, result)
    # accuracies.append(accuracy)
    # forest.append(clf)
    print("Forest: ",id, accuracy)
    return accuracy


results = []
def collect_result(result):
    global results
    results.append(result)


def Multithread_Run(fileName, features, target_feature, targets, loop, processors):
    # accuracies, forests = [], []
    processors = min(processors, mp.cpu_count())
    pool = mp.Pool(processors)
    for i in range(0, loop):
        pool.apply_async(Run, args=(fileName, features, target_feature, targets, i))
        print("Multithreaded finish building forest :", i)
    pool.close()
    pool.join()
    print("Multithreaded: ", iteration, " Forest")


def Loop_Run(fileName, features, target_feature, targets, loop):
    # accuracies, forest = [], []
    for i in range(loop):
        Run(fileName, features, target_feature, targets, accuracies)
    print("Single Threaded: ", loop," Accuracy:", sum(accuracies) / len(accuracies))


def singleThreadedRun(fileName_iris, feature_list_iris, target_iris, targets_iris, iteration):
    for i in range(0, iteration):
        Run(fileName_iris, feature_list_iris, target_iris, targets_iris, i)
        print("Single Threaded finish building forest :", i)
    print("Single Threaded: ", iteration, " Forest")

def Run5(fileName, features, target_feature, targets, iteration):
    accuracy = []
    forest = []
    pool = mp.Pool(processes=4)
    for i in range(0, iteration):
        pool.apply_async(Run, args=(fileName, features, target_feature, targets, accuracy, forest))
    pool.close()
    pool.join()

if __name__ == '__main__':
    fileName_bc = "source/breastCancer.csv"
    fileName_iris = "source/iris.csv"
    filename_abalone = "source/abalone.csv"

    feature_list_bc = ["id", "radius_mean", "texture_mean", "perimeter_mean", "area_mean", "smoothness_mean",
                       "compactness_mean", "concavity_mean", "concave points_mean", "symmetry_mean",
                       "fractal_dimension_mean", "radius_se", "texture_se", "perimeter_se", "area_se", "smoothness_se",
                       "compactness_se", "concavity_se", "concave points_se", "symmetry_se", "fractal_dimension_se",
                       "radius_worst", "texture_worst", "perimeter_worst", "area_worst", "smoothness_worst",
                       "compactness_worst", "concavity_worst", "concave points_worst", "symmetry_worst",
                       "fractal_dimension_worst"]
    feature_list_iris = ["sepal.length", "sepal.width", "petal.length", "petal.width"]
    feature_list_abalone = ["Length", "Diameter", "Height", "Whole weight", "Shucked weight", "Viscera weight",
                            "Shell weight", "Rings"]

    targets_bc = ['B', 'M']
    targets_iris = ['Setosa', 'Versicolor', 'Virginica']
    targets_abalone = ['M', 'F']

    target_bc = "Class"
    target_iris = "Class"
    target_abalone = 'Sex'

    print(f"Arguments count: {len(sys.argv)}")
    arg = sys.argv
    if arg[1] == "S":
        iteration = int(arg[2])
        print("Running Single threaded")
        print("Number of Forest:", iteration)
        singleThreadedRun(fileName_iris, feature_list_iris, target_iris, targets_iris,iteration)
    elif arg[1] == "M":
        iteration = int(arg[2])
        processoer = int(arg[3])
        print("Running Multi-threaded")
        print("Number of Forest:", iteration)
        print("Number of Threads:",processoer)
        Multithread_Run(fileName_iris, feature_list_iris, target_iris, targets_iris, iteration, processoer)
    elif arg[1] == "L":
        iteration = int(arg[2])
        print("Running Single threaded")
        print("Number of Forest:", iteration)
        Loop_Run(fileName_iris, feature_list_iris, target_iris, targets_iris,iteration)
    else:
        print("Invalid")