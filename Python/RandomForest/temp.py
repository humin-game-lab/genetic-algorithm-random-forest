def function1(currentBalance):
    currentBalance = currentBalance - 10
    print("currentBalance = ", currentBalance)
    return currentBalance


def function2(currentBalance):
    currentBalance = currentBalance * 10
    print("currentBalance = ", currentBalance)
    return currentBalance

def function3():
    print("Hey here is function3")
def function4():
    print("Hey here is function4")

currentBalance = 100
while True:

    print("type 1 to function-1")
    print("type 2 to function-2")
    print("type 3 to function-3")
    print("type 4 to function-4")
    print("type -1 to exit")

    val = input("Enter your value: ")
    if val == '1':
        currentBalance = function1(currentBalance)
    elif val == '2':
        currentBalance = function2(currentBalance)
    elif val == '3':
        function3()
    elif val == '4':
        function4()
    elif val == '-1':
        print("Thank you for running my code, BYE!")
        exit()
    else:
        print("Invalid input, please try again! ")

