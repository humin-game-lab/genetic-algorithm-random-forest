class Map {
  constructor() {
    this.store = {};
  }

  set(key, value) {
    // add or update key,value to linked list
    this.store[key] = value;
  }

  get(key) {
    if (key in this.store) {
      return this.store[key];
    }
    return null;
  }

  has(key) {
    if (key in this.store) {
      return true;
    }
    return false;
  }

  remove(key) {
    if (key in this.store) {
      delete this.store[key];
    }
    // Note :-  here removing will remove all values which is not a good solution
    // we will have to use double linked list in order to remove a value from linked list
    // I am not doing here, to make program simple
  }
}


class OrderedMap {
  constructor() {
    this.store = {};
  }

  set(key, value) {
    if (!(key in this.store)) {
      this.store[key] = [];
    }
    // add or update key,value to linked list
    this.store[key].push(value);
  }

  get(key) {
    if (key in this.store) {
      return this.store[key];
    }
    return null;
  }

  has(key) {
    if (key in this.store) {
      return true;
    }
    return false;
  }

  remove(key) {
    if (key in this.store) {
      delete this.store[key];
    }
    // Note :-  here removing will remove all values which is not a good solution
    // we will have to use double linked list in order to remove a value from linked list
    // I am not doing here, to make program simple
  }

  print(key) {
    const result = [];
    if (key in this.store) {
      const row = this.store[key];
      for (let i = 0; i < row.length; i++) {
        result.push({ from_epoch: row[i][0], performance: row[i][1] });
      }
      console.log(key, result);
    } else {
      console.log(
        "Tree is not in cache: No GA operator performed on the tree."
      );
    }
  }

  print_by_key(key) {
    console.log("id: ", key);
    console.log(this.store[key]);
    console.log("");
  }

  get_models_by_key(key) {
    const result = [];
    if (key in this.store) {
      const row = this.store[key];
      for (let i = 0; i < row.length; i++) {
        result.push(row[i][2]);
      }
    }
    return result;
  }

  get_nodes_by_key(key) {
    const result = [];
    if (key in this.store) {
      const row = this.store[key];
      for (let i = 0; i < row.length; i++) {
        result.push(row[i]);
      }
    }
    return result;
  }

  get_time() {
    const date_ob = new Date();
    const date = ("0" + date_ob.getDate()).slice(-2);
    const month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
    const hours = date_ob.getHours();
    const minutes = date_ob.getMinutes();
    const seconds = date_ob.getSeconds();
    // current date & time in YYYY-MM-DD hh:mm:ss format
    return String(
      month + "-" + date + "_" + hours + ":" + minutes + ":" + seconds
    );
  }

  write_log_to_file(output_path, filename) {
    console.log("Try writing log to ", output_path + "/" + filename + ".txt");
    let text = "";
    const time_stamp = this.get_time();
    text += String("Run time log from " + time_stamp + "\n\n");
    const keys = Object.keys(this.store);
    // console.log(keys);
    for (let i = 0; i < keys.length; i++) {
      const key = keys[i];
      text += String("Tree ID:" + String(key) + "\n");
      const row = this.store[key];
      // console.log(row.length)
      for (let j = 0; j < row.length; j++) {
        // console.log(j, row[j]);
        text += JSON.stringify(row[j]) + "\n";
      }
      text += "\n";
    }
    Deno.writeTextFileSync(output_path + "/" + filename + ".txt", text);
  }
}

export { OrderedMap, Map };
