

const RF_CONFIG = {
    feature_names: [],
    n_estimators: 50,
    max_features: "sqrt",
    max_depth: 20,
    min_sample_split: 2,
    max_bootstrap_samples: 0.7,
    bootstrap_with_replacement: true,
};

const GA_CONFIG = {
    config_description: "",
    selection_rate: 0.2,
    crossover_rate: 0.2,
    mutation_rate: 0.1,
    diversity_percentage: 0.1,
    crossover_type: "SINGLE",
    max_generation: 100,
    termination_condition: "NONE",
    feature_names: [],

    debug_config: {
        output_directory: "./output/",
        output_folder: "",
        timestamp_start: null,
        timestamp_end: null,
        memory_stamp: [],
    },
};


export { RF_CONFIG, GA_CONFIG };

