import "https://deno.land/x/lodash@4.17.19/vendor/underscore/underscore.js";
import { ArrayHelper } from "../util/array_helper.js";
import { Evaluation } from "./evaluation.js";
/*
FastDecisionTree:
  fit(): Train the decision tree model
  build_tree(): Build the decision tree
  get_split(): Select the best split point for a dataset
  test_split(): Split a dataset based on an attribute and an attribute value
  split(): Create child splits for a node or make terminal
  gini_index(): Calculate the Gini index for a split dataset
  to_terminal(): Create a terminal node value
  predict(): Make prediction given an array of testing data
  _predict(): Serve as private function for predict(): Make a prediction with a decision tree
*/

class FastDecisionTree {
  constructor(max_depth = 20, min_sample_split = 2) {
    this.root = null;
    this.training_data = [];
    this.features_names = [];
    this.feature_indexes = [];
    this.max_depth = max_depth;
    this.min_sample_split = min_sample_split;
    this.helper = new ArrayHelper();
  }

  parse(rawTreeData) {
    this.RF_CONFIG = rawTreeData.RF_CONFIG;
    this.root = JSON.parse(JSON.stringify(rawTreeData.root));
    this.features_names = rawTreeData.features_names;
    this.feature_indexes = rawTreeData.feature_indexes;
    this.max_depth = rawTreeData.max_depth;
    this.min_sample_split = rawTreeData.min_sample_split;
    this.max_bootsrap_samples = rawTreeData.max_bootsrap_samples;
    // this.training_data = rawTreeData.training_data;
  }

  // fit: Train the decision tree model
  fit(training, feature_names, feature_indexes) {
    this.training_data = training;
    this.features_names = feature_names;
    this.feature_indexes = feature_indexes;
    this.build_tree();
  }

  // build_tree: Build the decision tree
  build_tree() {
    const root = this.get_split(this.training_data);
    this.split(root, 1);
    this.root = root;
  }


  // get_split: Select the best split point for a dataset
  get_split(dataset) {
    // getting the class label
    const target_index = dataset[0].length - 1;
    const target_column = this.helper.getColumn(dataset, target_index);
    const class_values = _.uniq(target_column);

    let [b_index, b_value, b_score, b_groups] = [99999, 99999, 99999, []];

    for (let index = 0; index < dataset[0].length - 1; index++) {
      for (let i = 0; i < dataset.length; i++) {
        const row = dataset[i];
        const groups = this.test_split(index, row[index], dataset);
        const gini = this.gini_index(groups, class_values);
        if (gini < b_score) {
          [b_index, b_value, b_score, b_groups] = [
            this.feature_indexes[index],
            row[index],
            gini,
            groups,
          ];
        }
      }
    }
    return { index: b_index, value: b_value, gini: b_score, groups: b_groups };
  }

  // test_split: Split a dataset based on an attribute and an attribute value
  test_split(index, value, dataset) {
    const left = [];
    const right = [];
    for (let i = 0; i < dataset.length; i++) {
      let row = dataset[i];
      if (row[index] < value) {
        left.push(row);
      } else {
        right.push(row);
      }
    }
    return [left, right];
  }

  // split: Create child splits for a node or make terminal
  split(node, depth) {
    const [left, right] = [node["groups"][0], node["groups"][1]];
    // manage memory
    const newGroups = [];
    const newGroups_info = [];
    for (let i = 0; i < node["groups"].length; i++) {
      const group_length = node["groups"][i].length;
      newGroups.push(group_length);
      if (group_length > 0) {
        const target_column = this.getColumn(node["groups"][i]);
        const group_info = this.frequencyNumber(target_column);
        newGroups_info.push(group_info);
      } else {
        newGroups_info.push([]);
      }

    }
    delete node["groups"];
    node["groups"] = newGroups;
    node["groups_info"] = newGroups_info;

    // check for a no split
    if (left.length === 0 || right.length === 0) {
      const newArray = left.concat(right);
      node["left"] = node["right"] = this.to_terminal(newArray);
      return;
    }

    if (node["gini"] === 0) {
      node["left"] = this.to_terminal(left);
      node["right"] = this.to_terminal(right);
      return;
    }

    // check for max depth
    if (depth >= this.max_depth) {
      node["left"] = this.to_terminal(left);
      node["right"] = this.to_terminal(right);
      return;
    }

    // process left child
    if (left.length <= this.min_sample_split) {
      node["left"] = this.to_terminal(left);
    } else {
      node["left"] = this.get_split(left);
      this.split(node["left"], depth + 1);
    }

    // process right child
    if (right.length <= this.min_sample_split) {
      node["right"] = this.to_terminal(right);
    } else {
      node["right"] = this.get_split(right);
      this.split(node["right"], depth + 1);
    }
  }

  // gini_index: Calculate the Gini index for a split dataset
  gini_index(groups, classes) {
    // count all samples at a split point
    let n_instances = 0;
    for (let i = 0; i < groups.length; i++) {
      n_instances += groups[i].length;
    }

    let gini = 0.0;
    for (let i = 0; i < groups.length; i++) {
      const group = groups[i];
      const size = group.length;
      // error checking to avoid 0
      if (size === 0) {
        continue;
      }

      let score = 0.0;
      for (let j = 0; j < classes.length; j++) {
        const class_val = classes[j];
        let p = 0;
        let counter = 0;
        for (let k = 0; k < group.length; k++) {
          const row = group[k];
          if (row[row.length - 1] === class_val) {
            counter += 1;
          }
        }
        p = counter / size;
        score += p * p;
      }
      gini += (1.0 - score) * (size / n_instances);
    }
    return gini;
  }

  // to_terminal: Create a terminal node value
  to_terminal(group) {

    const outcomes = this.getColumn(group)
    return _.chain(outcomes).countBy().pairs().max(_.last).head().value();
  }

  // predict: Make a prediction with a decision tree
  predict(testings, node = this.root) {
    const predictions = [];
    for (const row of testings) {
      const prediction = this._predict(node, row);
      predictions.push(prediction);
    }
    return predictions;
  }

  predict_single_row(row, node = this.root) {
    const testing_row = JSON.parse(JSON.stringify(row));
    const prediction = this._predict(node, testing_row);
    return prediction;
  }


  // _predict: Make a prediction with a decision tree
  _predict(node, row) {
    if (row[node["index"]] < node["value"]) {
      if (typeof node["left"] === "object") {
        return this._predict(node["left"], row);
      } else {
        return node["left"];
      }
    } else {
      if (typeof node["right"] === "object") {
        return this._predict(node["right"], row);
      } else {
        return node["right"];
      }
    }
  }

  get_performance(x_test, y_test) {
    const y_predict = this.predict(x_test);
    const evaluation = new Evaluation();
    return evaluation.accuracy_metric(y_predict, y_test);
  }

  // using iterative inorder traversal to find all the non-leaf nodes
  find_available_nodes() {
    const non_leaf_nodes = [];
    const queue = [];
    queue.push(this.root);
    while (queue.length > 0) {
      const node = queue.pop();
      if (node !== this.root) {
        non_leaf_nodes.push(node);
      }
      if (typeof node["left"] == "object") {
        queue.push(node["left"]);
      }

      if (typeof node["right"] == "object") {
        queue.push(node["right"]);
      }
    }
    return non_leaf_nodes;
  }

  find_and_replace_node(node, old_node, new_node, verbose = false) {
    if (typeof node == "object") {
      if (_.isEqual(node["left"], old_node)) {
        node["left"] = new_node;
        return;
      }

      if (_.isEqual(node["right"], old_node)) {
        node["right"] = new_node;
        return;
      }
      this.find_and_replace_node(node["left"], old_node, new_node, verbose);
      this.find_and_replace_node(node["right"], old_node, new_node, verbose);
    }
    return;
  }


  getColumn(rows, index = rows[0].length - 1) {
    const arrayColumn = (arr, n) => arr.map((x) => x[n]);
    return arrayColumn(rows, index);
  }

  // reference: https://www.geeksforgeeks.org/counting-frequencies-of-array-elements/
  frequencyNumber(arr) {
    // Creating a HashMap containing integer
    // as a key and occurrences as a value
    let size = arr.length;
    let freqMap = new Map();
    for (let i = 0; i < size; i++) {
      if (freqMap.has(arr[i])) {
        // If number is present in freqMap,
        // incrementing it's count by 1
        freqMap.set(arr[i], freqMap.get(arr[i]) + 1);
      }
      else {
        // If integer is not present in freqMap,
        // putting this integer to freqMap with 1 as it's value
        freqMap.set(arr[i], 1);
      }
    }

    // Printing the freqMap
    let result = [];
    for (let [key, value] of freqMap.entries()) {
      result.push(String(key + ":" + value));
    }
    return result;
  }

}

export { FastDecisionTree };
