import * as parseCSV from "https://deno.land/std@0.82.0/encoding/csv.ts";
import * as denoforge from "https://deno.land/x/denoforge@1.0.0/denoforge.js";

class DataFrame {
  constructor(filePath = null, featureName = null, ignoredColumn = []) {
    this.filePath = filePath;
    this.rawData = [];
    this.dataframe = [];
    this.featureNames = [];
    this.targetName = featureName;
    this.ignoredColumns = ignoredColumn;
  }

  async preprocess() {
    await this.read(this.filePath);
  }

  moveTargetColumnToLast() {
    const targetColumn = this.dataframe.getSeries(this.targetName);
    this.dataframe = this.dataframe.dropSeries(this.targetName);
    this.dataframe = this.dataframe.withSeries(this.targetName, targetColumn);
    this.featureNames = this.dataframe.getColumnNames();
  }

  dropColumns() {
    const newdf = this.dataframe.dropSeries(this.ignoredColumns);
    this.dataframe = newdf;
    this.featureNames = newdf.getColumnNames();
  }

  countFromColumn(column) {
    const all_values = [];
    for (const val of column) {
      all_values.push(val.play);
    }
    console.log(all_values);
    const map = all_values.reduce(
      (acc, e) => acc.set(e, (acc.get(e) || 0) + 1),
      new Map()
    );
    return [...map.entries()];
  }

  async read(filename) {
    const content = await parseCSV.parse(
      Deno.readTextFileSync(filename, "utf-8"),
      {
        skipFirstRow: true,
      }
    );

    this.rawData = content;
    this.dataframe = new denoforge.DataFrame(this.rawData);
    this.featureNames = this.dataframe.getColumnNames();
    this.dropColumns();
    this.moveTargetColumnToLast();
  }

  TrainTestSplitCSV(data, training_percentage, testing_percentage) {
    const train_count = parseInt(training_percentage * data.length);
    const testing_count = parseInt(testing_percentage * data.length);

    const X_train = [];
    const y_train = [];
    const X_test = [];
    const y_test = [];

    X_train = data.slice(0, train_count);
    // X_train.dropColumns(["Play"])
    for (let i = 0; i < X_train.length; i++) {
      y_train.push(X_train[i].play);
    }

    X_test = data.slice(train_count, data.length);
    // X_test.dropColumns(["Play"])
    for (let i = 0; i < X_test.length; i++) {
      y_test.push(X_test[i].play);
    }

    console.log("Total data size: ", data.length);
    console.log("training size: ", train_count);
    console.log("testing size: ", testing_count);
    return [X_train, y_train, X_test, y_test];
  }

  TrainTestSplitDataFrame(
    dataframe,
    training_percentage,
    testing_percentage,
    target_name
  ) {
    const train_count = Math.ceil(training_percentage * dataframe.count());
    const testing_count = Math.floor(testing_percentage * dataframe.count());

    const X_train = dataframe.between(0, train_count);
    const X_test = dataframe.between(train_count, dataframe.count());

    X_train = X_train.dropSeries(target_name);
    X_test = X_test.dropSeries(target_name);
    const target_column = dataframe.getSeries(target_name);

    // error checking to make sure that the total number of training and testing items does not exceed the total row in the dataframe
    if (train_count + testing_count > dataframe.count()) {
      throw "TrainTestSplitDataFrame: Training and Testing objects exceed the total number of rows!";
    }

    const y_train = target_column.between(0, train_count);
    const y_test = target_column.between(train_count, dataframe.count());

    console.log("Total data size: ", dataframe.count());
    console.log("training size: ", train_count);
    console.log("testing size: ", testing_count);

    return [X_train, y_train, X_test, y_test];
  }

  ShuffleDataFrame(dataframe) {
    console.log("Shuffling the dataframe");
    const arrayOfArrays = dataframe.toRows();
    arrayOfArrays.sort(() => Math.random() - 0.5);
    const newdataframe = new denoforge.DataFrame(arrayOfArrays);
    return newdataframe;
  }
}

export { DataFrame };
