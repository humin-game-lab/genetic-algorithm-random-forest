import {
  attribute,
  Digraph,
  Node,
  Edge,
  renderDot,
} from "https://deno.land/x/graphviz@v0.2.1/mod.ts";

import {
  ensureDirSync,
  existsSync,
} from "https://deno.land/std@0.128.0/fs/mod.ts";

// import { red, green, gray, blue, bold, dim, bgYellow, bgBlack, bgWhite } from "https://deno.land/std/fmt/colors.ts";


class TreeNode extends Node {
  constructor(id, label) {
    super(id, {
      [attribute.label]: label,
      [attribute.shape]: "box",
      [attribute.style]: "filled",
      [attribute.colorscheme]: "pubu9",
      [attribute.color]: 5
    });
  }
}

class TreeLeaf extends Node {
  constructor(id, label) {
    super(`${id}`, {
      [attribute.label]: label,
      [attribute.style]: "filled",
      [attribute.colorscheme]: "pubu9",
      [attribute.color]: 1
    });
  }
}

class TreeEdge extends Edge {
  constructor(targets, label) {
    super(targets, {
      [attribute.label]: label,
      [attribute.colorscheme]: "pubu9",
      [attribute.color]: 10,
      [attribute.penwidth]: 2
    });
  }
}

class TreeDigraph extends Digraph {
  constructor(label = "sample") {
    super("G", {
      [attribute.label]: label,
      // [attribute.colorscheme]: "rdbu10",
      // [attribute.color]: 1
    });
  }
}

class Parser {
  constructor(feature_names = [], output_path = "./output/") {
    this.feature_names = feature_names;
    this.path = output_path;
    this.root = null;
    this.digraph = null;
  }

  parse(decision_tree, feature_names, graph_name) {
    this.clear();
    this.root = decision_tree.root;

    if (this.feature_names.length === 0) {
      this.feature_names = feature_names;
    }

    this.digraph = new TreeDigraph(graph_name)
    this.process(this.root, 0);
  }


  process(node, node_number, previous_node = null, edge_label = "") {
    if (typeof node != "object") {
      // reach a decision node
      const decision_node = new TreeLeaf(String(node_number), node);
      this.digraph.addNode(decision_node);

      if (previous_node != null) {
        const edge = new TreeEdge([previous_node, decision_node], edge_label);
        this.digraph.addEdge(edge);
      }

    } else {
      const feature = this.feature_names[node.index];
      const gini = node.gini;
      const data_visited = node.groups[0] + node.groups[1];
      const samples_info = node.groups_info;
      let label = "";
      if ("eval" in node) {
        const test_visited = node.eval;
        label = String(feature + "\ngini = " + gini + "\nsamples = " + data_visited + "\nleft =[" + samples_info[0] + "] right=[" + samples_info[1] + "]" + "\ntest_visited =" + JSON.stringify(test_visited));
      } else {
        label = String(feature + "\ngini = " + gini + "\nsamples = " + data_visited + "\nleft =[" + samples_info[0] + "] right=[" + samples_info[1] + "]");
      }
      const cur_node = new TreeNode(String(node_number), label);
      this.digraph.addNode(cur_node);


      if (previous_node != null) {
        const edge = new TreeEdge([previous_node, cur_node], edge_label);
        this.digraph.addEdge(edge);
      }

      const seed1 = Math.random();
      const seed2 = Math.random();

      const left_edge_label = "<" + node.value + "\nsamples=" + node.groups[0];
      const right_edge_label = ">=" + node.value + "\nsamples=" + node.groups[1];

      this.process(node.left, String(node_number + seed1), cur_node, left_edge_label);
      this.process(node.right, String(node_number + seed2), cur_node, right_edge_label);
    }
  }

  create_and_assign_output_folder(folder_name) {
    const pathFound = existsSync(this.path + folder_name);
    if (pathFound) {
      console.log(folder_name, " already exists in", this.output_directory);
      return false; // no need to anything
    }
    console.log("Creating ", this.path + folder_name);
    ensureDirSync(this.path + folder_name);
    this.path = this.path + folder_name + "/";
    return true;
  }

  clear() {
    this.digraph = new TreeDigraph();
    this.root = null;
  }

  get_output_path() {
    return this.path;
  }

  async output(file_name) {
    await renderDot(this.digraph, this.path + file_name + ".png", {
      format: "png",
    });
    console.log("Parser: Parsing tree to:", (this.path + file_name + ".png"));
  }
}

export { Parser };


