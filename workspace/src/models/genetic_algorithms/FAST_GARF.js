import "https://deno.land/x/lodash@4.17.19/vendor/underscore/underscore.js";
import { FastDecisionTree } from "../fast_decision_tree.js";
import { FastRandomForest } from "../fast_random_forest.js";
import { ArrayHelper } from "../../util/array_helper.js";
import { Evaluation } from "../evaluation.js";
import { OrderedMap } from "../../util/ordered_map.js";
import * as log from "https://deno.land/std@0.129.0/log/mod.ts";
import { Pool } from "../pool.js";
/*
params:
     selection_rate:  selection ratio
     crossover_rate:  crossover ratio
     mutation_rate:   mutation ratio
     crossover_type:  type of the crossover operator
     max_generation:  maximum number of generation/epochs
*/

class FastGARF {
    constructor(GA_CONFIG, RF_CONFIG) {
        // basic input data: training, testing, and optimization set
        this.training_set = [];
        this.optimization_set = [];
        this.optimization_target = [];
        this.testing_set = [];
        this.testing_target = [];
        this.feature_names = GA_CONFIG.feature_names;

        // GA specific variables
        this.population = [];
        this.accuracy = [];
        this.generation_counter = 0;
        this.chromosome_id = 0;


        // GA logging variables
        this.evolution_cache = new OrderedMap();
        this.evolution_log = [];

        this.accuracy_log = new OrderedMap(); // using testing set
        this.performance_log = new OrderedMap(); // using optimization set

        // Config files for GA and RF
        this.GA_CONFIG = GA_CONFIG;
        this.RF_CONFIG = RF_CONFIG;

        this.helper = new ArrayHelper();
        this.evaluation = new Evaluation();

        this.set_params();
    }

    set_params() {
        // parameters for the Genetic Algorithm model
        this.selection_rate = this.GA_CONFIG.selection_rate;
        this.crossover_rate = this.GA_CONFIG.crossover_rate;
        this.mutation_rate = this.GA_CONFIG.mutation_rate;
        this.population_size = this.GA_CONFIG.population_size;

        // this._identifier = this.GA_CONFIG.config_description;
        // this.output_directory = this.GA_CONFIG.debug_config.output_directory;
        // this.output_folder = this.GA_CONFIG.debug_config.output_folder;
    }


    run(train, x_optimization, y_optimization, x_test, y_test, verbose = false) {
        this.initialize(train, x_optimization, y_optimization, x_test, y_test, false);
        // this.population_snapshot();
        while (!this.terminate()) {
            this.crossover(false);
            this.mutation(false);
            this.fitness(false);
            this.evaluate(false);
            this.generation_counter += 1;
        }
        // this.population_snapshot();
        // this.result();
        // console.log("Finished");
    }


    initialize(train, x_optimization, y_optimization, x_test, y_test, verbose = false) {
        // using our caching technique
        this.pool = new Pool(train, x_optimization, y_optimization, x_test, y_test);

        // store our initial training set
        this.training_set = JSON.parse(JSON.stringify(train));
        this.optimization_set = JSON.parse(JSON.stringify(x_optimization));
        this.optimization_target = JSON.parse(JSON.stringify(y_optimization));
        this.testing_set = JSON.parse(JSON.stringify(x_test));
        this.testing_target = JSON.parse(JSON.stringify(y_test));

        if (verbose) {
            console.log("\n------------------------------Initialize-----------------------------------");
            console.log("Population Size: ", this.population_size);
            console.log("Gene length: ", this.RF_CONFIG.n_estimators);
        }

        for (let i = 0; i < this.population_size; i++) {
            // train the Random Forest model

            const randomForestClassifier = this.build_forest();
            const genes_ids = [];
            for (let i = 0; i < randomForestClassifier.trees.length; i++) {
                const key = this.pool.add_to_buffer(randomForestClassifier.trees[i].model);
                genes_ids.push(key)
            }

            const y_predicted_opt = this.pool.get_opt_prediction(genes_ids);
            const y_predicted_test = this.pool.get_test_prediction(genes_ids)
            const initial_performance = this.evaluation.accuracy_metric(y_predicted_opt, this.optimization_target);
            const initial_accuracy = this.evaluation.accuracy_metric(y_predicted_test, this.testing_target);

            if (verbose) {
                console.log("Initializing Chromosome", i, ": Performance: ", initial_performance, " Accuracy:", initial_accuracy);
            }
            this.population.push({
                chromosome_id: this.chromosome_id,
                genes: genes_ids,
                performance: initial_performance,
                accuracy: initial_accuracy
            });
            this.chromosome_id += 1;
        }
    }


    build_forest(config = this.RF_CONFIG) {
        // create the first population
        config.max_features = this.helper.random_number_generator(2, this.feature_names.length - 1);
        const randomForestClassifier = new FastRandomForest(config);
        // train the Random Forest model
        randomForestClassifier.fit(this.training_set, this.feature_names);
        return randomForestClassifier;
    }


    // fitness function for using the optimization set to give current chromosome a fitness scoore
    fitness(verbose = false) {
        let new_population = []
        for (let i = 0; i < this.population.length; i++) {
            const y_predicted = this.pool.get_opt_prediction(this.population[i].genes);
            const fitness_score = this.evaluation.accuracy_metric(y_predicted, this.optimization_target);

            new_population.push({
                chromosome_id: this.population[i].chromosome_id,
                genes: this.population[i].genes,
                performance: fitness_score,
                accuracy: 0
            })
        }

        if (verbose) {
            console.log("\n------------------------------Fitness-----------------------------------");
            console.log("Fitness in Generation: ", this.generation_counter);
            console.log("Chromosome, Fitness Score: ");
            for (let i = 0; i < new_population.length; i++) {
                console.log(new_population[i].chromosome_id, ",", new_population[i].performance);
            }
        }
        new_population = _.sortBy(new_population, function (obj) { return obj.performance; }).reverse();
        this.population = new_population.slice(0, this.population_size);
    }


    mutation(verbose = false) {
        if (verbose) {
            console.log("\n------------------------------Mutation-----------------------------------");
        }
        const mutation_number = Math.floor(this.mutation_rate * this.population.length);
        for (let i = 0; i < mutation_number; i++) {
            let mutate_from_idx = this.helper.random_number_generator(0, this.population.length - 1);
            let mutate_to_idx = this.helper.random_number_generator(0, this.population.length - 1);

            while (mutate_from_idx == mutate_to_idx) {
                mutate_from_idx = this.helper.random_number_generator(0, this.population.length - 1);
                mutate_to_idx = this.helper.random_number_generator(0, this.population.length - 1);
            }

            const length_of_chromosome = Math.min(this.population[mutate_from_idx].genes.length, this.population[mutate_to_idx].genes.length);
            let mutate_from_idx_chromosome = this.helper.random_number_generator(0, length_of_chromosome - 1);
            let mutate_to_idx_chromosome = this.helper.random_number_generator(0, length_of_chromosome - 1);
            // swap 
            let chosen_mutated_gene = this.population[mutate_from_idx].genes[mutate_from_idx_chromosome];
            let chosen_mutate_from_gene = this.population[mutate_to_idx].genes[mutate_to_idx_chromosome];

            while (chosen_mutated_gene == chosen_mutate_from_gene) {
                mutate_from_idx_chromosome = this.helper.random_number_generator(0, length_of_chromosome - 1);
                mutate_to_idx_chromosome = this.helper.random_number_generator(0, length_of_chromosome - 1);

                chosen_mutate_from_gene = this.population[mutate_from_idx].genes[mutate_from_idx_chromosome];
                chosen_mutated_gene = this.population[mutate_to_idx].genes[mutate_to_idx_chromosome];
            }

            if (verbose) {
                console.log("\nMutate: From chromosome", mutate_from_idx, " Gene:", mutate_from_idx_chromosome, " -> chromosome:", mutate_to_idx, " Gene:", mutate_to_idx_chromosome);
                console.log("Before Mutation: ");
                console.log("parent_1_genes: ", this.population[mutate_from_idx].genes);
                console.log("parent_2_genes: ", this.population[mutate_to_idx].genes)
            }



            const mutated_genes = JSON.parse(JSON.stringify(this.population[mutate_to_idx].genes));
            mutated_genes[mutate_to_idx_chromosome] = chosen_mutated_gene;


            if (verbose) {
                console.log("After Mutation: ");
                console.log("parent_1_genes: ", this.population[mutate_from_idx].genes);
                console.log("parent_2_genes: ", this.population[mutate_to_idx].genes)
                console.log("Child: ", mutated_genes);
            }

            this.population.push({
                chromosome_id: this.chromosome_id,
                genes: mutated_genes,
                performance: 0,
                accuracy: 0
            });
            this.chromosome_id += 1;


        }
        if (verbose) {
            console.log("-----------------------------------------------------------------------");
        }
    }


    crossover(verbose = true) {
        if (verbose) {
            console.log("\n------------------------------Crossover-----------------------------------");
        }
        const child_genes = []
        const crossover_number = Math.floor(this.crossover_rate * this.population.length);
        for (let i = 0; i < crossover_number; i++) {
            let crossover_from_idx = this.helper.random_number_generator(0, this.population.length - 1);
            let crossover_to_idx = this.helper.random_number_generator(0, this.population.length - 1);

            while (crossover_from_idx == crossover_to_idx) {
                crossover_from_idx = this.helper.random_number_generator(0, this.population.length - 1);
                crossover_to_idx = this.helper.random_number_generator(0, this.population.length - 1);
            }

            const parent_1_genes = this.population[crossover_from_idx].genes;
            const parent_2_genes = this.population[crossover_to_idx].genes;
            const length_of_chromosomes = Math.min(parent_1_genes.length, parent_2_genes.length);

            const crossover_point_on_genes = this.helper.random_number_generator(1, length_of_chromosomes - 1);

            if (verbose) {
                // ! DEBUG
                console.log("\nCrossover:", crossover_from_idx, "-> ", crossover_to_idx, "at [", crossover_point_on_genes, "]");
                console.log("Before Crossover: ");
                console.log("parent_1_genes: ", parent_1_genes);
                console.log("parent_2_genes: ", parent_2_genes);
            }

            const child_1_genes = [];
            const child_2_genes = [];

            for (let i = 0; i < crossover_point_on_genes; i++) {
                child_1_genes.push(parent_2_genes[i]);
                child_2_genes.push(parent_1_genes[i]);
            }

            for (let i = crossover_point_on_genes; i < parent_1_genes.length; i++) {
                child_1_genes.push(parent_1_genes[i]);
                child_2_genes.push(parent_2_genes[i]);
            }

            if (verbose) {
                // ! DEBUG
                console.log("\nAfter Crossover: ");
                console.log("child_1: ", child_1_genes);
                console.log("child_2: ", child_2_genes);
            }

            child_genes.push(child_1_genes);
            child_genes.push(child_2_genes);
        }


        for (let i = 0; i < child_genes.length; i++) {
            this.population.push({
                chromosome_id: this.chromosome_id,
                genes: child_genes[i],
                performance: 0,
                accuracy: 0
            });
            this.chromosome_id += 1;
        }
        if (verbose) {
            console.log("-----------------------------------------------------------------------\n");
        }
    }


    evaluate(verbose = false) {
        if (verbose) {
            console.log("\n------------------------------Evaluation-----------------------------------");
            console.log("Chromosome, Accuracy");
        }

        const current_generation_summary = []
        for (let i = 0; i < this.population.length; i++) {
            const y_predicted = this.pool.get_test_prediction(this.population[i].genes)
            const cur_accuracy = this.evaluation.accuracy_metric(y_predicted, this.testing_target);
            this.population[i].accuracy = cur_accuracy;
            current_generation_summary.push(this.population[i]);
            if (verbose) {
                console.log(this.population[i].chromosome_id, ",", cur_accuracy);
            }

        }
        const sorted_population = _.sortBy(this.population, function (obj) { return obj.accuracy; }).reverse();
        this.accuracy.push({
            "epoch": this.generation_counter,
            "chromosome_id": sorted_population[0].chromosome_id,
            "performance": sorted_population[0].performance,
            "accuracy": sorted_population[0].accuracy
        });

        this.evolution_log.push(current_generation_summary);

        if (verbose) {
            console.log("Generation:", this.generation_counter, "- Best chromosome:", sorted_population[0].chromosome_id, sorted_population[0].performance, sorted_population[0].accuracy, "\n");
        }
    }


    // Termination condition
    terminate(verbose = false) {
        if (this.generation_counter > 0) {
            if (verbose) {
                console.log(this.accuracy[this.generation_counter - 1]);
            }

            const best_accuracy = this.accuracy[this.generation_counter - 1].accuracy;
            if (Number(best_accuracy) == 100) {
                console.log("Perfect Score!")
                return true;
            }
        }

        if (this.generation_counter < this.GA_CONFIG.max_generation) {
            return false;
        }
        return true;
    }


    population_snapshot() {
        console.log("\n---------------------------------------------------------------------------");
        console.log(`------------------------------Population ${this.generation_counter} Snapshot--------------------------`);
        console.log("---------------------------------------------------------------------------");
        console.log("Chromosome_ID:", ", Genes_IDs,  Performance,  Accuracy:",);
        for (let i = 0; i < this.population.length; i++) {
            console.log(this.population[i].chromosome_id, String(this.population[i].genes), this.population[i].performance, this.population[i].accuracy)
        }
        console.log("---------------------------------------------------------------------------\n");
    }

    // Summary after each run
    result(verbose = true) {
        const sorted_accuracy = _.sortBy(this.accuracy, function (obj) { return obj.accuracy; }).reverse();

        if (verbose) {
            console.log("\n---------------------------------------------------------------------------");
            console.log("-----------------------------------SUMMARY---------------------------------");
            console.log("---------------------------------------------------------------------------");
            console.log("\nLearning Curve: ");
            console.log("Epoch,  Chromosome_ID,  Performance,  Accuracy")
            for (let i = 0; i < this.accuracy.length; i++) {
                console.log(this.accuracy[i].epoch, ",", this.accuracy[i].chromosome_id, ",", this.accuracy[i].performance, ",", this.accuracy[i].accuracy);
            }

            console.log("\nInitial Accuracy From All Chromosomes: ");
            for (let i = 0; i < this.evolution_log[0].length; i++) {
                console.log(this.evolution_log[0][i].chromosome_id, ",", this.evolution_log[0][i].performance, ",", this.evolution_log[0][i].accuracy);
            }

            console.log("\nEnd Accuracy From All Chromosomes: ");
            for (let i = 0; i < this.evolution_log[this.evolution_log.length - 1].length; i++) {
                console.log(this.evolution_log[this.evolution_log.length - 1][i].chromosome_id, ",", this.evolution_log[this.evolution_log.length - 1][i].performance, ",", this.evolution_log[this.evolution_log.length - 1][i].accuracy);
            }


            console.log("\nTotal number of generation: ", this.generation_counter);
            console.log("Initial Accuracy:", this.accuracy[0].epoch, ",", this.accuracy[0].chromosome_id, ",", this.accuracy[0].accuracy);
            console.log("Best Accuracy:", sorted_accuracy[0].epoch, ",", sorted_accuracy[0].chromosome_id, ",", sorted_accuracy[0].accuracy);



            console.log("---------------------------------------------------------------------------");
            console.log("---------------------------------------------------------------------------");
        }
    }


    write_to_file(filename) {
        let text = "";
        for (let i = 0; i < this.accuracy.length; i++) {
            text += JSON.stringify(this.accuracy[i].epoch) + "\n";
            text += JSON.stringify(this.accuracy[i].accuracy) + "\n";
            text += JSON.stringify(this.accuracy[i].genes) + "\n";
        }
        Deno.writeTextFileSync(filename + ".txt", text);
    }
    get_stat() {
        const sorted_accuracy = _.sortBy(this.accuracy, function (obj) { return obj.accuracy; }).reverse();
        const stat = {
            generations: this.generation_counter,
            initial_accuracy: this.accuracy[0].accuracy,
            best_accuracy: sorted_accuracy[0].accuracy,
            best_chromosome: [sorted_accuracy[0].epoch, sorted_accuracy[0].chromosome_id, sorted_accuracy[0].accuracy]
        }
        return stat;

    }
}

export { FastGARF };