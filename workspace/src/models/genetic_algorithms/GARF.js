import "https://deno.land/x/lodash@4.17.19/vendor/underscore/underscore.js";
import { FastDecisionTree } from "../fast_decision_tree.js";
import { FastRandomForest } from "../fast_random_forest.js";
import { ArrayHelper } from "../../util/array_helper.js";
import { Evaluation } from "../evaluation.js";
import { OrderedMap } from "../../util/ordered_map.js";
import * as log from "https://deno.land/std@0.129.0/log/mod.ts";

/*
params:
     selection_rate:  selection ratio
     crossover_rate:  crossover ratio
     mutation_rate:   mutation ratio
     crossover_type:  type of the crossover operator
     max_generation:  maximum number of generation/epochs
*/

class GARF {
    constructor(GA_CONFIG, RF_CONFIG) {
        // basic input data: training, testing, and optimization set
        this.training_set = [];
        this.optimization_set = [];
        this.optimization_target = [];
        this.testing_set = [];
        this.testing_target = [];
        this.feature_names = GA_CONFIG.feature_names;

        // GA specific variables
        this.population = [];
        this.accuracy = [];
        this.generation_counter = 0;
        this.chromosome_id = 0;

        // GA logging variables
        this.evolution_cache = new OrderedMap();
        this.evolution_log = new OrderedMap();

        this.accuracy_log = new OrderedMap(); // using testing set
        this.performance_log = new OrderedMap(); // using optimization set

        // Config files for GA and RF
        this.GA_CONFIG = GA_CONFIG;
        this.RF_CONFIG = RF_CONFIG;

        this.helper = new ArrayHelper();
        this.evaluation = new Evaluation();

        this.set_params();
    }

    set_params() {
        // parameters for the Genetic Algorithm model
        this.selection_rate = this.GA_CONFIG.selection_rate;
        this.crossover_rate = this.GA_CONFIG.crossover_rate;
        this.mutation_rate = this.GA_CONFIG.mutation_rate;
        this.population_size = this.GA_CONFIG.population_size;

        // this._identifier = this.GA_CONFIG.config_description;
        // this.output_directory = this.GA_CONFIG.debug_config.output_directory;
        // this.output_folder = this.GA_CONFIG.debug_config.output_folder;
    }


    run(train, x_optimization, y_optimization, x_test, y_test, verbose = false) {
        this.initialize(train, x_optimization, y_optimization, x_test, y_test, verbose);
        this.evaluate(x_test, y_test, false);
        while (!this.terminate()) {
            this.crossover(this.population, false);
            this.mutation(this.population, false);
            this.fitness(x_optimization, y_optimization, false);
            this.evaluate(x_test, y_test, false);
            log.debug(`finished generation: ${this.generation_counter}`);
            this.generation_counter += 1;
        }
        this.result();
        console.log("Finished");
    }


    initialize(train, x_optimization, y_optimization, x_test, y_test) {
        // store our initial training set
        this.training_set = JSON.parse(JSON.stringify(train));

        this.optimization_set = JSON.parse(JSON.stringify(x_optimization));
        this.optimization_target = JSON.parse(JSON.stringify(y_optimization));

        this.testing_set = JSON.parse(JSON.stringify(x_test));
        this.testing_target = JSON.parse(JSON.stringify(y_test));

        // const x_train = this.helper.getColumn(train);
        // const y_train = this.helper.removeColumn(train);

        console.log("Population Size: ", this.population_size);
        for (let i = 0; i < this.population_size; i++) {
            // train the Random Forest model
            // console.log("Initializing Chromosome", i, "...");

            const randomForestClassifier = this.build_forest();
            const y_predicted = randomForestClassifier.predict(this.testing_set);
            const cur_random_forest_accuracy = this.evaluation.accuracy_metric(y_predicted, this.testing_target);
            this.population.push({
                "id": this.chromosome_id,
                "model": randomForestClassifier,
                "performance": cur_random_forest_accuracy
            });
            this.chromosome_id += 1;
        }
    }


    build_forest(config = this.RF_CONFIG) {
        // create the first population
        config.max_features = this.helper.random_number_generator(2, this.feature_names.length - 1);
        const randomForestClassifier = new FastRandomForest(config);
        // train the Random Forest model
        randomForestClassifier.fit(this.training_set, this.feature_names);
        return randomForestClassifier;
    }


    // fitness function for using the optimization set to give current chromosome a fitness scoore
    fitness(x_optimization = this.optimization_set, y_optimization = this.optimization_target, verbose = true) {
        let new_population = []
        for (let i = 0; i < this.population.length; i++) {
            const y_predicted = this.population[i].model.predict(x_optimization);
            const fitness_score = this.evaluation.accuracy_metric(y_predicted, y_optimization);
            new_population.push({
                "id": this.population[i].id,
                "model": this.population[i].model,
                "performance": fitness_score
            })
        }

        if (verbose) {
            console.log("Fitness in Generation: ", this.generation_counter);
            for (let i = 0; i < new_population.length; i++) {
                console.log("            Chromosome: ", new_population[i].id, " Fitness Score: ", new_population[i].performance);
            }
        }
        new_population = _.sortBy(new_population, function (obj) { return obj.performance; }).reverse();
        this.population = new_population.slice(0, this.population_size);
    }


    mutation(population, verbose = false) {
        const mutation_number = Math.floor(this.mutation_rate * this.population.length)

        for (let i = 0; i < mutation_number; i++) {
            let mutate_from_idx = this.helper.random_number_generator(0, population.length - 1);
            let mutate_to_idx = this.helper.random_number_generator(0, population.length - 1);

            while (mutate_from_idx === mutate_to_idx) {
                mutate_from_idx = this.helper.random_number_generator(0, population.length - 1);
                mutate_to_idx = this.helper.random_number_generator(0, population.length - 1);
            }

            const length_of_chromosome = Math.min(population[mutate_from_idx].model.trees.length, population[mutate_to_idx].model.trees.length);
            let mutate_idx_on_chromosome = this.helper.random_number_generator(0, length_of_chromosome - 1);

            // swap 
            const chosen_mutated_tree = JSON.parse(JSON.stringify(population[mutate_from_idx].model.trees[mutate_idx_on_chromosome].model));
            const chosen_mutate_from_tree = JSON.parse(JSON.stringify(population[mutate_to_idx].model.trees[mutate_idx_on_chromosome].model));
            while (chosen_mutated_tree == chosen_mutate_from_tree) {
                mutate_idx_on_chromosome = this.helper.random_number_generator(0, length_of_chromosome - 1);
                chosen_mutated_tree = JSON.parse(JSON.stringify(population[mutate_from_idx].model.trees[mutate_idx_on_chromosome].model));
                chosen_mutate_from_tree = JSON.parse(JSON.stringify(population[mutate_to_idx].model.trees[mutate_idx_on_chromosome].model));
            }
            if (verbose) {
                console.log("Mutate: From chromosome", mutate_from_idx, " Gene : ", mutate_idx_on_chromosome, " to chromosome: ", mutate_to_idx);
            }
            const mutated_tree_copy = new FastDecisionTree();
            mutated_tree_copy.parse(population[mutate_from_idx].model.trees[mutate_idx_on_chromosome].model);
            population[mutate_to_idx].model.trees[mutate_idx_on_chromosome].model = mutated_tree_copy;
        }
    }


    crossover(population, verbose = false) {
        const childs = []
        const crossover_number = Math.floor(this.crossover_rate * this.population.length);
        for (let i = 0; i < crossover_number; i++) {
            let crossover_from_idx = this.helper.random_number_generator(0, population.length - 1);
            let crossover_to_idx = this.helper.random_number_generator(0, population.length - 1);
            while (crossover_from_idx === crossover_to_idx) {
                crossover_from_idx = this.helper.random_number_generator(0, population.length - 1);
                crossover_to_idx = this.helper.random_number_generator(0, population.length - 1);
            }

            if (verbose) {
                console.log("Crossover: ", crossover_from_idx, crossover_to_idx);
            }

            const parent_1 = population[crossover_from_idx].model;
            const parent_2 = population[crossover_to_idx].model;

            const length_of_chromosomes = Math.min(parent_1.trees.length, parent_2.trees.length);
            const crossover_point_on_genes = this.helper.random_number_generator(1, length_of_chromosomes);

            const child_1 = new FastRandomForest(parent_1.RF_CONFIG);
            const child_2 = new FastRandomForest(parent_2.RF_CONFIG);
            child_1.parse_forest(parent_1);
            child_2.parse_forest(parent_2);

            for (let i = 0; i < crossover_point_on_genes; i++) {
                child_1.trees[i].model = child_2.trees[i].model;
            }

            for (let i = crossover_point_on_genes; i < child_2.trees.length; i++) {
                child_2.trees[i].model = child_1.trees[i].model;
            }

            childs.push(child_1);
            childs.push(child_2);

        }
        for (const child of childs) {
            this.population.push({
                "id": this.chromosome_id,
                "model": child,
                "performance": 0
            });
            this.chromosome_id += 1;
        }
    }


    evaluate(x_test, y_test, verbose = false) {
        if (verbose) {
            console.log("Evaluation:", this.generation_counter);
            console.log("               Chromosome, Accuracy");
        }
        for (let i = 0; i < this.population.length; i++) {
            const y_predicted = this.population[i].model.predict(x_test);
            const cur_accuracy = this.evaluation.accuracy_metric(y_predicted, y_test);
            this.population[i].performance = cur_accuracy;
            if (verbose) {
                console.log("                   ", this.population[i].id, ",", cur_accuracy);
            }
        }
        const sorted_population = _.sortBy(this.population, function (obj) { return obj.performance; }).reverse();
        this.accuracy.push({
            "epoch": this.generation_counter,
            "chromosome_id": sorted_population[0].id,
            "performance": sorted_population[0].performance
        });

        if (verbose) {
            console.log("Generation:", this.generation_counter, "- Best chromosome:", sorted_population[0].id, sorted_population[0].performance, "\n");
        }
    }


    // Termination condition
    terminate() {
        if (this.generation_counter > 0) {
            console.log(this.accuracy[this.generation_counter - 1]);
            const best_accuracy = this.accuracy[this.generation_counter - 1].performance;
            if (Number(best_accuracy) == 100) {
                return true;
            }
        }

        if (this.generation_counter < this.GA_CONFIG.max_generation) {
            return false;
        }
        return true;
    }

    // Summary after each run
    result(verbose = true) {
        const sorted_accuracy = _.sortBy(this.accuracy, function (obj) { return obj.performance; }).reverse();
        if (verbose) {
            console.log("-----------------------------------------------------");
            console.log("------------------------SUMMARY----------------------");
            console.log("-----------------------------------------------------");
            console.log("\nTotal number of generation: ", this.generation_counter);
            console.log("Best Accuracy:", sorted_accuracy[0].epoch, sorted_accuracy[0].chromosome_id, sorted_accuracy[0].performance);
            console.log("Initial Accuracy:", this.accuracy[0].epoch, this.accuracy[0].chromosome_id, this.accuracy[0].performance);

            console.log("Learning Curve: ");
            for (let i = 0; i < this.accuracy.length; i++) {
                console.log(this.accuracy[i].epoch, ",", this.accuracy[i].chromosome_id, ",", this.accuracy[i].performance);
            }

            console.log("\n-----------------------------------------------------");
            console.log("-----------------------------------------------------");
        }
    }
}

export { GARF };