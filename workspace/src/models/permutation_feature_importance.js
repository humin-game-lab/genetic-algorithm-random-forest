
import { Evaluation } from "./evaluation.js"
class FeatureImportance {
    constructor(model, features, n_repeats = 10) {
        this.model = model;
        this.features = features;
        this.n_repeats = n_repeats;
        this.evaluation = new Evaluation();
    }


    fit(x_test, y_test) {
        let model_accuracy = this.get_model_accuracy(x_test, y_test);
        const permutation_accuracy = {};
        for (let i = 0; i < x_test[0].length; i++) {
            const accuracy = []
            for (let repeat = 0; repeat < this.n_repeats; repeat++) {
                const permutated_set = this.get_permutation_data(x_test, i);
                const y_pred = this.model.predict(permutated_set);
                const accuracy_permutation = this.evaluation.accuracy_metric(y_pred, y_test);
                // const mse = Math.pow(accuracy_permutation - model_accuracy, 1);
                accuracy.push(accuracy_permutation - model_accuracy);
            }
            permutation_accuracy[this.features[i]] = this.get_sum(accuracy);
        }

        console.log(permutation_accuracy)

        let temp = _.pairs(permutation_accuracy);
        const sorted_permutation_accuracy = _.sortBy(temp, function (pair) { return -pair[1] });
        return sorted_permutation_accuracy;
    }

    get_model_accuracy(x_test, y_test) {
        const y_pred = this.model.predict(x_test);
        const accuracy = this.evaluation.accuracy_metric(y_pred, y_test);
        return accuracy;
    }

    get_mean(array) {
        return array.reduce((a, b) => a + b) / array.length;
    }

    get_sum(array) {
        return array.reduce((a, b) => a + b);
    }

    get_permutation_data(rows, index) {
        let rows_copy = JSON.parse(JSON.stringify(rows));
        let column = this.get_column(rows_copy, index);
        let shuffled_column = this.shuffle(column);
        for (let i = 0; i < rows.length; i++) {
            rows_copy[i][index] = shuffled_column[i];
        }
        return rows_copy;
    };

    get_column(rows, index) {
        const array_column = (arr, n) => arr.map((x) => x[n]);
        return array_column(rows, index);
    }


    // reference: https://bost.ocks.org/mike/shuffle/
    shuffle(array) {
        let currentIndex = array.length, randomIndex;

        // While there remain elements to shuffle...
        while (currentIndex != 0) {

            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex--;

            // And swap it with the current element.
            [array[currentIndex], array[randomIndex]] = [
                array[randomIndex], array[currentIndex]];
        }
        return array;
    }



}

export { FeatureImportance }