import { Map } from "../util/ordered_map.js";
import { Evaluation } from "./evaluation.js";
import { ArrayHelper } from "../util/array_helper.js";
class Pool {
    constructor(train, x_opt, y_opt, x_test, y_test) {
        this.train = train;
        this.x_test = x_test;
        this.y_test = y_test;
        this.x_opt = x_opt;
        this.y_opt = y_opt;

        this.buffer = new Map();
        this.evaluation = new Evaluation();
        this.helper = new ArrayHelper();
        this.id = -1;
    }

    add_to_buffer(tree) {
        this.id += 1;
        this._create_entry(this.id, tree);
        return this.id;
    }

    get_from_buffer(id) {
        return this.buffer.get(id);
    }

    if_exists(id) {
        return this.buffer.has(id);
    }

    majority_vote(results) {
        let maxCount = 0;
        let predictionName = null;
        for (const key of Object.keys(results)) {
            if (results[key] > maxCount) {
                predictionName = key;
                maxCount = results[key];
            }
        }
        return predictionName;
    }


    get_test_prediction(chromosome) {
        const prediction_matrix = []
        for (let i = 0; i < chromosome.length; i++) {
            const gene_info = this.get_from_buffer(chromosome[i]);
            prediction_matrix.push(gene_info.y_predict_test);
        }

        const y_pred = []
        for (let i = 0; i < this.x_test.length; i++) {
            const cur_y_pred = this.helper.getColumn(prediction_matrix, i);
            const result = cur_y_pred.reduce(function (acc, curr) {
                return acc[curr] ? ++acc[curr] : (acc[curr] = 1), acc;
            }, {});

            const pred = this.majority_vote(result);
            y_pred.push(pred);
        }
        return y_pred;
    }

    get_opt_prediction(chromosome) {
        const prediction_matrix = []
        for (let i = 0; i < chromosome.length; i++) {
            const gene_info = this.get_from_buffer(chromosome[i]);
            prediction_matrix.push(gene_info.y_predict_opt);
        }

        const y_pred = []
        for (let i = 0; i < this.x_opt.length; i++) {
            const cur_y_pred = this.helper.getColumn(prediction_matrix, i);
            const result = cur_y_pred.reduce(function (acc, curr) {
                return acc[curr] ? ++acc[curr] : (acc[curr] = 1), acc;
            }, {});

            const pred = this.majority_vote(result);
            y_pred.push(pred);
        }
        return y_pred;
    }



    _create_entry(key, model) {
        const y_predict_test = model.predict(this.x_test);
        const accuracy_test = this.evaluation.accuracy_metric(y_predict_test, this.y_test);

        const y_predict_opt = model.predict(this.x_opt);
        const performance = this.evaluation.accuracy_metric(y_predict_opt, this.y_opt);

        const model_info = {
            model: model,
            y_predict_test: y_predict_test,
            y_predict_opt: y_predict_opt,
            performance: performance,
            accuracy: accuracy_test
        }

        this.buffer.set(key, model_info);
    }

}

export { Pool }