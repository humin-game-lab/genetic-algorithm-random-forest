import { OrderedMap } from "../util/ordered_map.js"
import { DecisionTreeAnalyzer } from "./decision_tree_analyzer.js"
class RandomForestAnalyzer {
    constructor(random_forest = null) {
        this.forest = random_forest;
        this.feature_log = new OrderedMap();
    }

    features_summary() {
        const trees = this.forest.trees;
        const feature_names = this.forest.feature_names;
        let feature_log = {};
        for (let i = 0; i < trees.length; i++) {
            const tree_analyzer = new DecisionTreeAnalyzer(trees[i].model, feature_names);
            const features_on_tree = tree_analyzer.get_features();
            for (const key in features_on_tree) {
                feature_log[key] = feature_log[key] ? feature_log[key] + features_on_tree[key] : 1;
            }
        }
        feature_log = _.pairs(feature_log);
        const sorted_feature_log = _.sortBy(feature_log, function (pair) { return -pair[1] });
        return sorted_feature_log;
    }



}


export { RandomForestAnalyzer };