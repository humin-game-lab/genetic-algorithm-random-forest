import { FastDecisionTree } from "./fast_decision_tree.js";
class DecisionTreeAnalyzer {
  constructor(tree = null, features_names = []) {
    this.tree = tree;
    this.features = features_names;
  }

  update_branches(x_test, y_test) {
    let tree_copy = new FastDecisionTree();
    tree_copy.parse(this.tree);
    for (let i = 0; i < x_test.length; i++) {
      this._update(tree_copy.root, x_test[i], y_test[i]);
    }
    return tree_copy;
  }

  _update(root, row, target) {
    const queue = [];
    queue.push(root);
    while (queue.length > 0) {
      const node = queue.pop();
      if (typeof node["eval"] === "undefined") {
        node["eval"] = {};
      } else {
        if (target in node["eval"]) {
          node["eval"][target] += 1;
        } else {
          node["eval"][target] = 1;
        }
      }

      // console.log(node.index, this.features[node.index], node.value);
      if (row[node["index"]] < node["value"]) {
        if (typeof node["left"] == "object") {
          queue.push(node["left"]);
        } else {
          return node["left"];
        }
      } else {
        if (typeof node["right"] == "object") {
          queue.push(node["right"]);
        } else {
          return node["right"];
        }
      }
    }
  }

  get_features(return_type = "object") {
    return this._get_features(this.tree.root, return_type);
  }

  _get_features(root, return_type) {
    const features_index_on_tree = [];
    const queue = [];
    queue.push(root);
    while (queue.length > 0) {
      const node = queue.pop();
      if (typeof node == "object") {
        features_index_on_tree.push(node["index"]);
      }
      if (typeof node["left"] == "object") {
        queue.push(node["left"]);
      }
      if (typeof node["right"] == "object") {
        queue.push(node["right"]);
      }
    }

    let features_on_tree = {};
    for (const feature_index of features_index_on_tree) {
      features_on_tree[this.features[feature_index]] = features_on_tree[
        this.features[feature_index]
      ]
        ? features_on_tree[this.features[feature_index]] + 1
        : 1;
    }
    if (return_type == "object") {
      return features_on_tree;
    } else if (return_type == "array") {
      let result = []
      for (var i in features_on_tree)
        result.push([i, features_on_tree[i]]);
      return result;
    }
  }
}

export { DecisionTreeAnalyzer };
