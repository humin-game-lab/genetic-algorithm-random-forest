import "https://deno.land/x/lodash@4.17.19/vendor/underscore/underscore.js";
import { ArrayHelper } from "../util/array_helper.js";
import { Evaluation } from "./evaluation.js";
import { Bootstrap } from "./bootstrap.js";
import { FastDecisionTree } from "./fast_decision_tree.js";
import { Parser } from "./parser.js";

class FastRandomForest {
  constructor(RF_CONFIG) {
    this.feature_names = RF_CONFIG.feature_names;
    this.n_estimators = RF_CONFIG.n_estimators;
    this.max_features = RF_CONFIG.max_features;
    this.max_depth = RF_CONFIG.max_depth;
    this.min_sample_split = RF_CONFIG.min_sample_split;

    this.max_bootstrap_samples = RF_CONFIG.max_bootstrap_samples;
    this.bootstrap_method = RF_CONFIG.bootstrap_with_replacement;

    this.RF_CONFIG = RF_CONFIG;
    this.training_set = [];
    this.trees = [];

    this.helper = new ArrayHelper();
    this.evaluation = new Evaluation();
  }

  parse_forest(rawForestData) {
    this.RF_CONFIG = rawForestData.RF_CONFIG;
    this.feature_names = rawForestData.feature_names;
    this.training_set = rawForestData.training_set;
    for (let i = 0; i < rawForestData.trees.length; i++) {
      const dt = new FastDecisionTree();
      dt.parse(rawForestData.trees[i].model);
      this.trees.push({
        id: rawForestData.trees[i].id,
        model: dt,
        performance: rawForestData.trees[i].performance,
      });
    }
  }

  build_forest(training_set, feature_names) {
    this.training_set = training_set;
    this.feature_names = feature_names;
    // iteratively build each decision tree
    const [bootstrap_data, oob_data, features, feature_indexes] = this.get_bootstrap_data(training_set, feature_names);

    for (let i = 0; i < this.n_estimators; i++) {
      // using bootstrap and get the out-of-bag samples
      const [cur_sampled_data, cur_oob] = [bootstrap_data[i], oob_data[i]];
      const [cur_feature_names, cur_feature_indexes] = [
        features[i],
        feature_indexes[i],
      ];

      // get the input ready for bootstrap sampling
      const decisionTree = new FastDecisionTree(
        this.max_depth,
        this.min_sample_split
      );

      decisionTree.fit(
        cur_sampled_data,
        cur_feature_names,
        cur_feature_indexes
      );


      this.trees.push({ id: i, model: decisionTree, performance: 0.0 });
    }
  }

  fit(training_set, feature_names, verbose = false) {
    this.build_forest(training_set, feature_names, verbose);
  }

  get_bootstrap_data(training_set, feature_names) {
    const all_feature_names = [...feature_names];
    training_set = this.helper.shuffle(training_set);
    const bootstrap_helper = new Bootstrap(
      training_set,
      all_feature_names,
      this.max_features,
      this.max_bootstrap_samples,
      this.n_estimators
    );
    return bootstrap_helper.run(this.bootstrap_method, false);
  }

  calculate_oob_score(tree, oob_data) {
    const y_oob = this.helper.getColumn(oob_data);
    const predicted = tree.predict(oob_data);
    const oob_score = this.evaluation.accuracy_metric(predicted, y_oob);
    return oob_score;
  }


  evaluate(X, y, verbose = false) {
    const temp_before_performance = []
    for (let i = 0; i < this.trees.length; i++) {
      const curTreePrediction = this.trees[i].model.predict(X);
      temp_before_performance.push(this.trees[i].performance);
      this.trees[i].performance = this.evaluation.accuracy_metric(curTreePrediction, y);
    }

    if (verbose) {
      for (let i = 0; i < this.trees.length; i++) {
        console.log("id: ", this.trees[i].id, " - performance: ", temp_before_performance[i], "->", this.trees[i].performance);
      }
    }
  }

  predict(x_test) {
    const predictions = [];
    for (const row of x_test) {
      const predictions_from_trees = [];
      for (const treeObj of this.trees) {
        const tree = treeObj.model;
        const prediction = tree.predict_single_row(row);
        predictions_from_trees.push(prediction);
      }

      const result = predictions_from_trees.reduce(function (acc, curr) {
        return acc[curr] ? ++acc[curr] : (acc[curr] = 1), acc;
      }, {});

      const predictionName = this.majority_vote(result);
      predictions.push(predictionName);
    }
    return predictions;
  }

  majority_vote(results) {
    let maxCount = 0;
    let predictionName = null;
    for (const key of Object.keys(results)) {
      if (results[key] > maxCount) {
        predictionName = key;
        maxCount = results[key];
      }
    }
    return predictionName;
  }

  get_accuracy(x_test, y_test) {
    const y_predict = this.predict(x_test);
    const accuracy = this.evaluation.accuracy_metric(y_predict, y_test);
    this.accuracy = accuracy;
    return accuracy;
  }

  get_performance(x_opt, y_opt) {
    const y_predict = this.predict(x_opt);
    const accuracy = this.evaluation.accuracy_metric(y_predict, y_opt);
    return accuracy;
  }



  pop_tree_by_index(index) {
    const selected_tree = this.trees[index];
    this.trees.splice(index, 1);
    return selected_tree;
  }

  pop_tree_by_id(id) {
    for (let i = 0; i < this.trees.length; i++) {
      if (this.trees[i].id === id) {
        const selected_tree = this.trees[i];
        this.trees.splice(i, 1);
        return selected_tree;
      }
    }
    return -1;
  }

  get_tree_by_id(id) {
    for (let i = 0; i < this.trees.length; i++) {
      if (this.trees[i].id === id) {
        return this.trees[i];
      }
    }
    return -1;
  }

  remove_bad_trees(ids) {
    for (let i = 0; i < ids.length; i++) {
      this.pop_tree_by_id(ids[i]);
    }
  }

  replace_tree_by_index(new_tree, idx) {
    this.trees[idx].model = new_tree;
    this.trees[idx].performance = 0;
  }


  modify_tree(input_tree) {
    this.trees.push({
      id: input_tree.id,
      model: input_tree.model,
      performance: input_tree.performance,
    });
    this.trees = _.sortBy(this.trees, "id");
  }

  add_trees_with_ids(trees, ids) {
    for (let i = 0; i < trees.length; i++) {
      this.trees.push({
        id: ids[i],
        model: trees[i].model,
        performance: trees[i].performance,
      });
    }
    this.trees = _.sortBy(this.trees, "id");
  }

  add_new_trees(addedTrees) {
    const first_tree_id = this.trees[this.trees.length - 1].id + 1;
    for (let i = 0; i < addedTrees.length; i++) {
      this.trees.push({
        id: first_tree_id + i,
        model: addedTrees[i].model,
        performance: addedTrees[i].performance,
      });
    }
  }

  get_sorted_trees() {
    return _.sortBy(this.trees, "performance");
  }

  async snapshot(path = "./visualization/") {
    const parser = new Parser(this.feature_names, path);
    for (let i = 0; i < this.trees.length; i++) {
      parser.parse(this.trees[i].model, this.feature_names, String(this.trees[i].id + "_" + this.trees[i].performance));
      await parser.output(this.trees[i].id);
    }
  }

  get_avg_performance_from_trees() {
    let sum = 0;
    for (const tree of this.trees) {
      sum += tree.performance;
    }
    return sum / this.trees.length;
  }

  snapshot_model(output_filepath) {
    const result = {};
    result.feature_names = this.feature_names;
    result.n_estimators = this.n_estimators;
    result.max_features = this.max_features;
    result.max_depth = this.max_depth;
    result.min_sample_split = this.min_sample_split;
    result.max_bootstrap_samples = this.max_bootstrap_samples;
    result.bootstrap_method = this.bootstrap_method;
    result.training_set = this.training_set;
    result.trees = this.trees;
    Deno.writeTextFileSync(output_filepath, JSON.stringify(result));
  }

  current_stat() {
    for (let i = 0; i < this.trees.length; i++) {
      console.log("id: ", this.trees[i].id, " performance: ", this.trees[i].performance, " features:", this.trees[i].model.feature_indexes);
    }
  }



}

export { FastRandomForest };
