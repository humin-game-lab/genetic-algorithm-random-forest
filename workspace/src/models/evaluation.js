class Evaluation {
  compare_two_trees(testing, testing_target, tree_1, tree_2, verbose = false) {
    const predicted_1 = tree_1.predict(testing);
    const predicted_2 = tree_2.predict(testing);
    const tree_1_accuracy = this.accuracy_metric(predicted_1, testing_target);
    const tree_2_accuracy = this.accuracy_metric(predicted_2, testing_target);

    if (verbose) {
      console.log(
        "parent tree vs. child tree:",
        tree_1_accuracy,
        tree_2_accuracy,
        tree_1_accuracy < tree_2_accuracy
      );
    }
    return tree_1_accuracy < tree_2_accuracy;
  }

  // need to add precision rate, recall rate and F1-score
  accuracy_metric(predicted, actual) {
    let correct = 0;
    for (let i = 0; i < actual.length; i++) {
      if (actual[i] == predicted[i]) {
        correct += 1;
      }
    }
    return (correct / actual.length) * 100.0;
  }


  correctly_classify_metric(predicted, actual) {
    let correct = 0;
    for (let i = 0; i < actual.length; i++) {
      if (actual[i] == predicted[i]) {
        correct += 1;
      }
    }
    return correct;
  }

  // only supports binary classification at the moment
  confusion_metric(predicted, actual) {
    let target_classes = new Set(predicted.concat(actual));
    let [fp, fn, tp, tn] = [0, 0, 0, 0];
    for (let i = 0; i < predicted.length; i++) {
      if (predicted[i] == actual[i]) {
        if (predicted[i] == 1) {
          tp += 1
        } else {
          tn += 1
        }
      } else {
        if (predicted[i] == 1) {
          fp += 1;
        } else {
          fn += 1;
        }
      }
    }
    const matrix = [
      [tn, fp],
      [fn, tp]
    ]
    return matrix;
  }

  array_to_counter(arr) {
    const counts = {};
    for (const num of arr) {
      counts[num] = counts[num] ? counts[num] + 1 : 1;
    }
    counts["total"] = arr.length;
    return counts;
  }


}

export { Evaluation };
