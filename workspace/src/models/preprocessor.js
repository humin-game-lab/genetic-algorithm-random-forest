import { parse as parseCsv } from "https://deno.land/std@0.128.0/encoding/csv.ts";
import { Evaluation } from "./evaluation.js";
class Preprocessor {
    constructor(CONFIG, label, verbose = false) {
        this._config = CONFIG;
        this.dataset_path = CONFIG.directory;
        this.columns_not_use = CONFIG.columns_not_use;
        this.target_name = CONFIG.target_name;
        this.data = [];
        this.features = [];
        this.number_samples = [];

        if (CONFIG["total_samples"] !== "undefined") {
            this.total_samples = CONFIG.total_samples;
        }

        if (verbose) {
            console.log("\n-----------------------------------------------------------------");
            console.log(`--------------------${label}-------------------------`);
            console.log("------------------------------------------------------------------");
            console.log(this._config);
            console.log("-----------------------------------------------------------------");
            console.log("-----------------------------------------------------------------\n");
        }
    }


    // ----------------------------------------------------------------
    // PUBLIC FUNCTIONS
    // ----------------------------------------------------------------
    async read() {
        let dataset = [];
        if (this.total_samples.length === 0) {
            const [data, features] = await this._read_csv();
            dataset = data;
            this.features = features;
        } else {
            for (let i = 0; i < this.total_samples; i++) {
                const train_file_name = this.dataset_path + "training" + i + ".csv";
                const test_file_name = this.dataset_path + "testing" + i + ".csv";
                const optimiization_file_name = this.dataset_path + "validation" + i + ".csv";
                const cur_sample = await this._read_train_test_optimization(train_file_name, test_file_name, optimiization_file_name);
                dataset.push(cur_sample);
            }
        }
        this.data = dataset;
        return dataset;
    }


    split_x_y(data, y_column_idx = data[0].length - 1) {
        const y_data = this._get_column_by_index(data);
        const X_data = this._remove_column_by_index(data, y_column_idx);
        return [X_data, y_data];
    }



    train_test_split(training_ratio, testing_ratio, verbose = false) {
        const train_count = this.data.length * training_ratio;
        const test_count = this.data.length * testing_ratio;

        const training = this.data.slice(0, train_count);
        const testing = this.data.slice(train_count, train_count + test_count);

        const [x_test, y_test] = this.split_x_y(testing);
        if (verbose) {
            console.log("Training:", training.length);
            console.log("testing:", x_test.length);
            console.log("Target:", y_test.length);
        }

        const feature_indexes = [...Array(this.features.length).keys()];
        return [training, x_test, y_test, feature_indexes];
    }

    train_test_optimization_split(training_ratio, testing_ratio, optimization_ratio, verbose = false) {
        const train_count = this.data.length * training_ratio;
        const test_count = this.data.length * testing_ratio;
        const opt_count = this.data.length * optimization_ratio;

        const train = this.data.slice(0, train_count);
        const test = this.data.slice(train_count, train_count + test_count);
        const opt = this.data.slice(train_count + test_count, train_count + test_count + opt_count);

        const [x_train, y_train] = this.split_x_y(train);
        const [x_test, y_test] = this.split_x_y(test);
        const [x_opt, y_opt] = this.split_x_y(opt);

        if (verbose) {
            console.log("Training Set:", train.length, "x", train[0].length);
            console.log("Testing Set:", x_test.length, "x", x_test[0].length);
            console.log("Optimization Set:", x_opt.length, "x", x_opt[0].length);
        }

        const feature_indexes = [...Array(this.features.length).keys()];
        const result = {
            "features": this.features,
            "features_indexes": feature_indexes,
            "training": train,
            "x_train": x_train,
            "y_train": y_train,
            "x_test": x_test,
            "y_test": y_test,
            "x_opt": x_opt,
            "y_opt": y_opt,
        }
        return result;
    }

    cross_validation_split(n_folds) {
        const dataset_split = [];
        // make a deep copy
        let dataset_copy = JSON.parse(JSON.stringify(this.data));

        // shuffle
        dataset_copy = this.shuffle(dataset_copy);

        // split into k splits
        const fold_size = Math.floor(dataset.length / n_folds);
        for (let i = 0; i < n_folds; i++) {
            const fold = [];
            while (fold.length < fold_size) {
                const index = Math.floor(Math.random() * dataset_copy.length);
                fold.push(dataset_copy[index]);
                dataset_copy.splice(index, 1);
            }
            dataset_split.push(fold);
        }

        const k_fold_cross_validation = [];
        for (let i = 0; i < dataset_split.length; i++) {
            const dataset_split_copy = JSON.parse(JSON.stringify(dataset_split));
            let testing = dataset_split_copy.splice(i, 1);
            let training = dataset_split_copy;
            training = [].concat.apply([], training);
            testing = [].concat.apply([], testing);
            const testing_target = this.getColumn(testing, testing[0].length - 1);
            const k_fold_cross_validation = [];
            k_fold_cross_validation.push({
                training_set: training,
                testing_set: testing,
                testing_target: testing_target,
            });
        }
        return k_fold_cross_validation;
    }

    shuffle(n = 1, data = this.data) {
        for (let i = 0; i < n; i++) {
            const shuffled_data = JSON.parse(JSON.stringify(data));
            this.data = this._shuffle_rows(shuffled_data);
        }
    }

    stat(y) {
        const evaluation = new Evaluation();
        return evaluation.array_to_counter(y);
    }


    // ----------------------------------------------------------------
    // PRIVATE FUNCTIONS
    // ----------------------------------------------------------------
    async _read(file_path) {
        const content = await parseCsv(Deno.readTextFileSync(file_path));
        return content;
    }

    async _read_csv(path = this.dataset_path, seperate_header = true) {
        let raw_data = await this._read(path);
        raw_data = this._convert_columns_to_numeric(raw_data);
        raw_data = this._remove_columns_by_name(raw_data, this.columns_not_use);
        const features = raw_data[0];
        raw_data = raw_data.slice(1);

        if (!seperate_header) {
            return raw_data;
        }
        return [raw_data, features];
    }

    async _read_train_test_optimization(train_path, test_path, opt_path) {
        const [training, features] = await this._read_csv(train_path);
        const [testing,] = await this._read_csv(test_path);
        const [opt,] = await this._read_csv(opt_path);

        const [x_train, y_train] = this.split_x_y(training);
        const [x_test, y_test] = this.split_x_y(testing);
        const [x_opt, y_opt] = this.split_x_y(opt);

        const result = {
            "features": features,
            "training": training,
            "x_train": x_train,
            "y_train": y_train,
            "x_test": x_test,
            "y_test": y_test,
            "x_opt": x_opt,
            "y_opt": y_opt
        }
        return result;
    }


    _convert_columns_to_numeric(dataset) {
        const numeric_content = [];
        for (let i = 0; i < dataset.length; i++) {
            const numeric_row = [];
            for (let j = 0; j < dataset[i].length; j++) {
                const cell = dataset[i][j];
                if (this._isNumeric(cell)) {
                    numeric_row.push(Number(cell));
                } else {
                    numeric_row.push(cell);
                }
            }
            numeric_content.push(numeric_row);
        }
        return numeric_content;
    }

    _isNumeric(str) {
        if (typeof str != "string") return false // we only process strings!  
        return !isNaN(str) && // use type coercion to parse the _entirety_ of the string (`parseFloat` alone does not do this)...
            !isNaN(parseFloat(str)) // ...and ensure strings of whitespace fail
    }

    _shuffle_rows(rows) {
        let currentIndex = rows.length, randomIndex;
        // While there remain elements to shuffle...
        while (currentIndex != 0) {
            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex--;
            // And swap it with the current element.
            [rows[currentIndex], rows[randomIndex]] = [rows[randomIndex], rows[currentIndex]];
        }
        return rows;
    }

    _get_column_by_index(rows, index = rows[0].length - 1) {
        const arrayColumn = (arr, n) => arr.map((x) => x[n]);
        return arrayColumn(rows, index);
    }


    _remove_column_by_index(rows, index = rows[0].length - 1) {
        const new_rows = [];
        for (let i = 0; i < rows.length; i++) {
            const row = [...rows[i]];
            row.splice(index, 1);
            new_rows.push(row);
        }
        return new_rows;
    }

    _remove_columns_by_name(rows, unused_columns) {
        const table = [];
        const features = rows[0];
        for (const name in features) {
            if (name in unused_columns) {
                table.push(false);
            }
            table.push(true);
        }
        return rows.map(function (x, index) {
            return x.filter(function (y, index1) {
                return table[index1] === true;
            });
        });
    }
}

export { Preprocessor }