import { Logger } from 'https://deno.land/x/log/mod.ts'

const minLevelForConsole = 'DEBUG' // config.minLevelForConsole
const minLevelForFile = 'WARNING' // config.minLevelForFile
// const minLevelForConsole = 'INFO' 
// const minLevelForFile = 'ERROR'
// const minLevelForFile = 'CRITICAL'

const fileName = "./warnings-errors.txt"

// import this logger in your sub modules so that you have one logger for your whole process
export const logger =
    await Logger.getInstance(minLevelForConsole, minLevelForFile, fileName)

logger.debug('example debug message')
logger.info('example info')
logger.warning('example warning')
logger.error('example error message')
logger.critical('example critical message')
