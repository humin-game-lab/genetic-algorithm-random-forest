import { ArrayHelper } from "../util/array_helper.js";
import { DataFrame } from "../models/dataframe.js";
import { sleep } from "https://deno.land/x/sleep@v1.2.1/mod.ts";

await sleep(2);

const helper = new ArrayHelper();

async function read() {
    console.log("Test: Random Forest with raw csv data");
    const datasetConfig = JSON.parse(
        await Deno.readTextFile("./config/config_original_dataset.json")
    );
    let dataset = datasetConfig.breast_cancer_dataset;
    const filePath = dataset.file_name;
    const columns_not_use = dataset.columns_not_use;
    const target_name = dataset.target_name;
    const dataframe = new DataFrame(filePath, target_name, columns_not_use);

    console.log("Dataset Source:", dataset.file_name);
    await dataframe.preprocess();
    const feature_names = dataframe.featureNames;
    dataset = dataframe.dataframe;
    dataset = helper.JSONtoArray(dataset);

    const [training, testing, actual] = helper.train_test_split(dataset, 0.7, 0.3, false);
    const [training_set, tree_feature_names, tree_feature_indexes] =
        helper.get_input_for_decision_tree(training, feature_names, feature_names.length);
    return [training, testing, actual];
}

function getColumn(rows, index = rows[0].length - 1) {
    const arrayColumn = (arr, n) => arr.map((x) => x[n]);
    return arrayColumn(rows, index);
}


function permutation(rows, index) {
    let column = getColumn(rows, index);
    let shuffled_column = shuffle(column);
    for (let i = 0; i < rows.length; i++) {
        rows[i][index] = shuffled_column[i];
    }
    return rows;
};


// reference: https://bost.ocks.org/mike/shuffle/
function shuffle(array) {
    let currentIndex = array.length, randomIndex;

    // While there remain elements to shuffle...
    while (currentIndex != 0) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex--;

        // And swap it with the current element.
        [array[currentIndex], array[randomIndex]] = [
            array[randomIndex], array[currentIndex]];
    }
    return array;
}

// let [training, testing, actual] = await read();

let feature_names = ["F1", "F2", "F3", "F4", "F5"];
let sample_data = [
    [1, 1, 1, 1, 1],
    [2, 2, 2, 2, 2],
    [3, 3, 3, 3, 3],
    [4, 4, 4, 4, 4],
    [5, 5, 5, 5, 5],
    [6, 6, 6, 6, 6],
    [7, 7, 7, 7, 7],
    [8, 8, 8, 8, 8]];

sample_data = [
    [1, 2, 3, 4, 5],
    [1, 2, 3, 4, 5],
    [1, 2, 3, 4, 5],
    [1, 2, 3, 4, 5],
    [1, 2, 3, 4, 5],
    [1, 2, 3, 4, 5],
    [1, 2, 3, 4, 5],
    [1, 2, 3, 4, 5]];


var switcher = [true, false, false, true, true]

function reduceMyArray(arr) {
    return arr.map(function (x, index) {
        return x.filter(function (y, index1) {
            return switcher[index1] === true;
        });
    });
}

var x = reduceMyArray(sample_data);
console.log(x);
