function confusion_metric(predicted, actual) {
    let target_classes = new Set(predicted.concat(actual));
    let [fp, fn, tp, tn] = [0, 0, 0, 0];
    for (let i = 0; i < predicted.length; i++) {
        if (predicted[i] == actual[i]) {
            if (predicted[i] == 1) {
                tp += 1
            } else {
                tn += 1
            }
        } else {
            if (predicted[i] == 1) {
                fp += 1;
            } else {
                fn += 1;
            }
        }
    }
    const matrix = [
        [tn, fp],
        [fn, tp]
    ]
    return matrix;
}

const actual_values = [1, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 1];
const predicted_values = [0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 0, 0, 0, 0];

console.log(confusion_metric(predicted_values, actual_values));

const x = [4150, 4151, 4152, 4153, 4154, 4155,
    4156, 4157, 4158, 4159, 4160, 4161,
    4162, 4163, 4164, 4165, 4166, 4167,
    4168, 4169, 4170, 4171, 4172, 4173,
    4174, 4175, 4176, 4177, 4178, 4179,
    4180, 4181, 4182, 4183, 4184, 4185,
    4186, 4187, 4188, 4189, 4190, 4191,
    4192, 4193, 4194, 4195, 4196, 4197,
    4198, 4199];

console.log(x.length);