import pydot
import os
import sys
from datetime import datetime

def dotToPNG(src_directory, dest_directory):
    dotsFiles = [f for f in os.listdir(src_directory)]
    for file in dotsFiles:
        tokens = os.path.splitext(file)
        print(file,tokens)
        if tokens[1] == ".dot":
            print("converting: ", file, "to", tokens[0]+ ".png")
            (graph,) = pydot.graph_from_dot_file(src_directory + "/" + file)
            graph.write_png(dest_directory+ "/" + tokens[0] + ".png")

if __name__ == "__main__":
    print(f"Arguments count: {len(sys.argv)}")
    args = sys.argv
    print("argv: ", args)
    if len(args) == 4 and args[1] == "-s":
        (graph,) = pydot.graph_from_dot_file(args[2])
        graph.write_png("./visualization/"+ str(datetime.now().strftime('%Y_%m_%d_%H_%M_%S')) +".png")
    elif len(args) == 4 and args[1] == "-m":
        print("Converting .dot files in ", args[1], "to", args[2])
        dotToPNG(args[2], args[3])
    else:
        print("Insufficient command line arguments.")
        
        