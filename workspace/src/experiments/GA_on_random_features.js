import { Evaluation } from "../models/evaluation.js";
import { sleep } from "https://deno.land/x/sleep@v1.2.1/mod.ts";
import { Preprocessor } from "../models/preprocessor.js";
import { FastGeneticAlgorithm } from "../models/fast_genetic_algorithm.js";

await sleep(1);


// preprocess data and get training and testing set
const dataset_config = JSON.parse(await Deno.readTextFile("./config/config.json"));
const dataset_names = ["breast_cancer_dataset", "john_wise_dataset"];
const config = dataset_config[dataset_names[1]];
let preprocessor = new Preprocessor(config);
await preprocessor.read();

// building random forest Classifier
const RF_CONFIG = {
    feature_names: [],
    n_estimators: 10,
    max_features: "sqrt",
    max_depth: 100,
    min_sample_split: 2,
    max_bootstrap_samples: 0.7,
    bootstrap_with_replacement: true,
};

const GA_CONFIG = {
    config_description: "",
    selection_rate: 0.2,
    crossover_rate: 0.2,
    mutation_rate: 0.1,
    diversity_percentage: 0.2,
    crossover_type: "SINGLE",
    max_generation: 100,
    termination_condition: "NONE",
    feature_names: [],

    debug_config: {
        output_directory: "./output/",
        output_folder: "",
        timestamp_start: null,
        timestamp_end: null,
        memory_stamp: [],
    },
};


const dataset = preprocessor.data;
const i = 2;

GA_CONFIG.feature_names = dataset[i].features;
RF_CONFIG.feature_names = dataset[i].features;
GA_CONFIG.debug_config.output_folder = "Mar9_dataset_" + i + "/";
const GA = new FastGeneticAlgorithm(GA_CONFIG, RF_CONFIG);
GA.run_GA_on_features(dataset[i].training, dataset[i].x_opt, dataset[i].y_opt, dataset[i].x_test, dataset[i].y_test);

