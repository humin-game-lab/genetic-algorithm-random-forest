import { Preprocessor } from "../models/preprocessor.js"
import { GARF } from "../models/genetic_algorithms/GARF.js"
import { FastGARF } from "../models/genetic_algorithms/FAST_GARF.js"
import { sleep } from "https://deno.land/x/sleep/mod.ts";
await sleep(1);


// preprocess data and get training and testing set
// For testing purpose: 
// const dataset_config = JSON.parse(await Deno.readTextFile("./config/config_original_dataset.json"));
// const dataset_names = ["iris_dataset", "bank_dataset", "wine_dataset", "breast_cancer_dataset", "abalone_dataset"];
// let preprocessor = new Preprocessor(config);
// await preprocessor.read();
// preprocessor.shuffle(10);
// const dataset = await preprocessor.train_test_optimization_split(0.5, 0.25, 0.25, true);

const dataset_config = JSON.parse(await Deno.readTextFile("./config/config.json"));
const dataset_names = ["breast_cancer_dataset", "john_wise_dataset"];
const config = dataset_config[dataset_names[1]];

// building random forest Classifier
const RF_CONFIG = {
    feature_names: [],
    n_estimators: 10,
    max_features: "random",
    max_depth: 20,
    min_sample_split: 2,
    max_bootstrap_samples: 0.7,
    bootstrap_with_replacement: true,
};

const GA_CONFIG = {
    config_description: "",
    population_size: 10,
    selection_rate: 0.5,
    crossover_rate: 0.2,
    mutation_rate: 0.1,
    diversity_percentage: 0.1,
    crossover_type: "SINGLE",
    max_generation: 10,
    termination_condition: "NONE",
    feature_names: [],
    debug_config: {
        output_directory: "./output/",
        output_folder: "experiment",
        timestamp_start: null,
        timestamp_end: null,
        memory_stamp: [],
    },
};


const idx = 0;
const tracker = [];
for (let idx = 0; idx < 100; idx++) {
    console.log("Running Dataset", idx);
    tracker[idx] = [];
    for (let j = 0; j < 4; j++) {
        const preprocessor = new Preprocessor(config, dataset_names[1]);
        const data = await preprocessor.read(config);


        const dataset = data[idx];
        GA_CONFIG.feature_names = dataset.features;
        RF_CONFIG.feature_names = dataset.features;


        // console.log("Target ")
        // console.log("\ny_train", preprocessor.stat(dataset.y_train));
        // console.log("y_opt", preprocessor.stat(dataset.y_opt));
        // console.log("y_test", preprocessor.stat(dataset.y_test), "\n");

        const GA = new FastGARF(GA_CONFIG, RF_CONFIG);
        GA.run(dataset.training, dataset.x_opt, dataset.y_opt, dataset.x_test, dataset.y_test);
        const stat = GA.get_stat();
        tracker[idx].push(stat)
    }

    // const log_name = "./output/uci/log_" + idx;
    // GA.write_to_file(log_name);
    // tracker.push(stat);
    console.log(tracker[idx]);
    console.log("\n\n");
}

console.log("Tracker");
for (let i = 0; i < tracker.length; i++) {
    let init_acc = 0;
    let end_acc = 0;
    for (let j = 0; j < 4; j++) {
        init_acc += tracker[i][j].initial_accuracy;
        end_acc += tracker[i][j].best_accuracy;
    }

    console.log(i, ",", init_acc / 4, ",", end_acc / 4);

}
// const dataset = preprocessor.data;
// for (let i = 10; i < 20; i++) {
//     GA_CONFIG.feature_names = dataset[i].features;
//     RF_CONFIG.feature_names = dataset[i].features;
//     const GA = new GARF(GA_CONFIG, RF_CONFIG);
//     GA.run(dataset[i].training, dataset[i].x_opt, dataset[i].y_opt, dataset[i].x_test, dataset[i].y_test);
// }


