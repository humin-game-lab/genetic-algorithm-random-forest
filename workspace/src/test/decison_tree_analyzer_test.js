/*
    Testing for Decision Tree
*/

import { ArrayHelper } from "../util/array_helper.js";
import { DataFrame } from "../models/dataframe.js";
import { Evaluation } from "../models/evaluation.js";
import { FastDecisionTree } from "../models/fast_decision_tree.js";
import { sleep } from "https://deno.land/x/sleep@v1.2.1/mod.ts";
import { DecisionTreeAnalyzer } from "../models/decision_tree_analyzer.js";
import { Parser } from "../models/parser.js"

await sleep(1);

const helper = new ArrayHelper();
const evaluation = new Evaluation();

console.log("Test: Decision Tree with partitioned csv data");
const config = JSON.parse(
  await Deno.readTextFile("./config/config_for_testing.json"),
);

const dataset_config = config.breast_cancer_dataset;

console.log("Dataset Source:", dataset_config.directory);
const directory = dataset_config.directory;
const target_name = dataset_config.target_name;
const columns_not_use = dataset_config.columns_not_use;

const training_filenames = dataset_config.training_filenames;
const testing_filenames = dataset_config.testing_filenames;

// for (let idx = 0; idx < training_filenames.length; idx++) {
let idx = 0;
const training_filePath = directory + training_filenames[idx];
const testing_filePath = directory + testing_filenames[idx];

const training_df = new DataFrame(
  training_filePath,
  target_name,
  columns_not_use,
);
const testing_df = new DataFrame(
  testing_filePath,
  target_name,
  columns_not_use,
);

await training_df.preprocess();
await testing_df.preprocess();

const feature_names = training_df.featureNames;
const training_set = helper.JSONtoArray(training_df.dataframe);
const testing_set = helper.JSONtoArray(testing_df.dataframe);

const y_testing = helper.getColumn(testing_set, feature_names.length - 1);
const x_testing = helper.removeColumn(testing_set, feature_names.length - 1);
const feature_indexes = [...Array(feature_names.length).keys()];

const decision_tree = new FastDecisionTree();
decision_tree.fit(training_set, feature_names, feature_indexes);
const predictions = decision_tree.predict(x_testing);
const accuracy = evaluation.accuracy_metric(y_testing, predictions);
console.log("Accuracy from Decision Tree: ", accuracy);

let dt_analyzer = new DecisionTreeAnalyzer(decision_tree);
const features_on_tree = dt_analyzer.get_features();

// console.log("----------------------------------------------------------------Decison Tree Analyzer----------------------------------------------------------------");
// console.log("Feature on the tree: ", features_on_tree);
let updated_tree = dt_analyzer.update_branches(x_testing, y_testing);
let parser = new Parser(feature_names, "./test/");
parser.parse(updated_tree);
parser.output("updated_branch_tree");
parser.parse(decision_tree);
parser.output("original_tree");
// }
