

import { ArrayHelper } from "../util/array_helper.js";
import { DataFrame } from "../models/dataframe.js";
import { Evaluation } from "../models/evaluation.js";
import { FastRandomForest } from "../models/fast_random_forest.js";
import { sleep } from "https://deno.land/x/sleep@v1.2.1/mod.ts";
import { RandomForestAnalyzer } from "../models/random_forest_analyzer.js";

await sleep(1);


const helper = new ArrayHelper();
const evaluation = new Evaluation();

const RF_CONFIG = {
    feature_names: [],
    n_estimators: 100,
    max_features: "sqrt",
    max_depth: 20,
    min_sample_split: 2,
    max_bootstrap_samples: 0.7,
    bootstrap_with_replacement: true,
    feature_names: []
}

async function random_forest_with_raw_data() {
    console.log("Test: Random Forest with raw csv data")
    const datasetConfig = JSON.parse(await Deno.readTextFile("./config/config_original_dataset.json"));
    let dataset = datasetConfig.breast_cancer_dataset;
    const filePath = dataset.file_name;
    const columns_not_use = dataset.columns_not_use;
    const target_name = dataset.target_name;
    const dataframe = new DataFrame(filePath, target_name, columns_not_use);

    console.log("Dataset Source:", dataset.file_name);
    await dataframe.preprocess();
    const feature_names = dataframe.featureNames;
    dataset = dataframe.dataframe;
    dataset = helper.JSONtoArray(dataset)
    RF_CONFIG.feature_names = feature_names;
    // dataset = helper.shuffle(dataset)

    const [training, testing, actual] = helper.train_test_split(dataset, 0.7, 0.3, false);
    const [training_set, tree_feature_names, tree_feature_indexes] = helper.get_input_for_decision_tree(training, feature_names, feature_names.length);

    const random_forest = new FastRandomForest(RF_CONFIG);
    random_forest.fit(training_set, tree_feature_names, tree_feature_indexes);
    const predictions = random_forest.predict(testing);
    const accuracy = evaluation.accuracy_metric(actual, predictions);
    console.log("Accuracy from Random Forest: ", accuracy, "\n");
    return random_forest;
}




let random_forest = await random_forest_with_raw_data();
let random_forest_analyzer = new RandomForestAnalyzer(random_forest);
const features_leg = random_forest_analyzer.get_features_summary();
console.log(features_leg);