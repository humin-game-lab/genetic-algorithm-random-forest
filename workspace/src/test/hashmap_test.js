
import { ArrayHelper } from "../util/array_helper.js";
import { DataFrame } from "../models/dataframe.js";
import { HashMap } from "../util/hashmap.js";
import { FastRandomForest } from "../models/fast_random_forest.js";
import { sleep } from "https://deno.land/x/sleep@v1.2.1/mod.ts";
import { OrderedMap } from "../util/ordered_map.js";



await sleep(1);

const RF_CONFIG = {
    n_estimators: 50,
    max_features: "sqrt",
    max_depth: 20,
    min_sample_split: 2,
    max_bootstrap_samples: 0.7,
    bootstrap_with_replacement: true
}

const helper = new ArrayHelper();
async function create_random_forest() {
    console.log("Test: Random Forest with raw csv data")
    const datasetConfig = JSON.parse(await Deno.readTextFile("./config/config_original_dataset.json"));
    let dataset = datasetConfig.breast_cancer_dataset;
    const filePath = dataset.file_name;
    const columns_not_use = dataset.columns_not_use;
    const target_name = dataset.target_name;
    const dataframe = new DataFrame(filePath, target_name, columns_not_use);

    console.log("Dataset Source:", dataset.file_name);
    await dataframe.preprocess();
    const feature_names = dataframe.featureNames;
    dataset = dataframe.dataframe;
    dataset = helper.JSONtoArray(dataset)
    dataset = helper.shuffle(dataset)

    const [training, testing, actual] = helper.train_test_split(dataset, 0.7, 0.3, false);
    const [training_set, tree_feature_names, tree_feature_indexes] = helper.get_input_for_decision_tree(training, feature_names, feature_names.length);

    const random_forest = new FastRandomForest(RF_CONFIG);
    random_forest.fit(training_set, tree_feature_names, tree_feature_indexes);
    return random_forest;
}

// const map = new HashMap();
// const random_forest = await create_random_forest();
// const trees = random_forest.trees;

// for(let i = 0; i < trees.length; i++){
//     map.set(trees[i].id, trees[i].model);
// }

// const random_forest_2 = await create_random_forest();
// const trees_2 = random_forest_2.trees;

// for(let i = 0; i < trees_2.length; i++){
//     map.set(trees_2[i].id, trees_2[i].model);
// }

// console.log(map);

const map = new OrderedMap();
map.set(1, "Eric");
map.set(2, "John");
map.set(1, "Miao");
map.set(2, "Meadows");
map.remove(1);

console.log(map.get(1)); // 100
console.log(map.get(2)); // 200
console.log(map);