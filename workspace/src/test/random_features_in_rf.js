import { Evaluation } from "../models/evaluation.js";
import { FastRandomForest } from "../models/fast_random_forest.js";
import { sleep } from "https://deno.land/x/sleep@v1.2.1/mod.ts";
import { Preprocessor } from "../models/preprocessor.js";
import { DecisionTreeAnalyzer } from "../models/decision_tree_analyzer.js";
import { ArrayHelper } from "../util/array_helper.js";

await sleep(1);
const evaluation = new Evaluation();
const helper = new ArrayHelper();
// building random forest Classifier
const RF_CONFIG = {
    feature_names: [],
    n_estimators: 10,
    max_features: -1,
    max_depth: 30,
    min_sample_split: 2,
    max_bootstrap_samples: 0.7,
    bootstrap_with_replacement: true,
};

// preprocess data and get training and testing set
const datasetConfig = JSON.parse(await Deno.readTextFile("./config/config_original_dataset.json"));
let config = datasetConfig.breast_cancer_dataset;
let preprocessor = new Preprocessor(config);
let [data, features] = await preprocessor.read_csv();
let [train, x_test, y_test, feature_indexes] = preprocessor.train_test_split(data, features, 0.7, 0.3, true);


let rf_accuracies = []
for (let i = 0; i < 2; i++) {
    RF_CONFIG.max_features = helper.random_number_generator(2, features.length);
    // console.log("max_features: ", RF_CONFIG.max_features)
    let random_forest = new FastRandomForest(RF_CONFIG);
    random_forest.fit(train, features, feature_indexes);
    const rf_predict = random_forest.predict(x_test);

    // evaluate RF model
    const forest_accuracy = evaluation.accuracy_metric(y_test, rf_predict);
    // console.log("Accuracy from Random Forest: ", forest_accuracy, "\n");

    rf_accuracies.push([RF_CONFIG.max_features, forest_accuracy])
    console.log("finished ", i);

    const filename = "./output/_" + i + "_model.json";
    random_forest.snapshot_model(filename);
    // const trees = random_forest.trees;
    // for (let i = 0; i < trees.length; i++) {
    //     let dt_analyer = new DecisionTreeAnalyzer(trees[i].model, features);
    //     console.log("Tree: ", i, dt_analyer.get_features("array"));
    // }
}



function custom_compare(a, b) {
    return a[1] - b[1];
}

console.log("feature_number, accuracy");
rf_accuracies = rf_accuracies.sort(custom_compare).reverse();
for (let i = 0; i < rf_accuracies.length; i++) {
    console.log(rf_accuracies[i][0], ",", rf_accuracies[i][1]);
}
