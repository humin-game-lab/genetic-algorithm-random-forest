import { Preprocessor } from "../models/preprocessor.js"
import { FastDecisionTree } from "../models/fast_decision_tree.js"
import { FastRandomForest } from "../models/fast_random_forest.js"
import { FeatureImportance } from "../models/permutation_feature_importance.js"
import { Evaluation } from "../models/evaluation.js"
import { sleep } from "https://deno.land/x/sleep/mod.ts";
import { Parser } from "../models/parser.js";
await sleep(2);

// preprocess data and get training and testing set
const datasetConfig = JSON.parse(await Deno.readTextFile("./config/config_original_dataset.json"));
const config = datasetConfig.breast_cancer_dataset;
const preprocessor = new Preprocessor(config);
const [data, features] = await preprocessor.read_csv();
const [train, x_test, y_test, feature_indexes] = preprocessor.train_test_split(data, features, 0.7, 0.3, true);


// building a classifier and test the classifier
const decision_tree = new FastDecisionTree();
decision_tree.fit(train, features, feature_indexes);
const dt_predict = decision_tree.predict(x_test);

// building random forest Classifier
const RF_CONFIG = {
    feature_names: features,
    n_estimators: 50,
    max_features: "sqrt",
    max_depth: 20,
    min_sample_split: 2,
    max_bootstrap_samples: 0.7,
    bootstrap_with_replacement: true
}

const random_forest = new FastRandomForest(RF_CONFIG);
random_forest.fit(train, features, feature_indexes);
const rf_predict = random_forest.predict(x_test);


// evaluate DT model
const evaluation = new Evaluation();
const tree_accuracy = evaluation.accuracy_metric(y_test, dt_predict);
console.log("Accuracy from Decision Tree: ", tree_accuracy, "\n");

// evaluate RF model
const forest_accuracy = evaluation.accuracy_metric(y_test, rf_predict);
console.log("Accuracy from Random Forest: ", forest_accuracy, "\n");


// eveluate the features using feature importances
const feature_importance = new FeatureImportance(random_forest, features);
const sorted_permutation_feature_importance = feature_importance.fit(x_test, y_test, 100);
console.log("sorted feature importance: ");
console.log(sorted_permutation_feature_importance);
