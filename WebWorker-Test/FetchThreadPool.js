// function printString(){
//     console.log("Tom"); 
//     setTimeout( ()=>{ console.log("Jacob"); }, 1000); 
//     console.log("Mark")
//  }
 
//  printString();
const myFirstPromise = new Promise((resolve, reject) => { 
    const condition = false;   
    if(condition) {
         setTimeout(function(){
             resolve("Promise is resolved!"); // fulfilled
        }, 300);
    } else {    
        reject('Promise is rejected!');  
    }
});

//  const demoPromise= function() {
//     myFirstPromise
//     .then((successMsg) => {
//         console.log("Success:" + successMsg);
//     })
//     .catch((errorMsg) => { 
//         console.log("Error:" + errorMsg);
//     })
//   }
  
  async function demoPromise() {
    try {
      let message = await myFirstPromise;
      let message  = await helloPromise();
      console.log(message);
  
    }catch((error) => { 
        console.log("Error:" + error.message);
    })
  }
  
  // finally, call our async function
  
  (async () => { 
    await myDate();
  })();

//   demoPromise();