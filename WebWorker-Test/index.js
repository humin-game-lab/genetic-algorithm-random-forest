import * as Helper from "./helper.js"

var dataset_config = JSON.parse(await Deno.readTextFile("./Dataset_Config.JSON"));
var dataset = dataset_config.tennis_dataset;


console.log("Dataset Config: \n", dataset);
var filepath = dataset.filename;
var columns_not_use = dataset.columns_not_use;
var target_name = dataset.target_name;

var df = await Helper.Read(filepath);
var dataframe = Helper.ConvertToDataFrame(df);
var dataframe = Helper.dropColumns(dataframe,columns_not_use);
var [X_train, y_train, X_test, y_test] = Helper.TrainTestSplitDataFrame(dataframe, .8, .2, target_name)

const worker = new Worker(new URL("./worker.js", import.meta.url).href, {
    type: "module",
    deno: {
      namespace: true,
    },
  });


var config = {
    filepath : dataset.filename,
    columns_not_use : dataset.columns_not_use,
    target_name : dataset.target_name,
    train: 0.8, test: 0.2
}



worker.postMessage(config);