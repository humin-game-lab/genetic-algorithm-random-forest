import * as parseCSV from 'https://deno.land/std@0.82.0/encoding/csv.ts';
import * as denoforge from "https://deno.land/x/denoforge@1.0.0/denoforge.js";


export async function CountFromColumn(column){
    var all_values = []
    for(const val of column){
        all_values.push(val.play)
    }
    console.log(all_values)
    var map = all_values.reduce((acc, e) => acc.set(e, (acc.get(e) || 0) + 1), new Map());
    return [...map.entries()]
}

export async function Read(filename){
    console.log("filename", filename)
    const content = await parseCSV.parse(await Deno.readTextFile(filename), {
        skipFirstRow: true,
    });
    return content;
}


export function TrainTestSplitCSV(data, training_percentage, testing_percentage){
    var train_count = parseInt(training_percentage * data.length);
    var testing_count = parseInt(testing_percentage * data.length);

    var X_train = []
    var y_train = []
    var X_test = []
    var y_test = []

    X_train = data.slice(0, train_count)
    for(let i = 0; i < X_train.length; i++){
        y_train.push(X_train[i].play)
    }

    X_test = data.slice(train_count, data.length)
    for(let i = 0; i < X_test.length; i++){
        y_test.push(X_test[i].play)
    }

    // console.log("Total data size: ", data.length)
    // console.log("training size: ", train_count);
    // console.log("testing size: ", testing_count);
    return [X_train, y_train, X_test, y_test]
}

export async function TrainTestSplitDataFrame(dataframe, training_percentage, testing_percentage, target_name){
    var train_count = Math.ceil(training_percentage * dataframe.count());
    var testing_count = Math.floor(testing_percentage * dataframe.count()) ;

    var target_column = dataframe.getSeries(target_name);

    var X_train = dataframe.between(0, train_count-1);
    var X_test = target_column.between(0, train_count-1);


    
    // X_test = X_test.dropSeries(target_name)
    // error checking to make sure that the total number of training and testing items does not exceed the total row in the dataframe
    if((train_count + testing_count) > dataframe.count()){
        throw 'TrainTestSplitDataFrame: Training and Testing objects exceed the total number of rows!';
    }



    var y_train = dataframe.between(train_count, dataframe.count());
    var y_test = target_column.between(train_count, dataframe.count());

    X_train = X_train.dropSeries(target_name);
    y_train = y_train.dropSeries(target_name);


    // console.log("Total data size: ", dataframe.count())
    // console.log("training size: ", train_count);
    // console.log("testing size: ", testing_count);


    return [X_train, y_train, X_test, y_test]
}


export async function ShuffleDataFrame(dataframe){
    console.log("Shuffling the dataframe");
    var arrayOfArrays = dataframe.toRows();
    arrayOfArrays.sort(() => Math.random() - 0.5)
    let newdataframe = new denoforge.DataFrame(arrayOfArrays);
    return newdataframe;
}

export async function ConvertToDataFrame(data){
    let dataframe = new denoforge.DataFrame(data);
    return dataframe;
}

export async function dropColumns(dataframe, columns){
    let newdf = dataframe.dropSeries(columns);
    return newdf;
}
