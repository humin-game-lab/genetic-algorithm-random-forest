import * as Helper from "./helper.js"


// sleep function for debugging
async function sleep(milliseconds){
  return new Promise(resolve => setTimeout(resolve, milliseconds))
}


self.onmessage = async (e) => {
  const { filepath, columns_not_use, target_name, train, test, output_filename, message } = e.data;
  let data = Process(filepath, columns_not_use, target_name, train, test)
  console.log("writing file to "+output_filename)
  Deno.writeTextFileSync(output_filename, message + '\n' + data[0] + '\n' + data[1] + '\n' + data[2] + '\n' + data[3]);
  // postMessage(data);
  self.close();
};


async function Process(filepath, columns_not_use, target_name, train, test) {
  var df = await Helper.Read(filepath);
  var dataframe = await Helper.ConvertToDataFrame(df);
  dataframe = await Helper.dropColumns(dataframe, columns_not_use);
  // dataframe = await Helper.ShuffleDataFrame(dataframe);
  var [X_train, y_train, X_test, y_test] = await Helper.TrainTestSplitDataFrame(dataframe, train, test, target_name)
  X_train = X_train.toJSON();
  X_test = X_test.toArray();
  y_train = y_train.toJSON();
  y_test = y_test.toArray();
  return [X_train, X_test, y_train, y_test]

}
