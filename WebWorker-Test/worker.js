import * as parseCSV from 'https://deno.land/std@0.82.0/encoding/csv.ts';
import * as denoforge from "https://deno.land/x/denoforge@1.0.0/denoforge.js";


self.onmessage = async (e) => {
  const { filepath, columns_not_use, target_name, train, test} = e.data;
  await Process(filepath, columns_not_use, target_name, train, test);
  console.log("Finished...")
  self.close();
};


async function Process(filepath, columns_not_use, target_name, train, test){
  var df = await Read(filepath);
  var dataframe = ConvertToDataFrame(df);
  var dataframe = dropColumns(dataframe, columns_not_use);
  var dataframe = ShuffleDataFrame(dataframe);
  var [X_train, y_train, X_test, y_test] = TrainTestSplitDataFrame(dataframe, train, test, target_name)
}


async function Read(filename){
  const content = await parseCSV.parse(await Deno.readTextFile(filename), {
      skipFirstRow: true,
  });
  return content;
}

function ShuffleDataFrame(dataframe){
  console.log("Shuffling the dataframe");
  var arrayOfArrays = dataframe.toRows();
  arrayOfArrays.sort(() => Math.random() - 0.5)
  let newdataframe = new denoforge.DataFrame(arrayOfArrays);
  return newdataframe;
}

function TrainTestSplitDataFrame(dataframe, training_percentage, testing_percentage, target_name){
  var train_count = Math.ceil(training_percentage * dataframe.count());
  var testing_count = Math.floor(testing_percentage * dataframe.count()) ;

  var X_train = dataframe.between(0, train_count);
  var X_test = dataframe.between(train_count, dataframe.count());

  var target_column = dataframe.getSeries(target_name);

  // error checking to make sure that the total number of training and testing items does not exceed the total row in the dataframe
  if((train_count + testing_count) > dataframe.count()){
      throw 'TrainTestSplitDataFrame: Training and Testing objects exceed the total number of rows!';
  }

  var y_train = target_column.between(0, train_count);
  var y_test = target_column.between(train_count, dataframe.count());

  console.log("Total data size: ", dataframe.count())
  console.log("training size: ", train_count);
  console.log("testing size: ", testing_count);

  return [X_train, y_train, X_test, y_test]
}

function ConvertToDataFrame(data){
  let dataframe = new denoforge.DataFrame(data);
  return dataframe;
}


function dropColumns(dataframe, columns){
  let newdf = dataframe.dropSeries(columns);
  return newdf;
}
