import {Parser} from "../src/modules.ts";

const output_directory = "./output/";
const newfolder = "newfolder";
const fileName = "output.txt";
const text = "Hello World \n This is a sample output \nEric Miao";

const parser = new Parser();
parser.create_folder(newfolder);

console.log("Parser: "+ output_directory + newfolder + "/" + fileName);
Deno.writeTextFileSync(output_directory + newfolder + "/" + fileName, text);
console.log("completed");