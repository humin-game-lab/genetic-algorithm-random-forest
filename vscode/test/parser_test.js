/*
    Testing for Decision Tree
*/

import {
  ArrayHelper,
  DataFrame,
  Evaluation,
  FastDecisionTree,
  Parser,
  sleep,
} from "../src/modules.ts";

await sleep(1);

const helper = new ArrayHelper();
const evaluation = new Evaluation();

async function decision_tree_with_raw_data() {
  console.log("Test: Decision Tree with raw csv data");
  const datasetConfig = JSON.parse(
    await Deno.readTextFile("./config/config_original_dataset.json")
  );
  let dataset = datasetConfig.breast_cancer_dataset;
  const filePath = dataset.file_name;
  const columns_not_use = dataset.columns_not_use;
  const target_name = dataset.target_name;
  const dataframe = new DataFrame(filePath, target_name, columns_not_use);

  console.log("Dataset Source:", dataset.file_name);
  await dataframe.preprocess();
  const feature_names = dataframe.featureNames;
  dataset = dataframe.dataframe;
  dataset = helper.JSONtoArray(dataset);
  // dataset = helper.shuffle(dataset)

  const [training, testing, actual] = helper.train_test_split(
    dataset,
    0.7,
    0.3,
    false
  );
  const [training_set, tree_feature_names, tree_feature_indexes] =
    helper.get_input_for_decision_tree(
      training,
      feature_names,
      feature_names.length
    );

  const decision_tree = new FastDecisionTree();
  decision_tree.fit(training_set, tree_feature_names, tree_feature_indexes);
  const parser = new Parser();
  parser.parse(decision_tree);
  const predictions = decision_tree.predict(testing);
  const accuracy = evaluation.accuracy_metric(actual, predictions);
  console.log("Accuracy from Decision Tree: ", accuracy);
}

async function decision_tree_with_train_test_data() {
  console.log("Test: Decision Tree with partitioned csv data");
  const config = JSON.parse(
    await Deno.readTextFile("./config/config_for_testing.json")
  );
  const dataset_config = config.breast_cancer_dataset;

  console.log("Dataset Source:", dataset_config.directory);
  const directory = dataset_config.directory;
  const target_name = dataset_config.target_name;
  const columns_not_use = dataset_config.columns_not_use;

  const training_filenames = dataset_config.training_filenames;
  const testing_filenames = dataset_config.testing_filenames;

  const training_filePath = directory + training_filenames[0];
  const testing_filePath = directory + testing_filenames[0];

  const training_df = new DataFrame(
    training_filePath,
    target_name,
    columns_not_use
  );
  const testing_df = new DataFrame(
    testing_filePath,
    target_name,
    columns_not_use
  );

  await training_df.preprocess();
  await testing_df.preprocess();

  const feature_names = training_df.featureNames;
  const training_set = helper.JSONtoArray(training_df.dataframe);
  const testing_set = helper.JSONtoArray(testing_df.dataframe);

  const y_testing = helper.getColumn(testing_set, feature_names.length - 1);
  const x_testing = helper.removeColumn(testing_set, feature_names.length - 1);
  const feature_indexes = [...Array(feature_names.length).keys()];

  const decision_tree = new FastDecisionTree();
  decision_tree.fit(training_set, feature_names, feature_indexes);
  const predictions = decision_tree.predict(x_testing);
  const accuracy = evaluation.accuracy_metric(y_testing, predictions);
  console.log("Accuracy from Decision Tree: ", accuracy);
}

await decision_tree_with_raw_data();
// await decision_tree_with_train_test_data();
