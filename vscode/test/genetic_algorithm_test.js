/*
    Testing for Genetic Algorithm
*/
import{
    ArrayHelper,
    DataFrame,
    FastGeneticAlgorithm,
    sleep
 } from "../src/modules.ts"
 
await sleep(1);


const RF_CONFIG = {
    n_estimators: 50,
    max_features: "sqrt",
    max_depth: 20,
    min_sample_split: 2,
    max_bootstrap_samples: 0.7,
    bootstrap_with_replacement: true
}

const GA_CONFIG = {
    selection_rate: 0.2,
    crossover_rate: 0.2,
    mutation_rate: 0.1,
    diversity_percentage: 0.1,
    crossover_type: "SINGLE",
    max_generation: 50,
    termination_condition: "NONE"
}

const helper = new ArrayHelper();
const datasetConfig = JSON.parse(await Deno.readTextFile("./config/config.json"));
const dataset_name = "breast_cancer_dataset_directory";
const columns_not_use = datasetConfig.columns_not_use;
const target_name = datasetConfig.target_name;
const directory = datasetConfig[dataset_name];

const train = [];
const x_optimization = [];
const y_optimization = [];
const x_test = [];
const y_test = [];
let feature_names = [];

async function read(){
    const training_filenames = datasetConfig.training_filenames;
    const testing_filenames = datasetConfig.testing_filenames;
    const optimization_filenames = datasetConfig.optimization_filenames;
    for(let index = 0; index < 100; index++){
        const training_filename = training_filenames[index];
        const testing_filename = testing_filenames[index]
        const optimization_filename = optimization_filenames[index]

        const training_filePath = directory + training_filename;
        const optimization_filePath = directory + optimization_filename;
        const testing_filePath = directory + testing_filename;

        const training_df = new DataFrame(training_filePath, target_name, columns_not_use);
        const optimization_df = new DataFrame(optimization_filePath, target_name, columns_not_use);
        const testing_df = new DataFrame(testing_filePath, target_name, columns_not_use);

        await training_df.preprocess();
        await optimization_df.preprocess();
        await testing_df.preprocess();
        feature_names = training_df.featureNames;

        const training_set = helper.JSONtoArray(training_df.dataframe);
        const optimization_set = helper.JSONtoArray(optimization_df.dataframe);
        const testing_set = helper.JSONtoArray(testing_df.dataframe);

        const y_opt = helper.getColumn(optimization_set, feature_names.length-1);
        const x_opt = helper.removeColumn(optimization_set, feature_names.length-1);

        const y_testing = helper.getColumn(testing_set, feature_names.length-1);
        const x_testing = helper.removeColumn(testing_set, feature_names.length-1);

        train.push(training_set);
        x_test.push(x_testing);
        y_test.push(y_testing);
        x_optimization.push(x_opt);
        y_optimization.push(y_opt);
    }
}


await read();

const idx = 0;
console.log("\ndataset name: ", dataset_name);
const training_data = train[idx];
const testing_data = x_test[idx];
const testing_target = y_test[idx];
const optimization_data = x_optimization[idx];
const optimization_target = y_optimization[idx];
const GeneticAlgorithm = new FastGeneticAlgorithm(GA_CONFIG, RF_CONFIG);
GeneticAlgorithm.run_GA_on_trees(training_data, feature_names, optimization_data, optimization_target, testing_data, testing_target);
console.log("========== Finished ==========\n");