import { ensureDirSync, existsSync } from "./modules.js";

class Parser {
  constructor(feature_names = [], output_directory = "./output/") {
    this.relations = [];

    this.nodes = [];
    this.feature_names = feature_names;
    this.output_directory = output_directory;
  }

  clear() {
    this.relations = [];
    this.nodes = [];
  }

  get_time() {
    const date_ob = new Date();
    const date = ("0" + date_ob.getDate()).slice(-2);
    const month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
    const hours = date_ob.getHours();
    const minutes = date_ob.getMinutes();
    const seconds = date_ob.getSeconds();
    console.log(
      month + "-" + date + " " + hours + ":" + minutes + ":" + seconds
    );
    return String(
      month + "-" + date + " " + hours + ":" + minutes + ":" + seconds
    );
  }

  create_folder(folder_name) {
    const pathFound = existsSync(this.output_directory + folder_name);
    if (pathFound) {
      console.log(folder_name, " already exists in", this.output_directory);
      return false; // no need to anything
    }
    console.log("Creating ", this.output_directory + folder_name);
    ensureDirSync(this.output_directory + folder_name);
    return true;
  }

  parseToFile(output_path, tree, ifFast = false) {
    this.clear();
    const root = tree.root;
    let text = "";

    if (ifFast) {
      this.process_FAST_tree(root, 0);
      text += "digraph FastDecisionTree { ";
    } else {
      this.process(root, 0);
      text += "digraph DecisionTree { ";
    }

    for (let i = 0; i < this.nodes.length; i++) {
      text += this.nodes[i];
    }

    for (let i = 0; i < this.relations.length; i++) {
      text += this.relations[i];
    }
    text += "}";

    console.log(
      "Parser: .dot Graph file written to " +
        this.output_directory +
        output_path +
        ".dot"
    );
    Deno.writeTextFileSync(this.output_directory + output_path + ".dot", text);
    this.clear();
  }

  parse(tree, ifFast = false) {
    const root = tree.root;
    // this.feature_names = tree.features_names;

    if (ifFast) {
      this.process_FAST_tree(root, 0);
      console.log("digraph FastDecisionTree { ");
    } else {
      this.process(root, 0);
      console.log("digraph DecisionTree { ");
    }

    for (let i = 0; i < this.nodes.length; i++) {
      console.log(this.nodes[i]);
    }

    for (let i = 0; i < this.relations.length; i++) {
      console.log(this.relations[i]);
    }
    console.log("}\n");
    this.clear();
  }

  process(tree, nodeNumber) {
    if (typeof tree != "object") {
      // reach the decision node and create a category node (TRUE OR FALSE)
      const node =
        nodeNumber + '[shape=box, style=filled, label="' + tree + '"]';
      this.nodes.push(node);
      return tree.index;
    }
    const node =
      nodeNumber +
      '[shape=box, style=filled, label=" ' +
      this.feature_names[tree.index] +
      "\n gini: " +
      tree.gini +
      '"]';
    this.nodes.push(node);

    const seed1 = Math.random();
    const seed2 = Math.random();
    return [
      // getting child nodes on the left branch (yes)
      this.relations.push(
        nodeNumber +
          " -> " +
          [
            nodeNumber +
              seed1 +
              '[label = "value' +
              "<" +
              tree.value +
              "\n samples: " +
              tree.groups[0].length +
              '"]',
          ]
      ),
      this.process(tree.left, nodeNumber + seed1),

      // getting child nodes on the right branch (no)
      this.relations.push(
        nodeNumber +
          " -> " +
          [
            nodeNumber +
              seed2 +
              '[label = "value' +
              ">" +
              tree.value +
              "\n samples: " +
              tree.groups[1].length +
              '"]',
          ]
      ),
      this.process(tree.right, nodeNumber + seed2),
    ];
  }

  process_FAST_tree(tree, nodeNumber) {
    if (typeof tree != "object") {
      // reach the decision node and create a category node (TRUE OR FALSE)
      const node =
        nodeNumber + '[shape=box, style=filled, label="' + tree + '"]';
      this.nodes.push(node);
      return tree.index;
    }
    const node =
      nodeNumber +
      '[shape=box, style=filled, label=" ' +
      this.feature_names[tree.index] +
      "\n gini: " +
      tree.gini +
      '"]';
    this.nodes.push(node);

    const seed1 = Math.random();
    const seed2 = Math.random();

    return [
      // getting child nodes on the left branch (yes)
      this.relations.push(
        nodeNumber +
          " -> " +
          [
            nodeNumber +
              seed1 +
              '[label = "value' +
              "<" +
              tree.value +
              "\n groups: " +
              tree.groups[0] +
              '"]',
          ]
      ),
      this.process_FAST_tree(tree.left, nodeNumber + seed1),

      // getting child nodes on the right branch (no)
      this.relations.push(
        nodeNumber +
          " -> " +
          [
            nodeNumber +
              seed2 +
              '[label = "value' +
              ">" +
              tree.value +
              "\n groups: " +
              tree.groups[1] +
              '"]',
          ]
      ),
      this.process_FAST_tree(tree.right, nodeNumber + seed2),
    ];
  }
}

export default { Parser };
