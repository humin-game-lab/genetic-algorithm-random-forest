import {
  ArrayHelper,
  Evaluation,
  OrderedMap,
  Parser,
  FastRandomForest,
  FastDecisionTree,
} from "./models.js";

import "https://deno.land/x/lodash@4.17.19/vendor/underscore/underscore.js";

/*
params:
     selection_rate:  selection ratio
     crossover_rate:  crossover ratio
     mutation_rate:   mutation ratio
     crossover_type:  type of the crossover operator
     max_generation:  maximum number of generation/epochs
*/

class FastGeneticAlgorithm {
  constructor(GA_CONFIG, RF_CONFIG) {
    // training, testing, and optimization set

    this.training_set = [];
    this.feature_names = [];

    this.optimization_set = [];
    this.optimization_target = [];

    this.testing_set = [];
    this.testing_target = [];

    this.current_population = null;
    this.accuracy = [];
    this.generation_counter = 0;

    this.evolution_cache = new OrderedMap();
    this.evolution_log = new OrderedMap();

    // Config files for GA and RF
    this.GA_CONFIG = GA_CONFIG;
    this.RF_CONFIG = RF_CONFIG;

    this.helper = new ArrayHelper();
    this.evaluation = new Evaluation();
    this.set_params();
  }

  set_params() {
    // parameters for the Genetic Algorithm model
    this._identifier = this.GA_CONFIG.config_description;
    this.feature_names = this.GA_CONFIG.feature_names;
    this.selection_rate = this.GA_CONFIG.selection_rate;
    this.crossover_rate = this.GA_CONFIG.crossover_rate;
    this.mutation_rate = this.GA_CONFIG.mutation_rate;
    this.crossover_type = this.GA_CONFIG.crossover_type;
    this.max_generation = this.GA_CONFIG.max_generation;
    this.termination_condition = this.GA_CONFIG.termination_condition;

    this.diversity_percentage = this.GA_CONFIG.diversity_percentage;
    this.restart_condition = 3;

    this.output_directory = this.GA_CONFIG.debug_config.output_directory;
    this.start_time = this.GA_CONFIG.debug_config.timestamp_start;
    this.memory_log = this.GA_CONFIG.debug_config.memory_stamp;
    this.output_folder = this.GA_CONFIG.debug_config.output_folder;
  }

  run_GA_on_forest(
    train,
    x_optimization,
    y_optimization,
    x_test,
    y_test,
    verbose = false
  ) {
    this.set_and_create_output_dir();
    this.initialize(
      train,
      x_optimization,
      y_optimization,
      x_test,
      y_test,
      verbose
    );
    const initial_accuracy = this.get_cur_accuracy(false);
    this.evaluate(false);
    this.accuracy.push({
      epoch: this.generation_counter,
      performance: initial_accuracy,
    });

    while (this.generation_counter < this.max_generation) {
      const accuracy_current_population = this.get_cur_accuracy(false);
      this.evaluate(verbose);
      this.remove_bad_trees();
      // this.print_current_population("After remove bad trees:");
      const accuracy_after_remove = this.get_cur_accuracy(false);
      this.add_diversity_from_n_forest(1);
      const accuracy_after_add = this.get_cur_accuracy(false);
      // this.print_current_population("After adding good trees:");
      console.log(
        `Epoch ${this.generation_counter}: `,
        accuracy_current_population,
        "->",
        accuracy_after_remove,
        "->",
        accuracy_after_add
      );
      this.find_tree_accuracy();
      temp_accuracies.push(accuracy_after_add);
      this.generation_counter += 1;
    }
  }

  set_and_create_output_dir() {
    this.helper.create_folder(this.output_directory, this.output_folder);
  }

  run_GA_on_trees(
    train,
    x_optimization,
    y_optimization,
    x_test,
    y_test,
    verbose = false
  ) {
    this.set_and_create_output_dir();
    this.initialize(
      train,
      x_optimization,
      y_optimization,
      x_test,
      y_test,
      verbose
    );
    const initial_accuracy = this.get_cur_accuracy(false);
    this.evaluate(false);
    this.accuracy.push({
      epoch: this.generation_counter,
      performance: initial_accuracy,
    });
    while (this.generation_counter < this.max_generation) {
      const [best_trees, worst_trees] = this.fitness();
      this.crossover(best_trees, worst_trees, verbose);
      this.mutation(verbose);

      // this.add_diversity_from_n_forest(10);
      // this.replace_bad_trees_with_new_trees();

      this.generation_counter += 1;
      const accuracy = this.get_cur_accuracy(false);
      this.accuracy.push({
        epoch: this.generation_counter,
        performance: accuracy,
      });
      this.evaluate(false);
      this.find_tree_accuracy();
    }

    this.analyze_log();
    this.output();
    // console.log("log:",this.evolution_log);
    this.evolution_log.write_log_to_file(
      this.output_directory + this.output_folder,
      this._identifier
    );
    console.log("");
  }

  analyze_log() {
    const [best_performance_id, best_accuracy_id] = this.find_tree_accuracy();
    const models = this.evolution_cache.get_models_by_key(best_performance_id);
    const accuracy_from_evolving = [];

    for (let i = 0; i < models.length; i++) {
      const predicted = models[i].predict(this.testing_set);
      const cur_accuracy = this.evaluation.accuracy_metric(
        predicted,
        this.testing_target
      );
      accuracy_from_evolving.push(cur_accuracy);
    }

    // console.log("Accuracy - Best Performance Tree (evaluated using OPT set): \n", accuracy_from_evolving)
    // this.evolution_cache.print("tree id: ", best_performance_id)
    // console.log();

    const accuracy_from_evolving2 = [];
    const models2 = this.evolution_cache.get_models_by_key(best_accuracy_id);
    for (let i = 0; i < models2.length; i++) {
      const predicted = models2[i].predict(this.testing_set);
      const cur_accuracy = this.evaluation.accuracy_metric(
        predicted,
        this.testing_target
      );
      accuracy_from_evolving2.push(cur_accuracy);
    }
    // console.log("Accuracy - Best Accuracy Tree (evaluated using Testing set): \n", accuracy_from_evolving2);
    // this.evolution_cache.print("tree id: ", best_accuracy_id);
    // console.log();

    this.snapshot_tree_evolution(best_performance_id);
    this.snapshot_tree_evolution(best_accuracy_id);

    this.evolution_log.print_by_key(best_performance_id);
    this.evolution_log.print_by_key(best_accuracy_id);
  }

  find_tree_accuracy() {
    const sorted_trees = this.current_population.get_sorted_trees();
    const tree_accuracy = [];
    const tree_performance = [];
    for (let i = 0; i < sorted_trees.length; i++) {
      const cur_tree = sorted_trees[i];
      const cur_tree_accuracy = cur_tree.model.get_performance(
        this.testing_set,
        this.testing_target
      );
      const cur_tree_performance = cur_tree.model.get_performance(
        this.optimization_set,
        this.optimization_target
      );
      tree_accuracy.push({ id: cur_tree.id, accuracy: cur_tree_accuracy });
      tree_performance.push({
        id: cur_tree.id,
        performance: cur_tree_performance,
      });
    }

    const sorted_tree_accuracy = _.sortBy(tree_accuracy, "accuracy");
    const sorted_tree_performance = _.sortBy(tree_performance, "accuracy");

    console.log(
      "Best DT Performance: ",
      sorted_tree_performance[sorted_tree_performance.length - 1]
    );
    console.log(
      "Best DT Accuracy: ",
      sorted_tree_accuracy[sorted_tree_accuracy.length - 1]
    );
    return [
      sorted_tree_performance[sorted_tree_performance.length - 1].id,
      sorted_tree_accuracy[sorted_tree_accuracy.length - 1].id,
    ];
  }

  snapshot_best_trees() {
    console.log("Here: snapshot_best_trees");
    const sorted_trees = this.current_population.get_sorted_trees();
    for (let i = 0; i < 2; i++) {
      const parser = new Parser(
        this.feature_names,
        this.output_path + this.output_directory + "/"
      );
      const cur_tree = sorted_trees[sorted_trees.length - i - 1];
      const output_filepath =
        "G" + this.generation_counter + "_ACC_" + cur_tree.performance;
      parser.parseToFile(output_filepath, cur_tree.model, true);
    }
  }

  snapshot_tree_evolution(id) {
    const parser = new Parser(
      this.feature_names,
      this.output_directory + this.output_folder + "/"
    );
    const models = this.evolution_cache.get_nodes_by_key(id);
    const folder_name = "treeID_" + String(id);

    parser.create_folder(folder_name);
    for (let i = 0; i < models.length; i++) {
      const cur_model = models[i];
      const output_filepath =
        folder_name + "/" + "Gen" + cur_model[0] + "_Acc_" + cur_model[1];
      parser.parseToFile(output_filepath, cur_model[2], true);
    }
  }

  print_current_population(optional_prompt = "") {
    console.log(
      "\n============================= Current Population ============================="
    );
    console.log(`Epoch ${this.generation_counter}: ${optional_prompt}`);
    for (let i = 0; i < this.current_population.trees.length; i++) {
      console.log(
        "id: ",
        this.current_population.trees[i].id,
        "performance:",
        this.current_population.trees[i].performance
      );
    }
    console.log();
  }

  output() {
    console.log("\nSimple GA Stat for : ");
    const x = _.sortBy(this.accuracy, "performance");
    console.log("Accuracy: ", x[0], " -> ", x[this.accuracy.length - 1]);
    // console.log("Accuracy from all epochs: ", this.accuracy);
  }

  initialize(
    train,
    x_optimization,
    y_optimization,
    x_test,
    y_test,
    verbose = false
  ) {
    // store our initial training set
    this.training_set = [...train];

    this.optimization_set = [...x_optimization];
    this.optimization_target = [...y_optimization];

    this.testing_set = [...x_test];
    this.testing_target = [...y_test];

    if (verbose) {
      console.log("training_set: ", this.training_set.length);
      console.log("testing_set: ", this.testing_set.length);
      console.log("optimization_set: ", this.optimization_set.length);
    }

    // train the Random Forest model
    const randomForestClassifier = this.build_forest();
    // assign it as the current population
    const forest_copy = new FastRandomForest(this.RF_CONFIG);
    forest_copy.parse_forest(randomForestClassifier);
    this.current_population = forest_copy;
  }

  // Evaluate: evaluate the Random Forest model performance using the optimization dataset
  evaluate(verbose = false) {
    this.current_population.evaluate(
      this.optimization_set,
      this.optimization_target,
      verbose
    );
  }

  // Accuracy: calculate the Random Forest model performance using the testing dataset
  get_cur_accuracy(verbose = false) {
    const accuracy = this.current_population.get_accuracy(
      this.testing_set,
      this.testing_target,
      false
    );
    if (verbose) {
      console.log(
        `Generation ${this.generation_counter} Accuracy: ${accuracy} `
      );
    }
    return accuracy;
  }

  fitness() {
    let trees_in_cur_population = this.current_population.trees;
    trees_in_cur_population = _.sortBy(trees_in_cur_population, "performance");
    const selection_number =
      trees_in_cur_population.length * this.selection_rate;
    const best_trees = [];
    const worst_trees = [];
    for (let i = 0; i < selection_number; i++) {
      worst_trees.push(trees_in_cur_population[i]);
      best_trees.push(
        trees_in_cur_population[trees_in_cur_population.length - i - 1]
      );
    }
    return [best_trees, worst_trees];
  }

  build_forest(config = this.RF_CONFIG) {
    // create the first population
    const randomForestClassifier = new FastRandomForest(config);
    // train the Random Forest model
    randomForestClassifier.fit(this.training_set, this.feature_names);
    randomForestClassifier.evaluate(
      this.optimization_set,
      this.optimization_target
    );
    return randomForestClassifier;
  }

  remove_bad_trees() {
    const sorted_trees = this.current_population.get_sorted_trees();
    const bad_tree_number = Math.floor(
      this.diversity_percentage * this.RF_CONFIG.n_estimators
    );
    for (let i = 0; i < bad_tree_number; i++) {
      this.current_population.pop_tree_by_id(sorted_trees[i].id);
    }
  }

  check_if_stop_improving() {
    if (this.accuracy.length > this.restart_condition) {
      let decrease_counter = 0;
      for (let i = 0; i < this.restart_condition; i++) {
        if (
          this.accuracy[this.accuracy.length - i - 1].performance >=
          this.accuracy[this.accuracy.length - i - 2].performance
        ) {
          decrease_counter += 1;
        }
      }
      if (decrease_counter === this.restart_condition) {
        return true;
      }
    }
    return false;
  }

  add_diversity() {
    const new_forest = this.build_forest();
    const sorted_trees = new_forest.get_sorted_trees();
    const best_trees = [];
    const selection_number = Math.floor(
      this.RF_CONFIG.n_estimators * this.diversity_percentage
    );
    for (let i = 0; i < selection_number; i++) {
      best_trees.push(sorted_trees[sorted_trees.length - i - 1]);
    }
    this.current_population.add_new_trees(best_trees);
  }

  // adding new trees from n forest
  add_diversity_from_n_forest(n = 10) {
    const config = {
      n_estimators: 100,
      max_features: "sqrt",
      max_depth: 20,
      min_sample_split: 2,
      max_bootstrap_samples: 0.7,
      bootstrap_with_replacement: true,
    };

    const new_unsorted_trees = [];
    const selection_number = Math.floor(
      this.RF_CONFIG.n_estimators * this.diversity_percentage
    );
    for (let i = 0; i < n; i++) {
      const new_forest = this.build_forest(config);
      const sorted_trees = new_forest.get_sorted_trees();
      for (let i = 0; i < selection_number; i++) {
        new_unsorted_trees.push(sorted_trees[sorted_trees.length - i - 1]);
      }
    }

    const best_trees = [];
    const sorted_trees = _.sortBy(new_unsorted_trees, "performance");
    for (let i = 0; i < selection_number; i++) {
      best_trees.push(sorted_trees[sorted_trees.length - i - 1]);
    }
    this.current_population.add_new_trees(best_trees);
  }

  replace_bad_trees_with_new_trees() {
    const newForest = this.build_forest();
    const sorted_trees = newForest.get_sorted_trees();
    const current_sorted_trees = this.current_population.get_sorted_trees();
    const bad_trees_ids = [];
    const best_trees = [];

    const selection_number = newForest.trees.length * this.diversity_percentage;
    for (let i = 0; i < selection_number; i++) {
      bad_trees_ids.push(current_sorted_trees[i].id);
      best_trees.push(sorted_trees[sorted_trees.length - i - 1]);
    }

    this.current_population.remove_bad_trees(bad_trees_ids);
    this.current_population.add_trees_with_ids(best_trees, bad_trees_ids);
    // console.log("highest performance tree:", this.current_population.bestTrees[0].performance);
  }

  terminate(verbose = false) {
    // we need at least 2 populations
    if (this.generation_counter < 3) {
      return false;
    }

    // if the maximum number of generation equals to the number of generations we have
    if (this.generation_counter >= this.max_generation) {
      return true;
    }

    // Termination condition 1: Compare the average performance from [0, previous population] and the average performance from [initial_population, current population]
    // Continue only if Avg_Accuracy[i] > Avg_Accuracy[i-1]
    if (this.termination_condition === "OPTIMAL") {
      const prev_avg = this.helper.get_average_from_objects(
        this.accuracy.slice(0, this.generation_counter - 1),
        "performance"
      );
      const cur_avg = this.helper.get_average_from_objects(
        this.accuracy,
        "performance"
      );
      return prev_avg > cur_avg;
    }

    if (this.termination_condition === "NONE") {
      return false;
    }

    // Termination condition 2: an Integer shows the maximum number of decreasing population we are allowed
    // Terminate if Accuracy[i] < Accuracy[i-1] < .... < Accuracy[i-n]
    // check if our genetic algorithm stop improve our current population
    if (this.accuracy.length > this.termination_condition) {
      let decrease_counter = 0;
      for (let i = 0; i < this.termination_condition; i++) {
        if (
          this.accuracy[this.accuracy.length - i - 1].performance >
          this.accuracy[this.accuracy.length - i - 2].performance
        ) {
          decrease_counter += 1;
        }
      }
      if (decrease_counter === this.termination_condition) {
        return true;
      }
    }
    return false;
  }

  mutation(verbose = false) {
    // getting the number of mutation on the current forest
    const forest_size = this.current_population.trees.length;
    const number_of_mutation = forest_size * this.mutation_rate;

    // for each mutation:
    if (verbose) {
      console.log(
        "Mutating",
        number_of_mutation,
        "genes from the current population"
      );
    }

    const mutatedIndex = [];
    for (let i = 0; i < number_of_mutation; i++) {
      // console.log("mutating: Gen:", this.generation_counter, " - ", i, number_of_mutation)
      const mutate_from_index = Math.floor(
        Math.random() * this.current_population.trees.length
      );
      const mutate_from_tree =
        this.current_population.pop_tree_by_index(mutate_from_index);

      const mutate_to_index = Math.floor(
        Math.random() * this.current_population.trees.length
      );
      const mutate_to_tree =
        this.current_population.pop_tree_by_index(mutate_to_index);

      mutatedIndex.push(mutate_to_index);
      const [tree1, tree2] = this.random_mutation(
        mutate_from_tree,
        mutate_to_tree,
        false
      );
      tree1.performance = tree1.model.get_performance(
        this.optimization_set,
        this.optimization_target
      );
      tree2.performance = tree2.model.get_performance(
        this.optimization_set,
        this.optimization_target
      );
      // this.current_population.add_trees([tree1, tree2]);

      this.current_population.modify_tree(tree1);
      this.current_population.modify_tree(tree2);
      // console.log("Mutation -> Modifying: ",tree1.id, tree2.id);
      this.add_to_cache(tree1);
      this.add_to_cache(tree2);

      const log_1 = {
        epoch: this.generation_counter,
        action: "MUTATION",
        mutated_from: tree2.id,
      };
      this.add_to_log(tree1.id, log_1);

      const log_2 = {
        epoch: this.generation_counter,
        action: "MUTATION",
        mutated_from: tree1.id,
      };
      this.add_to_log(tree2.id, log_2);
    }
    if (verbose) {
      console.log("Mutated Genes: ", mutatedIndex);
    }
  }

  random_mutation(tree1, tree2, verbose = false) {
    // now we find where do we swap branches
    let non_leaf_nodes_tree1 = tree1.model.find_available_nodes();
    let non_leaf_nodes_tree2 = tree2.model.find_available_nodes();

    // shuffle the possible split point
    non_leaf_nodes_tree1 = this.helper.shuffle(non_leaf_nodes_tree1);
    non_leaf_nodes_tree2 = this.helper.shuffle(non_leaf_nodes_tree2);

    // decide which tree should be the mutated tree
    const tree1_split_node_index = Math.floor(
      Math.random() * non_leaf_nodes_tree1.length
    );
    const tree2_split_node_index = Math.floor(
      Math.random() * non_leaf_nodes_tree2.length
    );

    const tree1_split_node = non_leaf_nodes_tree1[tree1_split_node_index];
    const tree2_split_node = non_leaf_nodes_tree2[tree2_split_node_index];

    // choose tree1 as the mutated tree if random number is [0,0.5)
    // choose tree2 as the mutated tree if random number is [0.5,1)
    let improve = false;
    const max_try = 10;
    let counter = 0;

    while (!improve && counter < max_try) {
      // making a copy of both trees
      const tree1_copy = new FastDecisionTree();
      const tree2_copy = new FastDecisionTree();
      tree1_copy.parse(tree1.model);
      tree2_copy.parse(tree2.model);

      const choice = Math.random();
      if (choice < 0.5) {
        tree1_copy.find_and_replace_node(
          tree1_copy.root,
          tree1_split_node,
          tree2_split_node,
          true
        );
        improve = this.evaluation.compare_two_trees(
          this.optimization_set,
          this.optimization_target,
          tree1.model,
          tree1_copy,
          verbose
        );
        // improve = this.evaluation.compare_two_trees(this.optimization_set, this.optimization_target, tree1.model, tree1_copy, verbose);
        if (improve) {
          tree1.model.find_and_replace_node(
            tree1.model.root,
            tree1_split_node,
            tree2_split_node,
            true
          );
          break;
        }
      } else {
        tree2_copy.find_and_replace_node(
          tree2_copy.root,
          tree2_split_node,
          tree1_split_node,
          true
        );
        improve = this.evaluation.compare_two_trees(
          this.optimization_set,
          this.optimization_target,
          tree2.model,
          tree2_copy,
          verbose
        );
        if (improve) {
          tree2.model.find_and_replace_node(
            tree2.model.root,
            tree2_split_node,
            tree1_split_node,
            true
          );
          break;
        }
      }
      counter += 1;
    }
    return [tree1, tree2];
  }

  // Crossover operator
  crossover(best_trees, worst_trees, verbose = false) {
    const current_gen_best_trees = best_trees;
    const current_gen_worst_trees = worst_trees;

    const crossover_number = Math.floor(
      this.current_population.trees.length * this.crossover_rate
    );

    if (verbose) {
      console.log(
        "Crossover",
        crossover_number,
        "genes from the current population"
      );
    }

    for (let i = 0; i < crossover_number; i++) {
      let child = null;
      // using single crossover technique
      if (this.crossover_type === "SINGLE") {
        let improve = false;
        const max_try = 10;
        let counter = 0;

        const good_tree_index = Math.floor(
          Math.random() * current_gen_best_trees.length
        );
        const bad_tree_index = Math.floor(
          Math.random() * current_gen_worst_trees.length
        );

        const good_tree = this.current_population.get_tree_by_id(
          current_gen_best_trees[good_tree_index].id
        );
        const bad_tree = this.current_population.pop_tree_by_id(
          current_gen_worst_trees[bad_tree_index].id
        );

        while (!improve && counter < max_try) {
          child = this.single_crossover(
            good_tree,
            bad_tree,
            this.crossover_type
          );
          improve = this.evaluation.compare_two_trees(
            this.optimization_set,
            this.optimization_target,
            bad_tree.model,
            child.model
          );
          counter += 1;
        }

        const child_performance = child.model.get_performance(
          this.optimization_set,
          this.optimization_target
        );
        // const good_tree_performance = good_tree.model.get_performance(this.optimization_set, this.optimization_target);

        child.performance = child_performance;
        // good_tree.performance = good_tree_performance;

        // console.log("Crossover -> Modifying: ",child.id);
        this.current_population.modify_tree(child);
        this.add_to_cache(child);

        const log = {
          epoch: this.generation_counter,
          action: "CROSSOVER",
          crossover_from: good_tree.id,
        };
        this.add_to_log(child.id, log);
      }
    }
  }

  add_to_cache(modified_tree) {
    const node = [
      this.generation_counter,
      modified_tree.performance,
      modified_tree.model,
    ];
    this.evolution_cache.set(modified_tree.id, node);
  }

  add_to_log(entry_key, log) {
    this.evolution_log.set(entry_key, log);
  }

  // tree1 is the good tree and tree2 is the bad tree
  single_crossover(tree1, tree2, criteria = "RANDOM") {
    // now we find where do we swap branches
    let non_leaf_nodes_tree1 = tree1.model.find_available_nodes();
    let non_leaf_nodes_tree2 = tree2.model.find_available_nodes();

    // shuffle the possible split point
    non_leaf_nodes_tree1 = this.helper.shuffle(non_leaf_nodes_tree1);
    non_leaf_nodes_tree2 = this.helper.shuffle(non_leaf_nodes_tree2);

    // decide which tree should be the mutated tree
    const tree1_split_node_index = Math.floor(
      Math.random() * non_leaf_nodes_tree1.length
    );
    const tree2_split_node_index = Math.floor(
      Math.random() * non_leaf_nodes_tree2.length
    );

    const tree1_split_node = non_leaf_nodes_tree1[tree1_split_node_index];
    const tree2_split_node = non_leaf_nodes_tree2[tree2_split_node_index];

    const child = {
      id: tree2.id,
      model: new FastDecisionTree(),
      performance: 0.0,
    };
    const tree2_copy = JSON.parse(JSON.stringify(tree2.model));
    child.model.parse(tree2_copy);
    child.model.find_and_replace_node(
      child.model.root,
      tree2_split_node,
      tree1_split_node,
      false
    );
    return child;
  }

  double_crossover(tree1, tree2, criteria = "RANDOM") {}
}

export { FastGeneticAlgorithm };
