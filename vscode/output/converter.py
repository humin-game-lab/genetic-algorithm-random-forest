import pydot
import os
import sys

def dotToPNG(directory):
    dotsFiles = [f for f in os.listdir(directory)]
    for file in dotsFiles:
        tokens = os.path.splitext(file)
        print(tokens)
        if tokens[1] == ".dot":
            print("converting: ", file, "to", tokens[0]+ ".png")
            (graph,) = pydot.graph_from_dot_file(directory + "/" + file)
            graph.write_png((directory + "/" + tokens[0] + ".png"))

if __name__ == "__main__":
    print(f"Arguments count: {len(sys.argv)}")
    for i, arg in enumerate(sys.argv):
        if i > 0:
            print("Converting .dot files in ", arg, "...")
            dotToPNG(arg)
        
        