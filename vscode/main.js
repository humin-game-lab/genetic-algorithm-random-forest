import { ArrayHelper, DataFrame, FastGeneticAlgorithm } from "./src/models.js";
import { sleep } from "./src/modules.js";
await sleep(1);

const helper = new ArrayHelper();
const datasetConfig = JSON.parse(
  await Deno.readTextFile("./config/config.json")
);

/*
    Testing for GARF when RF is considered a population
 */

const RF_CONFIG = {
  n_estimators: 100,
  max_features: "sqrt",
  max_depth: 20,
  min_sample_split: 2,
  max_bootstrap_samples: 0.7,
  bootstrap_with_replacement: true,
};

const GA_CONFIG = {
  config_description: "",
  selection_rate: 0.2,
  crossover_rate: 0.2,
  mutation_rate: 0.1,
  diversity_percentage: 0.1,
  crossover_type: "SINGLE",
  max_generation: 50,
  termination_condition: "NONE",
  feature_names: [],

  debug_config: {
    output_directory: "./output/",
    output_folder: null,
    timestamp_start: null,
    timestamp_end: null,
    memory_stamp: [],
  },
};

const dataset_names = ["breast_cancer_dataset", "john_wise_dataset"];

const dataset_name = dataset_names[1];
const columns_not_use = datasetConfig.columns_not_use;
const target_name = datasetConfig.target_name;
const directory = datasetConfig[dataset_name];

const train = [];
const x_optimization = [];
const y_optimization = [];
const x_test = [];
const y_test = [];
let feature_names = [];

async function read() {
  const training_filenames = datasetConfig.training_filenames;
  const testing_filenames = datasetConfig.testing_filenames;
  const optimization_filenames = datasetConfig.optimization_filenames;

  for (let index = 0; index < 100; index++) {
    const training_filename = training_filenames[index];
    const testing_filename = testing_filenames[index];
    const optimization_filename = optimization_filenames[index];

    const training_filePath = directory + training_filename;
    const optimization_filePath = directory + optimization_filename;
    const testing_filePath = directory + testing_filename;

    const training_df = new DataFrame(
      training_filePath,
      target_name,
      columns_not_use
    );
    const optimization_df = new DataFrame(
      optimization_filePath,
      target_name,
      columns_not_use
    );
    const testing_df = new DataFrame(
      testing_filePath,
      target_name,
      columns_not_use
    );

    await training_df.preprocess();
    await optimization_df.preprocess();
    await testing_df.preprocess();
    feature_names = training_df.featureNames;

    const training_set = helper.JSONtoArray(training_df.dataframe);
    const optimization_set = helper.JSONtoArray(optimization_df.dataframe);
    const testing_set = helper.JSONtoArray(testing_df.dataframe);

    const y_opt = helper.getColumn(optimization_set, feature_names.length - 1);
    const x_opt = helper.removeColumn(
      optimization_set,
      feature_names.length - 1
    );

    const y_testing = helper.getColumn(testing_set, feature_names.length - 1);
    const x_testing = helper.removeColumn(
      testing_set,
      feature_names.length - 1
    );

    train.push(training_set);
    x_test.push(x_testing);
    y_test.push(y_testing);
    x_optimization.push(x_opt);
    y_optimization.push(y_opt);
  }
}

function run(training_data, x_test, y_test, x_opt, y_opt) {
  const GA = new FastGeneticAlgorithm(GA_CONFIG, RF_CONFIG);
  GA.run_GA_on_trees(training_data, x_opt, y_opt, x_test, y_test);
}

function get_time() {
  const date_ob = new Date();
  const date = ("0" + date_ob.getDate()).slice(-2);
  const month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
  const hours = date_ob.getHours();
  const minutes = date_ob.getMinutes();
  const seconds = date_ob.getSeconds();
  return String(
    month + "-" + date + "_" + hours + ":" + minutes + ":" + seconds
  );
}

await read();

console.log("\ndataset name: ", dataset_name);
for (let idx = 0; idx < 1; idx++) {
  console.log(
    "\n========== Running: ",
    dataset_name,
    " Sample#" + idx + " =========="
  );

  GA_CONFIG.feature_names = feature_names;
  GA_CONFIG.debug_config.timestamp_start = new Date().toLocaleTimeString();
  GA_CONFIG.debug_config.memory_stamp.push({
    initial_memory: Deno.memoryUsage(),
  });
  GA_CONFIG.debug_config.output_folder =
    dataset_name + String(idx) + "_" + String(get_time());
  GA_CONFIG.config_description =
    dataset_name +
    "_S" +
    String(idx) +
    "_" +
    GA_CONFIG.debug_config.timestamp_start;

  const training_data = train[idx];
  const testing_data = x_test[idx];
  const testing_target = y_test[idx];
  const optimization_data = x_optimization[idx];
  const optimization_target = y_optimization[idx];
  run(
    training_data,
    testing_data,
    testing_target,
    optimization_data,
    optimization_target
  );
}
console.log("========== Finished ==========\n");
