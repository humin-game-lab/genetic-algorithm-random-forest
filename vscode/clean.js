#!/usr/bin/env deno run --allow-run --allow-write

const p = Deno.run({
  cmd: ["deno", "info"],
  stdout: "piped",
  env: { NO_COLOR: "1" }, // No color in output
});
const status = await p.status();
if (!status.success) {
  Deno.exit(status.code);
}
const t = new TextDecoder();
const output = t.decode(await p.output());
p.close();

const m = output.match(/Remote modules cache: "([^"]+)"/);
if (!m) {
  console.log("Remote modules cache not found");
  Deno.exit(1);
}
const folder = m[1];
if (confirm(`Are you sure you want to delete "${folder}"?`)) {
  console.log("deleting...");
  Deno.remove(folder, { recursive: true });
}
