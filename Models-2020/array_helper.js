class ArrayHelper {

    merge2DArrays(clusters) {
        let result = []
        for (const group of clusters) {
            for (const row of group) {
                result.push(row);
            }
        }
        return result
    }


    cross_validation_split(dataset, n_folds) {
        let dataset_split = []
        // make a deep copy
        let dataset_copy = JSON.parse(JSON.stringify(dataset));

        // shuffle
        dataset_copy = this.shuffle(dataset_copy)

        // split into k splits
        let fold_size = Math.floor(dataset.length / n_folds);
        for (let i = 0; i < n_folds; i++) {
            let fold = []
            while (fold.length < fold_size) {
                let index = Math.floor(Math.random() * dataset_copy.length);
                fold.push(dataset_copy[index]);
                dataset_copy.splice(index, 1);
            }
            dataset_split.push(fold);
        }

        let k_Fold_Cross_Validation = []
        for(let i = 0; i < dataset_split.length; i++){
            let dataset_split_copy = JSON.parse(JSON.stringify(dataset_split));
            let testing = dataset_split_copy.splice(i, 1);
            let training = dataset_split_copy;
            training = [].concat.apply([], training);
            testing = [].concat.apply([], testing);
            let testing_target = this.getColumn(testing, testing[0].length - 1);
            k_Fold_Cross_Validation.push(
                {
                    training_set: training,
                    testing_set: testing,
                    testing_target: testing_target
                }
            );
        }

        return k_Fold_Cross_Validation;
    }


    train_test_validation_split(data, training_ratio, testing_ratio, validation_ratio, verbose = true){
        let dataset = JSON.parse(JSON.stringify(data));
        let training_count = dataset.length * training_ratio;
        let testing_count = dataset.length * testing_ratio;
        let validation_count = dataset.length * validation_ratio;


        dataset = this.shuffle(dataset);
        dataset = this.shuffle(dataset);


        let training = dataset.slice(0, training_count);
        let testing = dataset.slice(training_count, training_count + testing_count);
        let validation = dataset.slice(training_count + testing_count, training_count + testing_count + validation_count);


        const arrayColumn = (arr, n) => arr.map((x) => x[n]);
        let testing_target = arrayColumn(testing, testing[0].length - 1);
        let validation_target = arrayColumn(validation, validation[0].length - 1);

        if (verbose) {
            console.log("Training:", training.length)
            console.log("testing:", testing.length, " testing_target", testing_target.length);
            console.log("validation:", validation.length, " validation_target", validation_target.length);
        }

        testing = this.removeColumn(testing, testing[0].length-1);
        validation = this.removeColumn(validation, testing[0].length-1);
        return [training, testing, testing_target, validation, validation_target];
    }



    train_test_split(data, training_ratio, testing_ratio, verbose = false) {
        let dataset = JSON.parse(JSON.stringify(data));

        let training_count = dataset.length * training_ratio
        let testing_count = dataset.length * testing_ratio
        dataset = this.shuffle(dataset)
        let training = dataset.slice(0, training_count)
        let testing = dataset.slice(training_count, training_count + testing_count)
        const arrayColumn = (arr, n) => arr.map((x) => x[n]);
        let actual = arrayColumn(testing, testing[0].length - 1)
        if (verbose) {
            console.log("Training:", training.length)
            console.log("testing:", testing.length)
            console.log("Target:", actual.length)
        }
        testing = this.removeColumn(testing, testing[0].length-1)
        return [training, testing, actual]
    }



    get_input_for_decision_tree(training_set, feature_names_all, number_of_features_per_tree){

        // the actual number of feature on each decision tree
        let feature_count = feature_names_all.length - number_of_features_per_tree;

        // make a copy of the training dataset and make a copy of the feature names
        let training = JSON.parse(JSON.stringify(training_set));
        let feature_names = JSON.parse(JSON.stringify(feature_names_all));

        // map each feature name to an index
        let feature_indexes = [...Array(feature_names.length).keys()];


        for(let i = 0; i < feature_count; i++){
            // randomly select an index as the feature to remove
            let index = Math.floor(Math.random() * (training[0].length-1));

            // remove the column
            training = this.removeColumn(training, index);

            // remove the feature name from the feature names array
            feature_names.splice(index, 1);

            // remove the feature index from the feature index array
            feature_indexes.splice(index,1)
        }


        // return training set, feature names in this training set, and feature indexes
        return [training, feature_names, feature_indexes]
    }


    getColumn(rows, index = rows[0].length - 1) {
        const arrayColumn = (arr, n) => arr.map((x) => x[n]);
        return arrayColumn(rows, index);
    }


    clearColumn(rows, columnIndex) {
        let newRows = []
        for (const row of rows) {
            let newRow = [...row]
            newRow[columnIndex] = null;
            newRows.push(newRow)
        }
        return newRows;
    }


    get_average_from_objects(array, key){
        var array_key = _.map(array, _.property(key));
        const average = (array_key) => array_key.reduce((a, b) => a + b) / array_key.length;
        return average(array_key);
    }


    shuffle(dataset) {
        let dataset_copy = JSON.parse(JSON.stringify(dataset))
        // console.log("Shuffling the dataset");
        dataset_copy.sort(() => Math.random() - 0.5)
        return dataset_copy;
    }


    JSONtoArray(JSON_data) {
        let dataset = []
        for (const row of JSON_data) {
            let result = [];
            let keys = Object.keys(row);
            keys.forEach(function (key) {
                result.push(row[key]);
            });
            dataset.push(result)
        }
        return dataset;
    }


    convertDatasetToFloat(dataset, targetIndex) {
        for (let i = 0; i < dataset[0].length; i++) {
            if (i != targetIndex) {
                dataset = this.convertColumnToFloat(dataset, i);
            }
        }
        return dataset;
    }


    convertColumnToFloat(rows, columnIndex) {
        for (const row of rows) {
            row[columnIndex] = parseFloat(row[columnIndex]);
        }
        return rows;
    }


    removeColumn(rows, index){
        let newRows = []
        for (let i = 0; i < rows.length; i++){
            let newRow = [... rows[i]]
            // rows[i].splice(index, 1);
            newRow.splice(index, 1);
            newRows.push(newRow)
        }
        return newRows;
    }
}

export {ArrayHelper}