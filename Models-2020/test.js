
import {ArrayHelper} from "./array_helper.js"
import {DataFrame} from "./data_frame.js";
import {Parser} from "./parser.js";
import {Evaluation} from "./evaluation.js";
import {FastGeneticAlgorithm} from "./fast_genetic_algorithm.js"
import {GeneticAlgorithm} from "./genetic_algorithm.js"


let helper = new ArrayHelper();
let evaluation = new Evaluation();
let parser = new Parser();

const datasetConfig = JSON.parse(await Deno.readTextFile("./config/cleaned_dataset.json"));
let dataset_names = {
    1: "abalone",
    2: "bank",
    3: "bankNote",
    4: "breastCancer",
    5: "dataset",
    6: "iris",
    7: "wine",
    8: "johnwise_cleaned",
    9: "ml_bench_dataset",
    10: "mlbench_BreastCancer",
    11: "mlbench_DNA",
    12: "mlbench_Glass",
    13: "mlbench_HouseVotes84",
    14: "mlbench_Ionosphere",
    15: "mlbench_LetterRecognition",
    16: "mlbench_PimaIndiansDiabetes",
    17: "mlbench_Satellite",
    18: "mlbench_Shuttle",
    19: "mlbench_Sonar",
    20: "mlbench_Soybean",
    21: "mlbench_Vehicle",
    22: "mlbench_Vowel",
}

let dataset = datasetConfig[dataset_names[4]];
dataset = datasetConfig[dataset_names[4]];

let RF_CONFIG = {
    n_estimators: 100,
    max_features: "sqrt",
    max_depth: 20,
    min_sample_split: 2,
    max_bootstrap_samples: 0.8,
    bootstrap_with_replacement: true
}

let GA_CONFIG = {
    selection_rate: 0.2,
    crossover_rate: 0.2,
    mutation_rate: 0.1,
    crossover_type: "SINGLE",
    max_generation: 100,
    termination_condition: "NONE"
}


const training_filenames = dataset.training_filenames;
const optimization_filenames = dataset.validation_filenames;
const testing_filenames = dataset.testing_filenames;

const columns_not_use = dataset.columns_not_use;
const target_name = dataset.target_name;
const directory = dataset.directory;

let total_files = training_filenames.length;
let trainings = [];
let optimizations = [];
let x_testings = [];
let y_testings = []
let feature_names = []


// reading in all dataframes
for(let i = 0; i < total_files; i++){
    let training_filename = training_filenames[i];
    let testing_filename = testing_filenames[i]
    let optimization_filename = optimization_filenames[i]

    let training_filePath = directory + training_filename;
    let optimization_filePath = directory + optimization_filename;
    let testing_filePath = directory + testing_filename;

    console.log(`\nParsing Training dataset from ${training_filePath}`);
    console.log(`Parsing Testing dataset from ${testing_filePath}`);

    let training_df = new DataFrame(training_filePath, target_name, columns_not_use);
    let optimization_df = new DataFrame(optimization_filePath, target_name, columns_not_use);
    let testing_df = new DataFrame(testing_filePath, target_name, columns_not_use);

    await training_df.preprocess();
    await optimization_df.preprocess();
    await testing_df.preprocess();
    feature_names = training_df.featureNames;

    let training_set = helper.JSONtoArray(training_df.dataframe);
    let optimization_set = helper.JSONtoArray(optimization_df.dataframe);
    let testing_set = helper.JSONtoArray(testing_df.dataframe);
    let y_testing = helper.getColumn(testing_set, feature_names.length-1);
    let x_testing = helper.removeColumn(testing_set, feature_names.length-1);

    trainings.push(training_set);
    optimizations.push(optimization_set);
    x_testings.push(x_testing);
    y_testings.push(y_testing);
}



console.log("\n========== Finished Parsing Dataset ==========");
console.log("Training: ", trainings[0].length);
console.log("Optimization: ", optimizations[0].length);
console.log("Testing: ", x_testings[0].length);


async function benchmark_test(){
    const training_filenames = dataset.training_filenames;
    const optimization_filenames = dataset.validation_filenames;
    const testing_filenames = dataset.testing_filenames;

    const columns_not_use = dataset.columns_not_use;
    const target_name = dataset.target_name;
    const directory = dataset.directory;


    let index = 0;
    let training_filename = training_filenames[index];
    let testing_filename = testing_filenames[index]
    let optimization_filename = optimization_filenames[index]

    let training_filePath = directory + training_filename;
    let optimization_filePath = directory + optimization_filename;
    let testing_filePath = directory + testing_filename;

    console.log("\n========== Finished Configuration ==========");
    console.log("Training file: ", training_filePath);
    console.log("Optimization file: ", optimization_filePath);
    console.log("Testing file: ", testing_filePath);

    let training_df = new DataFrame(training_filePath, target_name, columns_not_use);
    let optimization_df = new DataFrame(optimization_filePath, target_name, columns_not_use);
    let testing_df = new DataFrame(testing_filePath, target_name, columns_not_use);

    await training_df.preprocess();
    await optimization_df.preprocess();
    await testing_df.preprocess();
    let feature_names = training_df.featureNames;


    let training_set = helper.JSONtoArray(training_df.dataframe);
    let optimization_set = helper.JSONtoArray(optimization_df.dataframe);
    let testing_set = helper.JSONtoArray(testing_df.dataframe);
    let y_optimization = helper.getColumn(optimization_set, feature_names.length-1);
    let x_optimization = helper.removeColumn(optimization_set, feature_names.length-1);
    let y_testing = helper.getColumn(testing_set, feature_names.length-1);
    let x_testing = helper.removeColumn(testing_set, feature_names.length-1);


    console.log("\n========== Finished Raw Data Parsing ==========");
    console.log("Training dataset: ", training_set.length);
    console.log("Optimization dataset: ", optimization_set.length);
    console.log("Testing dataset: ", testing_set.length);


    let GA_fast = new FastGeneticAlgorithm(GA_CONFIG, RF_CONFIG);


    console.log("\n========== Training Fast GA and Regular GA ==========");
    const start_time_fast = performance.now();
    GA_fast.start2(training_set, feature_names, x_optimization, y_optimization, x_testing, y_testing, false);
    const end_time_fast = performance.now();
    const duration_fast = end_time_fast - start_time_fast;

    console.log();
    console.log("Call to Fast GA took", duration_fast, "milliseconds.");

}


// await benchmark_test();
