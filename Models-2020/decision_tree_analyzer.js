
class DecisionTreeAnalyzer{
    find_node_with_highest_gini_gain(tree){
        let result = [];
        let features_names = tree.features_names;
        this.iterative_through_tree(features_names, tree.root, null, result);
        // Get an array of the keys:
        let keys = Object.keys(result);

        // Then sort by using the keys to lookup the values in the original object:
        keys.sort(function(a, b) { return result[a] - result[b] });
        return keys;
    }

    iterative_through_tree(feature_names, node, parent_node=null, result = []){
        if (typeof (node) == 'object') {

            if(parent_node != null){
                let key = feature_names[parent_node["index"]];
                let value = parent_node.gini - node.gini;
                if (key in result){
                    result[key] = Math.max(result[key], value);
                }else{
                    result[key] = value;
                }

            }
            this.iterative_through_tree(feature_names, node["left"], node, result);
            this.iterative_through_tree(feature_names, node["right"], node, result);
        } else {
            return result;
        }
    }


}

export {DecisionTreeAnalyzer};