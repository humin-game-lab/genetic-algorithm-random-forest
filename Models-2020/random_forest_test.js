import {ArrayHelper} from "./array_helper.js"
import {DataFrame} from "./data_frame.js";
import {FastRandomForest} from "./fast_random_forest.js";
import {Evaluation} from "./evaluation.js";

let helper = new ArrayHelper();
let evaluation = new Evaluation();
const datasetConfig = JSON.parse(await Deno.readTextFile("./config/small_dataset_config.json"));

let RF_CONFIG = {
    n_estimators: 100,
    max_features: "sqrt",
    max_depth: 20,
    min_sample_split: 2,
    max_bootstrap_samples: 0.6,
    bootstrap_with_replacement: false
}

let dataset_names = [
    "small_BreastCancer",
    "small_DNA",
    "small_Glass",
    "small_HouseVotes84",
    "small_Ionosphere",
    "small_LetterRecognition",
    "small_PimaIndiansDiabetes",
    "small_Satellite",
    "small_Shuttle",
    "small_Sonar",
    "small_Soybean",
    "small_Vehicle",
    "small_Vowel",
]


let dataset_name = dataset_names[i]
const training_filenames = datasetConfig.training_filenames;
const testing_filenames = datasetConfig.testing_filenames;

const columns_not_use = datasetConfig.columns_not_use;
const target_name = datasetConfig.target_name;
const directory1 = datasetConfig.mlBench_directory;
const directory2 = datasetConfig.dataset_directory;

const directory = directory1 + dataset_name + "/";

let total_files = training_filenames.length;
let trainings = [];
let x_testings = [];
let y_testings = [];
let feature_names = [];
console.log(`Parsing Training dataset from:`, dataset_name);

// reading in all dataframes
for(let i = 0; i < total_files; i++){
    let training_filename = training_filenames[i];
    let testing_filename = testing_filenames[i]

    let training_filePath = directory + training_filename;
    let testing_filePath = directory + testing_filename;

    // console.log(`\nParsing Training dataset from ${training_filePath}`);
    // console.log(`Parsing Testing dataset from ${testing_filePath}`);

    let training_df = new DataFrame(training_filePath, target_name, columns_not_use);
    let testing_df = new DataFrame(testing_filePath, target_name, columns_not_use);

    await training_df.preprocess();
    await testing_df.preprocess();
    feature_names = training_df.featureNames;

    let training_set = helper.JSONtoArray(training_df.dataframe);
    let testing_set = helper.JSONtoArray(testing_df.dataframe);
    let y_testing = helper.getColumn(testing_set, feature_names.length-1);
    let x_testing = helper.removeColumn(testing_set, feature_names.length-1);

    trainings.push(training_set);
    x_testings.push(x_testing);
    y_testings.push(y_testing);
}



// console.log(`\n========== Finished Parsing Dataset ${directory} ==========`);
// console.log("Features [", feature_names.length, "] : ", feature_names)
// console.log("Training: ", trainings[0].length);
// console.log("Testing: ", x_testings[0].length);

let train = []
let x_test = []
let y_test = []

async function read(){
    const training_filenames = datasetConfig.training_filenames;
    const testing_filenames = datasetConfig.testing_filenames;

    for(let index = 0; index < training_filenames.length; index++){
        let training_filename = training_filenames[index];
        let testing_filename = testing_filenames[index]

        let training_filePath = directory + training_filename;
        let testing_filePath = directory + testing_filename;

        // console.log(`========== Sample #${index+1} Finished Configuration ==========`);
        // console.log("Training file: ", training_filePath);
        // console.log("Testing file: ", testing_filePath);

        let training_df = new DataFrame(training_filePath, target_name, columns_not_use);
        let testing_df = new DataFrame(testing_filePath, target_name, columns_not_use);

        await training_df.preprocess();
        await testing_df.preprocess();
        let feature_names = training_df.featureNames;

        let training_set = helper.JSONtoArray(training_df.dataframe);
        let testing_set = helper.JSONtoArray(testing_df.dataframe);
        let y_testing = helper.getColumn(testing_set, feature_names.length-1);
        let x_testing = helper.removeColumn(testing_set, feature_names.length-1);

        train.push(training_set);
        x_test.push(x_testing);
        y_test.push(y_testing);
    }
}


let accuracies = []
let time_fit = []
let time_fit_predict = []
function run(training_data, testing_data, testing_target, featureNames){
    let randomForestClassifier = new FastRandomForest(RF_CONFIG);
    const t0 = performance.now();

    randomForestClassifier.fit(training_data, featureNames);
    const t1 = performance.now();
    let predicted = randomForestClassifier.predict(testing_data);
    const t2 = performance.now();
    let accuracy = evaluation.accuracy_metric(predicted, testing_target);

    let duration_fit = (t1-t0)/1000;
    let duration_fit_predict = (t2-t0)/1000;

    accuracies.push(accuracy)
    time_fit.push(duration_fit);
    time_fit_predict.push(duration_fit_predict/1000);
    console.log(accuracy,",", duration_fit,",", duration_fit_predict-duration_fit);
}


await read();

for(let i = 0; i < 50; i++){
    run(train[i], x_test[i], y_test[i], feature_names);
}

console.log();
console.log();




