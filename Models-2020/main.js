import {ArrayHelper} from "./array_helper.js"
import {DataFrame} from "./data_frame.js";
import {GeneticAlgorithm} from "./genetic_algorithm.js"

let RF_CONFIG = {
    n_estimators: 10,
    max_features: "sqrt",
    max_depth: 5,
    min_sample_split: 2,
    max_bootstrap_samples: 0.6,
}

let GA_CONFIG = {
    training_percentage: 0.3,
    validation_percentage: 0.3,
    testing_percentage: 0.3,
    selection_rate: 0.3,
    crossover_rate: 0.3,
    mutation_rate: 0.1,
    crossover_type: "SINGLE",
    max_generation: 100,
    termination_condition: "NONE"
}


const dataset_name = [
    "mlbench_breastCancer",
    "mlbench_DNA",
    "mlbench_glass",
    "mlbench_houseVotes84",
    "mlbench_letterRecognition",
    "mlbench_pimaIndiansDiabetes",
    "mlbench_satellite",
    "mlbench_shuttle",
    "mlbench_sonar",
    "mlbench_soybean",
    "mlbench_vehicle",
    "mlbench_vowel"
];

const datasetConfig = JSON.parse(await Deno.readTextFile("./MLBench_Config.JSON"));

console.log("Dataset: ", dataset_name[11]);
let dataset = datasetConfig[dataset_name[11]];

console.log("Dataset Config: \n", dataset);
const filePath = dataset.filename;
const columns_not_use = dataset.columns_not_use;
const target_name = dataset.target_name;

let dataframe = new DataFrame(filePath, target_name, columns_not_use);
let helper = new ArrayHelper();

await dataframe.preprocess();
let feature_names = dataframe.featureNames;
dataset = dataframe.dataframe;
dataset = helper.JSONtoArray(dataset)
dataset = helper.shuffle(dataset)
dataset = helper.convertDatasetToFloat(dataset, dataset[0].length - 1);
console.log("number of rows: ", dataset.length);
console.log("feature names: ", feature_names);

let GA = new GeneticAlgorithm(dataset, feature_names);
GA.RunWithRestart_old(GA_CONFIG, RF_CONFIG);

// for(let t = 0; t < 5; t++)
//     console.log("Counter: ", t)
//     let GA = new GeneticAlgorithm(dataset, feature_names);
//     GA.RunWithRestart_old(GA_CONFIG, RF_CONFIG);
//     console.log("------------------------------------------------------------------\n")
// }



// console.log("feature names: ", feature_names);
// let GA = new GeneticAlgorithm(dataset, feature_names);
// GA.Run(GA_CONFIG, RF_CONFIG);


// GA.set_params(GA_CONFIG, RF_CONFIG);
// GA.KFoldValidation();

//
// for(let x = 0; x < dataset_name.length; x++) {
//     console.log("Dataset: ", dataset_name[x]);
//     let dataset = datasetConfig[dataset_name[x]];
//
//     // console.log("Dataset Config: \n", dataset);
//     const filePath = dataset.filename;
//     const columns_not_use = dataset.columns_not_use;
//     const target_name = dataset.target_name;
//
//     let dataframe = new DataFrame(filePath, target_name, columns_not_use);
//     let helper = new ArrayHelper();
//
//     await dataframe.preprocess();
//     let feature_names = dataframe.featureNames;
//     dataset = dataframe.dataframe;
//     dataset = helper.JSONtoArray(dataset)
//     dataset = helper.shuffle(dataset)
//     dataset = helper.convertDatasetToFloat(dataset, dataset[0].length - 1);
//
//     let GA = new GeneticAlgorithm(dataset, feature_names);
//     GA.Run(GA_CONFIG, RF_CONFIG);
//     // GA.set_params(GA_CONFIG, RF_CONFIG);
//     // GA.KFoldValidation(5);
// }