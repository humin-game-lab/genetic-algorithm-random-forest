import pydot
import os

dotsFiles = [f for f in os.listdir()]

for file in dotsFiles:
    print(file)
    tokens = os.path.splitext(file)
    if tokens[1] == ".dot":
        print("converting: ", file, "to", tokens[0]+ ".png")
        (graph,) = pydot.graph_from_dot_file(file)
        graph.write_png((tokens[0] + ".png"))