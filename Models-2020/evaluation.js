import {ArrayHelper} from "./array_helper.js";

class Evaluation{

    compare_two_trees(testing, testing_target, tree_1, tree_2, verbose = false){
        let predicted_1 = tree_1.predict(testing);
        let predicted_2 = tree_2.predict(testing);
        let tree_1_accuracy = this.accuracy_metric(predicted_1, testing_target);
        let tree_2_accuracy = this.accuracy_metric(predicted_2, testing_target);

        if (verbose){
            console.log("parent tree vs. child tree:", tree_1_accuracy, tree_2_accuracy, tree_1_accuracy < tree_2_accuracy);
        }
        return tree_1_accuracy < tree_2_accuracy;
    }




    evaluate_k_fold(dataset, algorithm, n_folds, args, ifReturnTrees = false){
        let Helper = new ArrayHelper();
        let targetColumnIndex = dataset[0].length-1;
        const folds = this.cross_validation_split(dataset, n_folds);
        let scores = []
        let trees = []
        let DT = new algorithm()
        for(const fold of folds){
            let trainingSet = [...folds]
            let index = trainingSet.indexOf(fold);
            trainingSet.splice(index,1); // remove the testing fold from the total dataset
            let testingSet = [...fold]
            let actual = this.getColumn(testingSet);
            testingSet = this.clearColumn(testingSet, targetColumnIndex)
            trainingSet = Helper.merge2DArrays(trainingSet);
            let [predicted, tree] = DT.buildAndReturnTree(trainingSet, testingSet, args[0], args[1], true)
            let accuracy = this.accuracy_metric(actual, predicted)
            scores.push(accuracy)
            trees.push(tree);
        }
        if (ifReturnTrees){
            return [scores, trees];
        }
        console.log(scores)
        return scores;
    }


    cross_validation_split(dataset, n_folds){
        let dataset_split = []
        let dataset_copy = [...dataset]
        let fold_size = Math.floor(dataset.length/n_folds);
        for(let i = 0; i < n_folds; i++){
            let fold = []
            while (fold.length < fold_size){
                let index = Math.floor(Math.random() * dataset_copy.length);
                fold.push(dataset_copy[index]);
                dataset_copy.splice(index,1);
            }
            dataset_split.push(fold);
        }
        return dataset_split;
    }


    // need to add precision rate, recall rate and F1-score
    accuracy_metric(predicted, actual){
        let correct = 0;
        for (let i = 0; i < actual.length; i++){
            if (actual[i] == predicted[i]){
                correct += 1;
            }
        }
        return (correct/actual.length) * 100.0
    }


    train_test_split(dataset, training_ratio, testing_ratio, vervose = false){
        let training_count = dataset.length * training_ratio
        let testing_count = dataset.length * testing_ratio
        dataset = shuffle(dataset)
        let training = dataset.slice(0, training_count)
        let testing = dataset.slice(training_count, training_count+testing_count)
        const arrayColumn = (arr, n) => arr.map((x) => x[n]);
        let actual = arrayColumn(testing, testing[0].length-1)
        if (vervose == true){
            console.log("Training:")
            console.log(training)
            console.log("testing:")
            console.log(testing)
            console.log("Target:")
            console.log(actual)
        }
        return [training, testing, actual]
    }


    getColumn(rows, index){
        const arrayColumn = (arr, n) => arr.map((x) => x[n]);
        let result = arrayColumn(rows, rows[index])
        return result
    }


    printDataset(training_ratio, testing_ratio, target){
        console.log("Training:")
        console.log(training)
        console.log("testing:")
        console.log(testing)
        console.log("Target:")
        console.log(target)
    }


    clearColumn(rows, columnIndex){
        let newRows = []
        for (const row of rows){
            let newRow = [...row]
            newRow[columnIndex] = null;
            newRows.push(newRow)
        }
        return newRows;
    }

    shuffle(dataset){
        console.log("Shuffling the dataset");
        dataset.sort(() => Math.random() - 0.5)
        return dataset;
    }

    JSONtoArray(JSON_data){
        let dataset = []
        for(const row of JSON_data){
            var result = [];
            var keys = Object.keys(row);
            keys.forEach(function(key){
                result.push(row[key]);
            });
            dataset.push(result)
        }
        return dataset;
    }

    convertDatasetToFloat(dataset, targetIndex){
        for(let i = 0; i< dataset[0].length; i++){
            if (i != targetIndex){
                dataset = convertColumnToFloat(dataset, i);
            }
        }
        return dataset;
    }

    convertColumnToFloat(rows, columnIndex){
        for(const row of rows){
            row[columnIndex] = parseFloat(row[columnIndex]);
        }
        return rows;
    }

}

export {Evaluation}