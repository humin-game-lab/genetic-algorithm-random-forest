import {DecisionTree} from "./decision_tree.js";
import {ArrayHelper} from "./array_helper.js";
import {Evaluation} from "./evaluation.js";
import {Parser} from "./parser.js";
import {FastRandomForest} from "./fast_random_forest.js";
import {FastDecisionTree} from "./fast_decision_tree.js";
import "https://deno.land/x/lodash@4.17.19/vendor/underscore/underscore.js";

/*
params:
     selection_rate:  selection ratio
     crossover_rate:  crossover ratio
     mutation_rate:   mutation ratio
     crossover_type:  type of the crossover operator
     max_generation:  maximum number of generation/epochs
*/

class FastGeneticAlgorithm {
    constructor(GA_CONFIG, RF_CONFIG) {
        // training, testing, and optimization set
        this.training_set = [];

        this.feature_names = [];
        this.optimization_set = []
        this.optimization_target = []

        this.testing_set = []
        this.testing_target = []

        this.current_population = null;
        this.accuracy = [];
        this.generation_counter = 0;

        // Config files for GA and RF
        this.GA_CONFIG = GA_CONFIG;
        this.RF_CONFIG = RF_CONFIG;

        this.helper = new ArrayHelper();
        this.evaluation = new Evaluation();
        this.parser = new Parser();
        this.set_params();
    }


    set_params(){
        // parameters for the Genetic Algorithm model
        this.selection_rate = this.GA_CONFIG.selection_rate;
        this.crossover_rate = this.GA_CONFIG.crossover_rate;
        this.mutation_rate  = this.GA_CONFIG.mutation_rate;
        this.crossover_type = this.GA_CONFIG.crossover_type;
        this.max_generation = this.GA_CONFIG.max_generation;
        this.termination_condition = this.GA_CONFIG.termination_condition;
        this.diversity_percentage = this.GA_CONFIG.diversity_percentage;
        this.restart_condition = 3;
        this.feature_names = this.RF_CONFIG.feature_names;
    }

    start(train, feature_names, x_optimization, y_optimization, x_test, y_test, verbose = false){
        this.feature_names = [...feature_names];
        this.initialize(train, x_optimization, y_optimization, x_test, y_test, verbose);
        let initial_accuracy = this.get_cur_accuracy(false);
        // console.log("Epoch 0: ", initial_accuracy);
        this.accuracy.push({epoch: this.generation_counter, performance: initial_accuracy});
        while(this.generation_counter < this.max_generation){
            this.evaluate(verbose);
            let [best_trees, worst_trees] = this.fitness();
            this.crossover(best_trees, worst_trees, verbose);
            this.mutation(verbose);
            this.replace_bad_trees_with_new_trees();
            this.generation_counter += 1;
            let accuracy = this.get_cur_accuracy(false)
            // console.log(`Epoch ${this.generation_counter}: `, accuracy);
            // let ifStopImprove = this.check_if_stop_improving();
            // if (ifStopImprove){
            //     this.remove_bad_trees();
            //     // this.print_current_population("After remove bad trees:");
            //     this.add_diversity_from_n_forest(50);
            //     // this.add_diversity();
            //     accuracy = this.get_cur_accuracy(false)
            //     this.accuracy.push({epoch: this.generation_counter, performance: accuracy});
            // }else{
            //     this.accuracy.push({epoch: this.generation_counter, performance: accuracy});
            // }
            this.accuracy.push({epoch: this.generation_counter, performance: accuracy});
        }
        this.find_tree_accuracy();
        // this.output();
    }

    start2(train, feature_names, x_optimization, y_optimization, x_test, y_test, verbose = false){
        let temp_accuracies = [];
        this.feature_names = [...feature_names];
        this.initialize(train, x_optimization, y_optimization, x_test, y_test, verbose);
        let initial_accuracy = this.get_cur_accuracy(false);
        // console.log("Epoch 0: ", initial_accuracy);
        this.accuracy.push({epoch: this.generation_counter, performance: initial_accuracy});

        while(this.generation_counter < this.max_generation){
            let accuracy_current_population = this.get_cur_accuracy(false);
            this.evaluate(verbose);
            this.remove_bad_trees();
            // this.print_current_population("After remove bad trees:");
            let accuracy_after_remove = this.get_cur_accuracy(false);
            this.add_diversity_from_n_forest(1);
            let accuracy_after_add = this.get_cur_accuracy(false);
            // this.print_current_population("After adding good trees:");
            console.log(`Epoch ${this.generation_counter}: `, accuracy_current_population, "->", accuracy_after_remove, "->", accuracy_after_add);
            this.find_tree_accuracy();
            temp_accuracies.push(accuracy_after_add);
            this.generation_counter += 1;
        }
        return temp_accuracies;
    }

    find_tree_accuracy(){
        let sorted_trees = this.current_population.get_sorted_trees();
        let best_performance = []
        for(let i = 0; i < 10; i++){
            let cur_tree = sorted_trees[sorted_trees.length - i - 1];
            let cur_tree_accuracy = cur_tree.model.get_performance(this.testing_set, this.testing_target);
            best_performance.push(cur_tree_accuracy)
            // best_performance.push({model: cur_tree.model, accuracy: cur_tree_accuracy});
        }
        let best_dt_end_accuracy = Math.max(...best_performance);
        // let best_dt_end_accuracy = _.max(best_performance, _.property('accuracy'));
        // let parser = new Parser(this.feature_names);
        // parser.parse(best_dt_end_accuracy.model, true);
        console.log(best_dt_end_accuracy)
    }

    snapshot_best_trees(){
        console.log("Here: snapshot_best_trees");
        let sorted_trees = this.current_population.get_sorted_trees();
        for(let i = 0; i < 10; i++){
            let parser = new Parser(this.feature_names);
            let cur_tree = sorted_trees[sorted_trees.length - i - 1];
            let output_filename = "G"+this.generation_counter + "_ID_" + cur_tree.id + "_ACC_"+cur_tree.performance;
            parser.parse( cur_tree.model,true);
        }
    }

    print_current_population(optional_prompt = ""){
        console.log("\n============================= Current Population =============================");
        console.log(`Epoch ${this.generation_counter}: ${optional_prompt}`);
        for(let i = 0; i < this.current_population.trees.length; i++){
            console.log("id: ", this.current_population.trees[i].id, "performance:", this.current_population.trees[i].performance);
        }
        console.log();
    }

    output(){
        console.log("Simple GA Stat: ")
        let x = _.sortBy(this.accuracy, "performance");
        console.log("Accuracy: ", x[0], " -> ", x[this.accuracy.length-1])
        // console.log(x);
    }


    initialize(train, x_optimization, y_optimization, x_test, y_test, verbose = false){
        // store our initial training set
        this.training_set = [...train]

        this.optimization_set = [...x_optimization];
        this.optimization_target = [...y_optimization];

        this.testing_set = [...x_test]
        this.testing_target = [...y_test]

        if(verbose){
            console.log("training_set: ", this.training_set.length);
            console.log("testing_set: ", this.testing_set.length);
            console.log("optimization_set: ", this.optimization_set.length);
        }


        // train the Random Forest model
       let randomForestClassifier =  this.build_forest();
        // assign it as the current population
        let forest_copy = new FastRandomForest(this.RF_CONFIG);
        forest_copy.parse_forest(randomForestClassifier);
        this.current_population = forest_copy;
    }


    // Evaluate: evaluate the Random Forest model performance using the optimization dataset
    evaluate(verbose = false){
        this.current_population.evaluate(this.optimization_set,
                                        this.optimization_target,
                                        verbose);
    }


    // Accuracy: calculate the Random Forest model performance using the testing dataset
    get_cur_accuracy(verbose = false){
        let accuracy = this.current_population.get_accuracy(this.testing_set, this.testing_target, verbose);
        if(verbose){
            console.log(`Generation ${this.generation_counter} Accuracy: ${accuracy} `);
        }
        return accuracy;
    }



    fitness(){
        let trees_in_cur_population = this.current_population.trees;
        trees_in_cur_population = _.sortBy(trees_in_cur_population, 'performance');
        let best_trees = [];
        let worst_trees = [];
        let selection_number = trees_in_cur_population.length * this.selection_rate;
        for(let i = 0; i < selection_number; i++){
            worst_trees.push(trees_in_cur_population[i]);
            best_trees.push(trees_in_cur_population[trees_in_cur_population.length-i-1]);
        }
        return [best_trees, worst_trees];
    }


    build_forest(config = this.RF_CONFIG){
        // create the first population
        let randomForestClassifier = new FastRandomForest(config);
        // train the Random Forest model
        randomForestClassifier.fit(this.training_set, this.feature_names);
        randomForestClassifier.evaluate(this.optimization_set, this.optimization_target);
        return randomForestClassifier;
    }


    remove_bad_trees(){
        let sorted_trees = this.current_population.get_sorted_trees();
        let bad_tree_number = Math.floor(this.diversity_percentage * this.RF_CONFIG.n_estimators);
        for(let i = 0; i < bad_tree_number; i++){
            this.current_population.pop_tree_by_id(sorted_trees[i].id);
        }
    }




    check_if_stop_improving(){
        if(this.accuracy.length > this.restart_condition){
            let decrease_counter = 0
            for(let i = 0; i < this.restart_condition; i++){
                if(this.accuracy[this.accuracy.length-i-1].performance >= this.accuracy[this.accuracy.length-i-2].performance){
                    decrease_counter += 1;
                }
            }
            if (decrease_counter === this.restart_condition){
                return true;
            }
        }
        return false;
    }

    add_diversity(){
        let new_forest = this.build_forest();
        let sorted_trees = new_forest.get_sorted_trees();
        let best_trees = [];
        let selection_number = Math.floor(this.RF_CONFIG.n_estimators * this.diversity_percentage);
        for(let i = 0; i < selection_number; i++){
            best_trees.push(sorted_trees[sorted_trees.length-i-1]);
        }
        this.current_population.add_new_trees(best_trees)
    }

    // adding new trees from n forest
    add_diversity_from_n_forest(n = 10){
        let config = {
            n_estimators: 100,
            max_features: "sqrt",
            max_depth: 20,
            min_sample_split: 2,
            max_bootstrap_samples: 0.7,
            bootstrap_with_replacement: true
        }

        let new_unsorted_trees = [];
        let selection_number = Math.floor(this.RF_CONFIG.n_estimators * this.diversity_percentage);
        for(let i = 0; i < n; i++){
            let new_forest = this.build_forest(config);
            let sorted_trees = new_forest.get_sorted_trees();
            for(let i = 0; i < selection_number; i++){
                new_unsorted_trees.push(sorted_trees[sorted_trees.length-i-1])
            }
        }

        let best_trees = [];
        let sorted_trees = _.sortBy(new_unsorted_trees, 'performance');
        for(let i = 0; i < selection_number; i++){
            best_trees.push(sorted_trees[sorted_trees.length-i-1]);
        }
        this.current_population.add_new_trees(best_trees)
    }

    replace_bad_trees_with_new_trees(){
        let newForest = this.build_forest();
        let sorted_trees = newForest.get_sorted_trees();
        let current_sorted_trees = this.current_population.get_sorted_trees();
        let bad_trees_ids = [];
        let best_trees = [];

        let selection_number = newForest.trees.length * this.diversity_percentage;
        for(let i = 0; i < selection_number; i++){
            bad_trees_ids.push(current_sorted_trees[i].id);
            best_trees.push(sorted_trees[sorted_trees.length-i-1]);
        }

        this.current_population.remove_bad_trees(bad_trees_ids);
        this.current_population.add_trees_with_ids(best_trees, bad_trees_ids);
        // console.log("highest performance tree:", this.current_population.bestTrees[0].performance);
    }


    terminate(verbose = false){
        // we need at least 2 populations
        if (this.generation_counter < 3){
            return false;
        }

        // if the maximum number of generation equals to the number of generations we have
        if (this.generation_counter >= this.max_generation){
            return true;
        }

        // Termination condition 1: Compare the average performance from [0, previous population] and the average performance from [initial_population, current population]
        // Continue only if Avg_Accuracy[i] > Avg_Accuracy[i-1]
        if (this.termination_condition === "OPTIMAL"){
            let prev_avg = this.helper.get_average_from_objects(this.accuracy.slice(0, this.generation_counter-1), "performance");
            let cur_avg = this.helper.get_average_from_objects(this.accuracy, "performance");
            return prev_avg > cur_avg;
        }

        if(this.termination_condition === "NONE"){
            return false;
        }

        // Termination condition 2: an Integer shows the maximum number of decreasing population we are allowed
        // Terminate if Accuracy[i] < Accuracy[i-1] < .... < Accuracy[i-n]
        // check if our genetic algorithm stop improve our current population
        if(this.accuracy.length > this.termination_condition){
            let decrease_counter = 0
            for(let i = 0; i < this.termination_condition; i++){
                if(this.accuracy[this.accuracy.length-i-1].performance > this.accuracy[this.accuracy.length-i-2].performance){
                    decrease_counter += 1;
                }
            }
            if (decrease_counter === this.termination_condition){
                return true;
            }
        }
        return false;
    }



    mutation(verbose = false){
        // getting the number of mutation on the current forest
        let forest_size = this.current_population.trees.length;
        let number_of_mutation = forest_size * this.mutation_rate;

        // for each mutation:
        if(verbose){
            console.log("Mutating", number_of_mutation, "genes from the current population")
        }

        let mutatedIndex = []
        for(let i = 0; i < number_of_mutation; i++){
            // console.log("mutating: Gen:", this.generation_counter, " - ", i, number_of_mutation)
            let mutate_from_index = Math.floor(Math.random() * this.current_population.trees.length);
            let mutate_from_tree = this.current_population.pop_tree_by_index(mutate_from_index);

            let mutate_to_index = Math.floor(Math.random() * this.current_population.trees.length);
            let mutate_to_tree = this.current_population.pop_tree_by_index(mutate_to_index);

            mutatedIndex.push(mutate_to_index);
            let[tree1, tree2] = this.random_mutation(mutate_from_tree, mutate_to_tree, false);
            tree1.performance = tree1.model.get_performance(this.optimization_set, this.optimization_target);
            tree2.performance = tree2.model.get_performance(this.optimization_set, this.optimization_target);
            this.current_population.add_trees([tree1, tree2]);
        }
        if (verbose){
            console.log("Mutated Genes: ", mutatedIndex);
        }
    }

    random_mutation(tree1, tree2, verbose = false){
        // now we find where do we swap branches
        let non_leaf_nodes_tree1 = tree1.model.find_available_nodes();
        let non_leaf_nodes_tree2 = tree2.model.find_available_nodes();

        // shuffle the possible split point
        non_leaf_nodes_tree1 = this.helper.shuffle(non_leaf_nodes_tree1);
        non_leaf_nodes_tree2 = this.helper.shuffle(non_leaf_nodes_tree2);

        // decide which tree should be the mutated tree
        let tree1_split_node_index = Math.floor(Math.random() * non_leaf_nodes_tree1.length);
        let tree2_split_node_index = Math.floor(Math.random() * non_leaf_nodes_tree2.length);

        let tree1_split_node = non_leaf_nodes_tree1[tree1_split_node_index];
        let tree2_split_node = non_leaf_nodes_tree2[tree2_split_node_index];

        // choose tree1 as the mutated tree if random number is [0,0.5)
        // choose tree2 as the mutated tree if random number is [0.5,1)
        let improve = false
        let max_try = 1;
        let counter = 0;

        while(!improve && counter < max_try){
            // making a copy of both trees
            let tree1_copy = new DecisionTree();
            let tree2_copy = new DecisionTree();
            tree1_copy.parse(tree1.model);
            tree2_copy.parse(tree2.model);

            let choice = Math.random();
            if (choice < 0.5){
                tree1_copy.find_and_replace_node(tree1_copy.root, tree1_split_node, tree2_split_node, true);
                improve = this.evaluation.compare_two_trees(this.optimization_set, this.optimization_target, tree1.model, tree1_copy, verbose);
                // improve = this.evaluation.compare_two_trees(this.optimization_set, this.optimization_target, tree1.model, tree1_copy, verbose);
                if(improve){
                    tree1.model.find_and_replace_node(tree1.model.root, tree1_split_node, tree2_split_node, true);
                    break;
                }
            }else{
                tree2_copy.find_and_replace_node(tree2_copy.root, tree2_split_node, tree1_split_node, true);
                improve = this.evaluation.compare_two_trees(this.optimization_set, this.optimization_target, tree2.model, tree2_copy, verbose);
                if(improve){
                    tree2.model.find_and_replace_node(tree2.model.root, tree2_split_node, tree1_split_node, true);
                    break;
                }
            }
            counter += 1;
        }
        return [tree1, tree2];
    }


    // Crossover operator
    crossover(best_trees, worst_trees, verbose = false){
        let current_gen_best_trees = best_trees;
        let current_gen_worst_trees = worst_trees;

        let crossover_number = Math.floor(this.current_population.trees.length * this.crossover_rate)
        let children = [];

        if(verbose){
            console.log("Crossover", crossover_number, "genes from the current population")
        }

        for(let i = 0; i < crossover_number; i++){
            let child = null;
            // using single crossover technique
            if(this.crossover_type === "SINGLE"){
                let improve = false
                let max_try = 1;
                let counter = 0;

                let good_tree_index = Math.floor(Math.random() * current_gen_best_trees.length);
                let bad_tree_index = Math.floor(Math.random() * current_gen_worst_trees.length);

                let good_tree = this.current_population.pop_tree_by_id(current_gen_best_trees[good_tree_index].id);
                let bad_tree = this.current_population.pop_tree_by_id(current_gen_worst_trees[bad_tree_index].id);

                while(!improve && counter < max_try){
                    child = this.single_crossover(good_tree, bad_tree, this.crossover_type);
                    improve = this.evaluation.compare_two_trees(this.optimization_set, this.optimization_target, bad_tree.model, child.model);
                    counter += 1;
                }
                let child_performance = child.model.get_performance(this.optimization_set, this.optimization_target);
                let good_tree_performance = good_tree.model.get_performance(this.optimization_set, this.optimization_target);
                child.performance = child_performance;
                good_tree.performance = good_tree_performance;
                this.current_population.add_trees([child, good_tree]);
            }
        }
        return children;
    }


    // tree1 is the good tree and tree2 is the bad tree
    single_crossover(tree1, tree2, criteria = "RANDOM") {
        // now we find where do we swap branches
        let non_leaf_nodes_tree1 = tree1.model.find_available_nodes();
        let non_leaf_nodes_tree2 = tree2.model.find_available_nodes();

        // shuffle the possible split point
        non_leaf_nodes_tree1 = this.helper.shuffle(non_leaf_nodes_tree1);
        non_leaf_nodes_tree2 = this.helper.shuffle(non_leaf_nodes_tree2);

        // decide which tree should be the mutated tree
        let tree1_split_node_index = Math.floor(Math.random() * non_leaf_nodes_tree1.length);
        let tree2_split_node_index = Math.floor(Math.random() * non_leaf_nodes_tree2.length);

        let tree1_split_node = non_leaf_nodes_tree1[tree1_split_node_index];
        let tree2_split_node = non_leaf_nodes_tree2[tree2_split_node_index];

        let child = {id: tree2.id, model: new FastDecisionTree(), performance: 0.0};
        let tree2_copy = JSON.parse(JSON.stringify(tree2.model));
        child.model.parse(tree2_copy)
        child.model.find_and_replace_node(child.model.root, tree2_split_node, tree1_split_node, false);
        return child;
    }

}

export {FastGeneticAlgorithm}