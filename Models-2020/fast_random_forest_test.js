/**
 * Computes weight based on three factors:
 *   items sent
 *   items received
 *   last timestamp
 */

import {ArrayHelper} from "./array_helper.js"
import {DataFrame} from "./data_frame.js";
import {Parser} from "./parser.js";
import {Evaluation} from "./evaluation.js";
import {FastRandomForest} from "./fast_random_forest.js"
import {RandomForest} from "./random_forest.js"


let helper = new ArrayHelper();
let evaluation = new Evaluation();
let parser = new Parser();

const datasetConfig = JSON.parse(await Deno.readTextFile("./config/cleaned_dataset.json"));
let dataset_names = {
    1: "abalone",
    2: "bank",
    3: "bankNote",
    4: "breastCancer",
    5: "dataset",
    6: "iris",
    7: "wine",
    8: "johnwise_cleaned",
    9: "ml_bench_dataset",
    10: "mlbench_BreastCancer",
    11: "mlbench_DNA",
    12: "mlbench_Glass",
    13: "mlbench_HouseVotes84",
    14: "mlbench_Ionosphere",
    15: "mlbench_LetterRecognition",
    16: "mlbench_PimaIndiansDiabetes",
    17: "mlbench_Satellite",
    18: "mlbench_Shuttle",
    19: "mlbench_Sonar",
    20: "mlbench_Soybean",
    21: "mlbench_Vehicle",
    22: "mlbench_Vowel",
}

let dataset = datasetConfig[dataset_names[16]];

let RF_CONFIG = {
    n_estimators: 100,
    max_features: "sqrt",
    max_depth: 20,
    min_sample_split: 2,
    max_bootstrap_samples: 0.8,
    bootstrap_with_replacement: true
}


const training_filenames = dataset.training_filenames;
const optimization_filenames = dataset.validation_filenames;
const testing_filenames = dataset.testing_filenames;

const columns_not_use = dataset.columns_not_use;
const target_name = dataset.target_name;
const directory = dataset.directory;

let total_files = training_filenames.length;
let trainings = [];
let optimizations = [];
let x_testings = [];
let y_testings = []
let feature_names = []


// reading in all dataframes
for(let i = 0; i < total_files; i++){
    let training_filename = training_filenames[i];
    let testing_filename = testing_filenames[i]
    let optimization_filename = optimization_filenames[i]

    let training_filePath = directory + training_filename;
    let optimization_filePath = directory + optimization_filename;
    let testing_filePath = directory + testing_filename;

    console.log(`\nParsing Training dataset from ${training_filePath}`);
    console.log(`Parsing Testing dataset from ${testing_filePath}`);

    let training_df = new DataFrame(training_filePath, target_name, columns_not_use);
    let optimization_df = new DataFrame(optimization_filePath, target_name, columns_not_use);
    let testing_df = new DataFrame(testing_filePath, target_name, columns_not_use);

    await training_df.preprocess();
    await optimization_df.preprocess();
    await testing_df.preprocess();
    feature_names = training_df.featureNames;

    let training_set = helper.JSONtoArray(training_df.dataframe);
    let optimization_set = helper.JSONtoArray(optimization_df.dataframe);
    let testing_set = helper.JSONtoArray(testing_df.dataframe);
    let y_testing = helper.getColumn(testing_set, feature_names.length-1);
    let x_testing = helper.removeColumn(testing_set, feature_names.length-1);

    trainings.push(training_set);
    optimizations.push(optimization_set);
    x_testings.push(x_testing);
    y_testings.push(y_testing);
}



console.log("\n========== Finished Parsing Dataset ==========");
console.log("Training: ", trainings[0].length);
console.log("Optimization: ", optimizations[0].length);
console.log("Testing: ", x_testings[0].length);


function regular_forest_benchmark(training_sets, testing_sets, testing_targets, feature_names, n){
    console.log("\n========== Training Regular Decision Tree ==========");
    let start = performance.now();
    let time = []
    let regular_rfs = [];
    for(let i = 0; i < n; i++){
        let training_set = training_sets[i];
        let rf_regular = new RandomForest(RF_CONFIG);
        let start_time = performance.now();
        rf_regular.fit(training_set, feature_names);
        let end_time = performance.now();
        let duration_each = end_time - start_time;
        time.push(duration_each);
        regular_rfs.push(rf_regular);
        console.log(i, " Finished");
    }
    let end = performance.now();
    let duration = end - start;
    console.log("Call to Training Regular Random Forest took", duration/1000, "seconds.");
    console.log("Each training phase: ", time);

    let regular_rfs_accuracies = [];
    for(let i = 0; i < n; i++){
        let accuracy = regular_rfs[i].get_accuracy(x_testings[i], y_testings[i]);
        regular_rfs_accuracies.push(accuracy);
    }
    console.log("Regular RF Accuracies: ", regular_rfs_accuracies);
    const average = (arr) => arr.reduce((a, b) => a + b) / arr.length;
    return average(regular_rfs_accuracies);
}


function fast_random_forest_benchmark(training_sets, testing_sets, testing_targets, feature_names, n){
    console.log("\n========== Training Fast Decision Tree ==========");
    let start = performance.now();
    let fast_rfs = [];
    let time = [];
    for(let i = 0; i < n; i++){
        let training_set = training_sets[i];
        let rf_fast = new FastRandomForest(RF_CONFIG);
        let start_time = performance.now();
        rf_fast.fit(training_set, feature_names);
        let end_time = performance.now();
        let duration_each = end_time - start_time;
        time.push(duration_each);
        fast_rfs.push(rf_fast);
        console.log(i, " Finished");
    }

    let end = performance.now();
    let duration = end - start;
    console.log("Call to Training Fast Random Forest took", duration/1000, "seconds.");
    console.log("Each training phase: ", time);

    let fast_rfs_accuracies = [];
    for(let i = 0; i < n; i++){
        let accuracy = fast_rfs[i].get_accuracy(x_testings[i], y_testings[i]);
        fast_rfs_accuracies.push(accuracy);
    }
    console.log("Fast RF Accuracies: ", fast_rfs_accuracies);
    const average = (arr) => arr.reduce((a, b) => a + b) / arr.length;
    return average(fast_rfs_accuracies)

}




let rrf_accuracy = regular_forest_benchmark(trainings, x_testings, y_testings, feature_names, 5);
let frf_accuracy =fast_random_forest_benchmark(trainings, x_testings, y_testings, feature_names, 5);
console.log("Regular Random Forest Accuracy: ", rrf_accuracy);
console.log("Fast Random Forest Accuracy:", frf_accuracy);





async function benchmark_test(){

    const training_filenames = dataset.training_filenames;
    const optimization_filenames = dataset.validation_filenames;
    const testing_filenames = dataset.testing_filenames;

    const columns_not_use = dataset.columns_not_use;
    const target_name = dataset.target_name;
    const directory = dataset.directory;


    let index = 0;
    let training_filename = training_filenames[index];
    let testing_filename = testing_filenames[index]
    let optimization_filename = optimization_filenames[index]

    let training_filePath = directory + training_filename;
    let optimization_filePath = directory + optimization_filename;
    let testing_filePath = directory + testing_filename;

    console.log("\n========== Finished Configuration ==========");
    console.log("Training file: ", training_filePath);
    console.log("Optimization file: ", optimization_filePath);
    console.log("Testing file: ", testing_filePath);

    let training_df = new DataFrame(training_filePath, target_name, columns_not_use);
    let optimization_df = new DataFrame(optimization_filePath, target_name, columns_not_use);
    let testing_df = new DataFrame(testing_filePath, target_name, columns_not_use);

    await training_df.preprocess();
    await optimization_df.preprocess();
    await testing_df.preprocess();
    let feature_names = training_df.featureNames;


    let training_set = helper.JSONtoArray(training_df.dataframe);
    let optimization_set = helper.JSONtoArray(optimization_df.dataframe);
    let testing_set = helper.JSONtoArray(testing_df.dataframe);
    let y_testing = helper.getColumn(testing_set, feature_names.length-1);
    let x_testing = helper.removeColumn(testing_set, feature_names.length-1);


    console.log("\n========== Finished Raw Data Parsing ==========");
    console.log("Training dataset: ", training_set.length);
    console.log("Optimization dataset: ", optimization_set.length);
    console.log("Testing dataset: ", testing_set.length);


    let RF_fast = new FastRandomForest(RF_CONFIG);
    let RF_regular = new RandomForest(RF_CONFIG);


    console.log("\n========== Training Fast Random Forest and Regular Random Forest ==========");
    const start_time_fast_rf = performance.now();
    RF_fast.fit(training_set, feature_names);
    const end_time_fast_rf = performance.now();
    const duration_fast_rf = end_time_fast_rf - start_time_fast_rf;


    const start_time_regular_rf = performance.now();
    RF_regular.fit(training_set, feature_names);
    const end_time_regular_rf = performance.now();
    const duration_regular_rf = end_time_regular_rf - start_time_regular_rf;



    console.log();
    console.log("Call to Fast Random Forest took", duration_fast_rf, "milliseconds.");
    console.log("Call to Regular Random Forest took", duration_regular_rf, "milliseconds.");
    console.log();


    console.log("\n========== Predictions From Decision Tree ==========");
    let RF_fast_accuracy = RF_fast.get_accuracy(x_testing, y_testing);
    let RF_regular_accuracy = RF_regular.get_accuracy(x_testing, y_testing);

    console.log("Regular Random Forest Accuracy: ", RF_regular_accuracy);
    console.log("Fast Random Forest Accuracy: ", RF_fast_accuracy);
}

// await benchmark_test();
