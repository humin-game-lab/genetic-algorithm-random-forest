import {DecisonTreeAnalyzer} from "./decision_tree_analyzer.js";
import {RandomForestAnalyzer} from "./random_forest_analyzer.js";
import {GeneticAlgorithmAnalyzer} from "./genetic_algorithm_analyzer.js";

class Analyzer{
    constructor() {
        this.RandomForestAnalyzer = new RandomForestAnalyzer();
        this.GeneticAlgorithmAnalyzer = new GeneticAlgorithmAnalyzer();
        this.DecisonTreeAnalyzer = new DecisonTreeAnalyzer();
    }


/*
    // TODO: Adding analysis features to the model

    // TODO:
    //       1. Proximity for instances
    //       2. Feature Importance 1: look at the decrease in gini impurity
    //       3. Feature Importance 2: take permutations of the testing data
    //       4. Feature Importance 3: using IDs


*/


}