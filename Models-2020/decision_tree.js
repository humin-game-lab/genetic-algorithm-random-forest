import "https://deno.land/x/lodash@4.17.19/vendor/underscore/underscore.js";
import {Evaluation} from "./Evaluation.js";


// max_depth: the longest path between the root node and the leaf node:
// min_sample_split:  the minimum required number of observations in any given node in order to split it.
// min_samples_leaf: the minimum number of samples that should be present in the leaf node after splitting a node.
// n_estimators: the number of trees in the forest
// max_sample (bootstrap sample) : The max_samples hyperparameter determines what fraction of the original dataset is given to any individual tree. You might be thinking that more data is always better. Let’s try to see if that makes sense.
// max_features: This resembles the number of maximum features provided to each tree in a random forest.

class DecisionTree {
    constructor(max_depth = 5, min_sample_split = 10) {
        this.root = null;
        this.training_data = [];
        this.testing_data = [];
        this.tree_features_names = [];
        this.features_names = [];
        this.feature_indexes = [];
        this.oob_data = [];

        this.max_depth = max_depth;
        this.min_sample_split = min_sample_split;
    }

    get_params() {
        let dt_config = {
            features_names: this.features_names,
            features_index: this.feature_indexes,
            max_depth: this.max_depth,
            min_sample_split: this.min_sample_split,
        }
        return dt_config;
    }

    set_params(dt_config) {
        this.features_names = dt_config.features_names;
        this.features_index = dt_config.feature_indexes;
        this.max_depth = dt_config.max_depth;
        this.min_sample_split = dt_config.min_sample_split;
    }

    if_row_in_oob_data(row) {
        let i, j, current;
        for (i = 0; i < this.oob_data.length; ++i) {
            current = this.oob_data[i];
            for(j = 0; j < row.length && row[j] === current[j]; ++j){
                if (j === row.length) {
                    return true;
                }
            }
        }
        return false;
    }


    fit(training, featureNames, tree_features_names, featureIndexes, oob_data = []) {
        this.training_data = training;
        this.features_names = featureNames;
        this.tree_features_names = tree_features_names;
        this.feature_indexes = featureIndexes;
        this.oob_data = oob_data;
        this.build_tree();
    }


    build_tree() {
        let root = this.get_split(this.training_data);
        this.split(root, this.max_depth, this.min_sample_split, 1);
        this.root = root;
    }


    predict(testings, node = this.root) {
        this.testing_data = JSON.parse(JSON.stringify(testings))
        let predictions = []
        for (const row of testings) {
            let prediction = this._predict(node, row)
            predictions.push(prediction)
        }
        return predictions
    }

    get_performance(testings, targets){
        let predicted = this.predict(testings);
        let evaluation = new Evaluation();
        return evaluation.accuracy_metric(predicted, targets);
    }

    predict_single_row(row, node = this.root) {
        let testing_row = JSON.parse(JSON.stringify(row))
        let prediction = this._predict(node, testing_row)
        return prediction;
    }


    gini_index(groups, classes) {
        // count all samples at a split point
        let n_instances = 0;
        for (let i = 0; i < groups.length; i++) {
            n_instances += groups[i].length;
        }

        let gini = 0.0;
        for (let i = 0; i < groups.length; i++) {
            let group = groups[i];
            let size = group.length;
            // error checking to avoid 0
            if (size === 0) {
                continue;
            }

            let score = 0.0;
            for (let j = 0; j < classes.length; j++) {
                let class_val = classes[j];
                let p = 0;
                let counter = 0;
                for (let k = 0; k < group.length; k++) {
                    let row = group[k];
                    if (row[row.length - 1] === class_val) {
                        counter += 1;
                    }
                }
                p = counter / size;
                score += p * p;
            }
            gini += (1.0 - score) * (size / n_instances);
        }
        return gini;
    }


    test_split(index, value, dataset) {
        let left = [];
        let right = [];
        for (let i = 0; i < dataset.length; i++) {
            let row = dataset[i];
            if (row[index] < value) {
                left.push(row);
            } else {
                right.push(row);
            }
        }
        return [left, right];
    }


    get_split(dataset) {
        // getting the class label
        const arrayColumn = (arr, n) => arr.map((x) => x[n]);
        let targetIndex = dataset[0].length - 1;
        let target_column = arrayColumn(dataset, targetIndex);
        let class_values = _.uniq(target_column);
        let [b_index, b_value, b_score, b_groups] = [99999, 99999, 99999, []];
        for (let index = 0; index < dataset[0].length - 1; index++) {
            for (let i = 0; i < dataset.length; i++) {
                let row = dataset[i];
                let groups = this.test_split(index, row[index], dataset);
                let gini = this.gini_index(groups, class_values);
                if (gini < b_score) {
                    [b_index, b_value, b_score, b_groups] = [
                        this.feature_indexes[index],
                        row[index],
                        gini,
                        groups,
                    ];
                }
            }
        }
        return {"index": b_index, "value": b_value, "gini" : b_score, "groups": b_groups};
    }


    to_terminal(group) {
        const arrayColumn = (arr, n) => arr.map((x) => x[n]);
        let targetIndex = 0;
        if (group.length > 0) {
            targetIndex = group[0].length - 1;
        }
        let outcomes = arrayColumn(group, targetIndex);
        let result = _.chain(outcomes).countBy().pairs().max(_.last).head().value();
        return result;
    }


    split(node, max_depth, min_size, depth) {
        let [left, right] = [node["groups"][0], node["groups"][1]]
        // check for a no split
        if (left.length === 0 || right.length === 0) {
            let newArray = left.concat(right)
            node["left"] = node["right"] = this.to_terminal(newArray);
            return;
        }

        // check for max depth
        if (depth >= max_depth) {
            node["left"] = this.to_terminal(left)
            node["right"] = this.to_terminal(right)
            return;
        }

        // process left child
        if (left.length <= min_size) {
            node["left"] = this.to_terminal(left);
        } else {
            node["left"] = this.get_split(left);
            this.split(node["left"], max_depth, min_size, depth + 1);
        }


        // process right child
        if (right.length <= min_size) {
            node["right"] = this.to_terminal(right);
        } else {
            node["right"] = this.get_split(right);
            this.split(node["right"], max_depth, min_size, depth + 1);
        }
    }


    print_tree() {
        this._print_tree()
    }


    // using iterative inorder traversal to find all the non-leaf nodes
    find_available_nodes(max_depth = null) {
        let non_leaf_nodes = [];
        let queue = [];
        queue.push(this.root);
        while (queue.length > 0) {
            let node = queue.pop();
            if(node !== this.root){
                non_leaf_nodes.push(node);
            }
            if (typeof (node["left"]) == 'object') {
                queue.push(node["left"]);
            }

            if (typeof (node["right"]) == 'object') {
                queue.push(node["right"]);
            }
        }
        return non_leaf_nodes
    }


    find_and_replace_node(node, old_node, new_node, verbose = false){
        if (typeof (node) == 'object') {

            if(_.isEqual(node["left"], old_node)){
                node["left"] = new_node;
                return;
            }

            if(_.isEqual(node["right"], old_node)){
                node["right"] = new_node;
                return;
            }
            this.find_and_replace_node(node["left"], old_node, new_node, verbose);
            this.find_and_replace_node(node["right"], old_node, new_node, verbose);
        }
        return;
    }




    _print_tree(node = this.root, depth = 0) {
        if (typeof (node) == 'object') {
            console.log(`${' '.repeat(depth)}`, "X", node["index"], this.features_names[node["index"]], "<", node["value"])
            this.print_tree(node["left"], depth + 1)
            this.print_tree(node["right"], depth + 1)
        } else {
            console.log(`${' '.repeat(depth)}`, node);
        }
    }


    _predict(node, row) {
        if (row[node["index"]] < node["value"]) {
            if (typeof (node["left"]) == 'object') {
                return this._predict(node["left"], row);
            } else {
                return node["left"]
            }
        } else {
            if (typeof (node["right"]) == 'object') {
                return this._predict(node["right"], row);
            } else {
                return node["right"]
            }
        }
    }

    getLeftDepth(node = this.root) {
        if (typeof (node) != 'object') {
            return 0;
        }
        let leftDepth = this.getLeftDepth(node.left);
        return leftDepth + 1;
    }

    getRightDepth(node = this.root) {
        if (typeof (node) != 'object') {
            return 0;
        }
        let rightDepth = this.getRightDepth(node.right);
        return rightDepth + 1;

    }

    maxDepth(node = this.root) {
        // Root being null means tree doesn't exist.
        if (typeof (node) != 'object') {
            return 0;
        }
        // Get the depth of the left and right subtree
        // using recursion.
        let leftDepth = this.maxDepth(node.left);
        let rightDepth = this.maxDepth(node.right);

        // Choose the larger one and add the root to it.
        if (leftDepth > rightDepth) {
            return leftDepth + 1;
        } else {
            return rightDepth + 1;
        }
    }

    parse(rawTreeData) {
        this.root = JSON.parse(JSON.stringify(rawTreeData.root))
        this.features_names = rawTreeData.features_names;
        this.feature_indexes = rawTreeData.feature_indexes;
        this.min_size = rawTreeData.min_size;
        this.RF_CONFIG = rawTreeData.RF_CONFIG;
        this.max_depth = rawTreeData.max_depth;
        this.min_sample_split = rawTreeData.min_sample_split;
        this.max_bootsrap_samples = rawTreeData.max_bootsrap_samples;
        this.training_data = rawTreeData.training_data;
        this.testing_data = rawTreeData.testing_data;
        this.oob_data = rawTreeData.oob_data;
    }
}

export {DecisionTree}