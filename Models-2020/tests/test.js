import {DecisionTree_CART} from "../old_code/DecisionTreeCART.js";
import {Evaluation} from "../Evaluation.js";
import {Parser} from "../Parser.js";

let dataset = [[2.771244718,1.784783929,0],
    [1.728571309,1.169761413,0],
    [3.678319846,2.81281357,0],
    [3.961043357,2.61995032,0],
    [2.999208922,2.209014212,0],
    [7.497545867,3.162953546,1],
    [9.00220326,3.339047188,1],
    [7.444542326,0.476683375,1],
    [10.12493903,3.234550982,1],
    [6.642287351,3.319983761,1]]

let evaluation = new Evaluation();
let DT = new DecisionTree_CART();


let train_x = [[2.771244718,1.784783929,0],
    [1.728571309,1.169761413,0],
    [3.678319846,2.81281357,0],
    [3.961043357,2.61995032,0],
    [2.999208922,2.209014212,0],
    [9.00220326,3.339047188,1],
    [7.444542326,0.476683375,1],
    [6.642287351,3.319983761,1]]

let test_x = [[7.497545867,3.162953546,null], [10.12493903,3.234550982,null]]
let test_y = [1,1]

let [predicted, tree] = DT.buildAndReturnTree(train_x, test_x, 10,10, true)
let accuracy = evaluation.accuracy_metric(test_y, predicted)

console.log("\nAccuracy: ")
console.log(accuracy)

console.log("\nTree: ")
console.log(tree)
// DT.print_tree(tree)

let parser = new Parser();