import {DataFrame} from "../DataFrame.js";
import {ArrayHelper} from "../ArrayHelper.js";
import {Evaluation} from "../Evaluation.js";
import {RandomForest} from "../RandomForest.js";
import {Parser} from "../Parser.js";
import {DecisionTree} from "../DecisionTree.js";

const datasetConfig = JSON.parse(await Deno.readTextFile("./Dataset_Config.JSON"));
let dataset = datasetConfig.breast_cancer_dataset;
console.log("Dataset Config: \n", dataset);
const filePath = dataset.filename;
const columns_not_use = dataset.columns_not_use;
const target_name = dataset.target_name;

let dataframe = new DataFrame(filePath, target_name, columns_not_use);
let helper = new ArrayHelper();
let evaluation = new Evaluation()

await dataframe.preprocess();
let feature_names = dataframe.featureNames;
dataset = dataframe.dataframe;
dataset = helper.JSONtoArray(dataset)
dataset = helper.shuffle(dataset)
dataset = helper.convertDatasetToFloat(dataset, dataset[0].length - 1);


let [training, testing, actual] = helper.train_test_split(dataset, 0.7, 0.3, false);
let clf = new RandomForest(50, 20, feature_names);
clf.fit(training)
let predictions = clf.predict(testing)
// clf.test_GA(predictions);
// clf.test(testing, actual)
//
//


function cross_over_one_child(goodTree, badTree) {
    let goodTreeCopy = JSON.parse(JSON.stringify(goodTree))
    let random = Math.random();
    if (random < 0.5) {
        badTree.root.left = goodTreeCopy.root.left;
    } else {
        badTree.root.right = goodTreeCopy.root.right;
    }
    return [goodTree, badTree];
};




