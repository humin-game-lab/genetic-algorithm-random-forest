import {ArrayHelper} from "./array_helper.js"
import {DataFrame} from "./data_frame.js";
import {Parser} from "./parser.js";
import {Evaluation} from "./evaluation.js";
import {FastDecisionTree} from "./fast_decision_tree.js"
import {DecisionTree} from "./decision_tree.js";


let helper = new ArrayHelper();
let evaluation = new Evaluation();
let parser = new Parser();

let RF_CONFIG = {
    n_estimators: 50,
    max_features: "sqrt",
    max_depth: 5,
    min_sample_split: 2,
    max_bootstrap_samples: 0.6,
    feature_names:[]
}


let GA_CONFIG = {
    selection_rate: 0.2,
    crossover_rate: 0.2,
    mutation_rate: 0.1,
    crossover_type: "SINGLE",
    max_generation: 100,
    termination_condition: "NONE"
}

const datasetConfig = JSON.parse(await Deno.readTextFile("./cleaned_dataset.JSON"));

let dataset = datasetConfig["breast_cancer_dataset"];

const training_filenames = dataset.training_filenames;
const optimization_filenames = dataset.validation_filenames;
const testing_filenames = dataset.testing_filenames;

const columns_not_use = dataset.columns_not_use;
const target_name = dataset.target_name;
const directory = dataset.directory;

// console.log(training_filenames, testing_filenames);

let index = 0;
let training_filename = training_filenames[index];
let testing_filename = testing_filenames[index]
let optimization_filename = optimization_filenames[index]

let training_filePath = directory + training_filename;
let optimization_filePath = directory + optimization_filename;
let testing_filePath = directory + testing_filename;

console.log("\n========== Finished Configuration ==========");
console.log("Training file: ", training_filePath);
console.log("Optimization file: ", optimization_filePath);
console.log("Testing file: ", testing_filePath);

let training_df = new DataFrame(training_filePath, target_name, columns_not_use);
let optimization_df = new DataFrame(optimization_filePath, target_name, columns_not_use);
let testing_df = new DataFrame(testing_filePath, target_name, columns_not_use);

await training_df.preprocess();
await optimization_df.preprocess();
await testing_df.preprocess();
let feature_names = training_df.featureNames;




let training_set = helper.JSONtoArray(training_df.dataframe);
let optimization_set = helper.JSONtoArray(optimization_df.dataframe);
let testing_set = helper.JSONtoArray(testing_df.dataframe);
let y_testing = helper.getColumn(testing_set, feature_names.length-1);
let x_testing = helper.removeColumn(testing_set, feature_names.length-1);


console.log("\n========== Finished Raw Data Parsing ==========");
console.log("Training dataset: ", training_set.length);
console.log("Optimization dataset: ", optimization_set.length);
console.log("Testing dataset: ", testing_set.length);

console.log("\n========== Genetic Algorithm Configuration ==========");
console.log(GA_CONFIG);
console.log("\n========== Random Forest Model Configuration ==========");
console.log(RF_CONFIG);



let indexes = [];
for (let i = 0; i < feature_names.length; i ++){
    indexes.push(i);
}

let Fast_DT = new FastDecisionTree();
Fast_DT.fit(training_set, feature_names, indexes);
let predicted1 = Fast_DT.predict(testing_set);
console.log("Fast Decision Tree Accuracy: ", evaluation.accuracy_metric(predicted1, y_testing))


let DT = new DecisionTree();
DT.fit(training_set, feature_names, feature_names, indexes);
let predicted2 = Fast_DT.predict(testing_set);
console.log("Decision Tree Accuracy: ", evaluation.accuracy_metric(predicted2, y_testing))