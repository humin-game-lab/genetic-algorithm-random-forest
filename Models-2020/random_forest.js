import "https://deno.land/x/lodash@4.17.19/vendor/underscore/underscore.js";
import {ArrayHelper} from "./array_helper.js";
import {DecisionTree} from "./decision_tree.js";
import {Evaluation} from "./evaluation.js";
import {DecisionTreeAnalyzer} from "./decision_tree_analyzer.js";


/*
params:
     max_depth: the longest path between the root node and the leaf node:
     min_sample_split:  the minimum required number of observations in any given node in order to split it.
     min_samples_leaf: the minimum number of samples that should be present in the leaf node after splitting a node.
     n_estimators: the number of trees in the forest
     max_sample (bootstrap sample) : The max_samples hyper-parameter determines what fraction of the original dataset is given to any individual tree. You might be thinking that more data is always better. Let’s try to see if that makes sense.
     max_features: This resembles the number of maximum features provided to each tree in a random forest.
*/

class RandomForest{
    constructor(RF_CONFIG, id = null){

        this.training_set = [];
        this.feature_names =[];

        this.n_estimators = null;
        this.max_features = null;
        this.max_depth = null;
        this.min_sample_split = null;
        this.max_bootstrap_samples = null;

        this.set_params(RF_CONFIG);

        this.trees = []
        this.bestTrees = []
        this.worstTrees = []
        this.bestTreesIDs = []
        this.worstTreesIDs = []
        this.sortedTrees = []

        this.oob_score = 0.0;
        this.accuracy = 0.0;

        this.RF_CONFIG = RF_CONFIG;
        this.helper = new ArrayHelper();
        this.evaluation = new Evaluation();

        this.id = id;
    }


    parse_forest(rawForestData){
        this.set_params(rawForestData.RF_CONFIG);
        this.RF_CONFIG = rawForestData.RF_CONFIG;

        this.training_set = rawForestData.training_set;
        this.feature_names =rawForestData.feature_names;


        this.trees = []
        this.bestTreesIDs = rawForestData.bestTreesIDs;
        this.worstTreesIDs = rawForestData.worstTreesIDs;
        this.sortedTrees = []

        this.oob_score = 0.0
        this.accuracy = 0.0

        this.helper = new ArrayHelper();
        this.evaluation = new Evaluation();

        let tempTrees = []
        for(let i = 0; i < rawForestData.trees.length; i++){
            let dt = new DecisionTree();
            dt.parse(rawForestData.trees[i].model);
            tempTrees.push({
                            id: rawForestData.trees[i].id,
                            model: dt,
                            performance: rawForestData.trees[i].performance
                            });
        }

        this.trees = tempTrees;
        this.bestTrees = []
        this.worstTrees = []
        for(let i = 0; i < rawForestData.bestTreesIDs.length; i++){
            this.bestTrees.push({
                                 id:this.bestTreesIDs[i],
                                 model: this.trees[this.bestTreesIDs[i]].model,
                                 performance: this.trees[this.bestTreesIDs[i]].performance
                                });
            this.worstTrees.push({
                                 id:this.worstTreesIDs[i],
                                 model: this.trees[this.worstTreesIDs[i]].model,
                                 performance: this.trees[this.worstTreesIDs[i]].performance
                                });
        }
    }


    remove_bad_trees(ids = this.worstTreesIDs){
        for(let i = 0; i < ids.length; i++){
            this.pop_tree_by_id(ids[i]);
        }
        this.worstTrees = [];
        this.worstTreesIDs = [];
    }


    set_params(RF_CONFIG){
        this.n_estimators = RF_CONFIG.n_estimators;
        this.max_features = RF_CONFIG.max_features;
        this.max_depth = RF_CONFIG.max_depth;
        this.min_sample_split = RF_CONFIG.min_sample_split;
        this.max_bootstrap_samples = RF_CONFIG.max_bootstrap_samples;
    }

    get_random_features(bootstrap_data, verbose = false){
        let number_of_features_per_tree = 0;

        // determine the number of feature per tree;
        if (this.max_features === "sqrt"){
            number_of_features_per_tree = Math.sqrt(this.feature_names.length) + 1;
        }else if (this.max_features ==="log2"){
            number_of_features_per_tree = Math.log2(this.feature_names.length) + 1;
        }else{
            number_of_features_per_tree = this.feature_names.length-1;
        }



        // getting input for our decision tree model
        let [training, feature_names, feature_indexes] = this.helper.get_input_for_decision_tree(
                                                                                                bootstrap_data,
                                                                                                this.feature_names,
                                                                                                number_of_features_per_tree,
                                                                                                );

        if (verbose){
            console.log("feature names: ", feature_names);
            console.log("feature indexes: ", feature_indexes);
            console.log("training count: ", training.length);
        }
        return [training, feature_names, feature_indexes];
    }


    fit(training_set, feature_names, verbose = false){
        this.training_set = training_set;
        this.feature_names = feature_names;
        this.build_forest(verbose);
    }



    build_forest(verbose = false){
        // iteratively build each decision tree
        for(let i = 0; i < this.n_estimators; i++) {

            // using bootstrap and get the out-of-bag samples
            let [sampled_data, oob_data] = this.bootstrap(verbose);

            // get the input ready for bootstrap sampling
            let [training_data, features, feature_indexes] = this.get_random_features(sampled_data, verbose);

            let decisionTree = new DecisionTree(this.max_depth, this.min_sample_split);
            decisionTree.fit(training_data, this.feature_names, features, feature_indexes, oob_data);

            let analyzer = new DecisionTreeAnalyzer();
            let result = analyzer.find_node_with_highest_gini_gain(decisionTree);
            this.trees.push({"id": i, "model": decisionTree, "performance": 0.0});
        }

        this.calculate_oob_score();

        if(verbose){
            console.log("OOB Score:",this.oob_score);
        }
    }




    bootstrap(verbose = false){
        // make a copy of the original data
        let training_copy = JSON.parse(JSON.stringify(this.training_set));

        // shuffle the data to add randomness
        training_copy = this.helper.shuffle(training_copy);

        // spliting the data into training data and oob data
        let endIndex = Math.floor(this.max_bootstrap_samples * training_copy.length);

        let oob_samples = training_copy.slice(endIndex, training_copy.length);
        let training_samples = training_copy.slice(0,endIndex);

        if (verbose){
            console.log("Bootstrap: training sample:", training_samples.length, " OOB samples:", oob_samples.length);
         }
        // return training data for building our tree and the OOB data for OOB score
        return [training_samples, oob_samples];
    }



    calculate_oob_score(){
        // oob dataset is essentially our training set
        let oob_set = JSON.parse(JSON.stringify(this.training_set));
        // getting the target column from the oob set
        let oob_targets = this.helper.getColumn(oob_set, oob_set[0].length-1);

        // for storing all the predictions from the forest
        let predictions = [];

        // iterate through the set and predict each row from the tree that contains that row in the OOB array
        for(let i = 0; i < oob_set.length; i++){
            // current row
            let row = oob_set[i];
            let row_predictions_from_trees = []

            let temp = [];
            // iterate through all the trees
            for(let j = 0; j < this.trees.length; j++){
                // check if the current row is in current tree's oob array
                if (this.trees[j].model.if_row_in_oob_data(row)){
                    let oob_prediction = this.trees[j].model.predict_single_row(row);
                    row_predictions_from_trees.push(oob_prediction);
                    temp.push(j);
                }

            }
            const result = row_predictions_from_trees.reduce(function (acc, curr) {
                return acc[curr] ? ++acc[curr] : acc[curr] = 1, acc
            }, {});


            let majority_vote_result = this.majority_vote(result);
            predictions.push(majority_vote_result);
        }

        this.oob_score = this.evaluation.accuracy_metric(predictions, oob_targets);
        return this.oob_score;
    }


    predict(testing){
        let predictions = [];
        for(const row of testing){
            let predictions_from_trees = []
            for (const treeObj of this.trees){
                let tree = treeObj.model
                let prediction = tree.predict_single_row(row);
                predictions_from_trees.push(prediction);
            }

            const result = predictions_from_trees.reduce(function (acc, curr) {
                return acc[curr] ? ++acc[curr] : acc[curr] = 1, acc
            }, {});
            let predictionName = this.majority_vote(result);
            predictions.push(predictionName);
        }
        return predictions;
    }


    majority_vote(results) {
        let maxCount = 0;
        let predictionName = null;
        for (const key of Object.keys(results)){
            if (results[key] > maxCount){
                predictionName = key
                maxCount = results[key]
            }
        }
        return predictionName
    }



    // evaluate each tree in the current forest
    evaluate(testing, actual, verbose){
        // let each tree in the forest make a prediction on each row on the data
        for(let i = 0; i < this.trees.length; i++){
            // make a copy of the testing data
            // current tree prediction
            let curTreePrediction = this.trees[i].model.predict(testing)

            // current tree accuracy
            this.trees[i].performance = this.evaluation.accuracy_metric(actual, curTreePrediction);
        }

        if(verbose){
            for(let i = 0; i < this.trees.length; i++){
                console.log("id: ", this.trees[i].id, " - performance: ", this.trees[i].performance);
            }
            // console.log("Accuracy of trees in current forest: ", _.pluck(individualTreeAccuracies, "accuracy"))
        }

        // sort the trees array so the accuracy is in ascending order
        this.sortedTrees = _.sortBy(this.trees, 'performance');
        // this.find_best_trees(0.2);
        // this.find_worst_trees(0.2);
        // individualTreeAccuracies = _.sortBy(individualTreeAccuracies, "accuracy")
        // this.sortedTrees = individualTreeAccuracies
    }


    forest_analysis(){
        console.log("Accuracy: ", this.accuracy);
        console.log("Best tree performance: ", _.pluck(this.bestTrees, "performance"));
        console.log("Worst tree performance: ", _.pluck(this.worstTrees, "performance"))
    }



    // evaluate each tree in the current forest
    get_accuracy(testing, actual){
        let predicted = this.predict(testing);
        let accuracy = this.evaluation.accuracy_metric(actual, predicted);
        this.accuracy = accuracy;
        return accuracy;
    }


    test_performance(testing, actual, verbose = false) {
        let predicted = this.predict(testing);
        let accuracy = this.evaluation.accuracy_metric(actual, predicted);
        if (verbose) {
            console.log(accuracy);
        }
        this.accuracy = accuracy;
        return accuracy;
    }


    find_best_trees(percentage = 0.1, verbose = false){
        let bestTrees = []
        this.bestTrees = []
        this.bestTreesIDs = []
        let count = Math.floor(this.trees.length * percentage)
        for(let i = this.trees.length-1; i > this.trees.length-1-count; i--){
            this.bestTreesIDs.push(this.sortedTrees[i].id);
            bestTrees.push(this.sortedTrees[i]);
        }

        this.bestTrees = bestTrees
        if (verbose){
            console.log("best trees ids: ", this.bestTreesIDs)
            console.log("accuracies from the best trees: ", _.pluck(this.bestTrees, "performance"))
        }
        return bestTrees;
    }


    find_worst_trees(percentage = 0.1, verbose = false){
        let worstTrees = []
        this.worstTreesIDs = []
        let count = Math.floor(this.trees.length * percentage)

        for(let i = 0; i < count; i ++){
            this.worstTreesIDs.push(this.sortedTrees[i].id)
            worstTrees.push(this.sortedTrees[i])
        }

        this.worstTrees = worstTrees
        if (verbose){
            console.log("worst trees ids: ", this.worstTreesIDs)
            console.log("accuracies from the worst trees: ", _.pluck(this.worstTrees, "performance"))
        }

        return worstTrees;
    }


    add_trees_withids(addedTrees, ids){
        let i = 0;
        for(const t of addedTrees){
            let copyOfTree = JSON.parse(JSON.stringify(t.model))
            let newTree = new DecisionTree();
            newTree.parse(copyOfTree);
            this.trees.push({"id": ids[i], "model": newTree, "performance": t.performance});
            i+=1;
        }
        this.trees = _.sortBy(this.trees, "id");
        this.sortedTrees = _.sortBy(this.trees, "performance");
    }

    add_trees(addedTrees){
        for(const t of addedTrees){
            let copyOfTree = JSON.parse(JSON.stringify(t.model))
            let newTree = new DecisionTree();
            newTree.parse(copyOfTree);
            this.trees.push({"id": t.id, "model": newTree, "performance": t.performance});
        }

        this.trees = _.sortBy(this.trees, "id");
        this.sortedTrees = _.sortBy(this.trees, "performance");
    }



    get_tree(index){
        let selected_tree = this.trees[index]
        return this._treeCopy(selected_tree);
    }


    pop_tree_by_id(id){
        for(let i = 0; i < this.trees.length; i++){
            if(this.trees[i].id === id){
                let selected_tree = this._treeCopy(this.trees[i]);
                this.trees.splice(i,1)
                return selected_tree;
            }
        }
        return -1;
    }


    pop_tree_by_index(index){
        let selected_tree = this._treeCopy(this.trees[index]);
        this.trees.splice(index,1)
        return selected_tree;
    }


    _treeCopy(tree){
        let new_tree = new DecisionTree();
        let raw_tree_data = JSON.parse(JSON.stringify(tree.model));
        new_tree.parse(raw_tree_data);
        let tree_copy = ({"id": tree.id, "model": new_tree, "performance": tree.performance});
        return tree_copy;
    }

}


export{RandomForest}