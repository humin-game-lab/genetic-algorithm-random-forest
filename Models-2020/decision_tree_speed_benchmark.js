/**
 * Computes weight based on three factors:
 *   items sent
 *   items received
 *   last timestamp
 */

import {ArrayHelper} from "./array_helper.js"
import {DataFrame} from "./data_frame.js";
import {FastDecisionTree} from "./fast_decision_tree.js"
import {DecisionTree} from "./decision_tree.js";
import {Parser} from "./parser.js";
import {Evaluation} from "./evaluation.js";

let helper = new ArrayHelper();
let evaluation = new Evaluation();
let parser = new Parser();

const datasetConfig = JSON.parse(await Deno.readTextFile("./config/cleaned_dataset.json"));
let dataset = datasetConfig["mlbench_Satellite"];

const training_filenames = dataset.training_filenames;
const optimization_filenames = dataset.validation_filenames;
const testing_filenames = dataset.testing_filenames;

const columns_not_use = dataset.columns_not_use;
const target_name = dataset.target_name;
const directory = dataset.directory;

let total_files = training_filenames.length;
let trainings = [];
let optimizations = [];
let x_testings = [];
let y_testings = []
let feature_names = []


// reading in all dataframes
for(let i = 0; i < total_files; i++){
    let training_filename = training_filenames[i];
    let testing_filename = testing_filenames[i]
    let optimization_filename = optimization_filenames[i]

    let training_filePath = directory + training_filename;
    let optimization_filePath = directory + optimization_filename;
    let testing_filePath = directory + testing_filename;

    console.log(`\nParsing Training dataset from ${training_filePath}`);
    console.log(`Parsing Testing dataset from ${testing_filePath}`);

    let training_df = new DataFrame(training_filePath, target_name, columns_not_use);
    let optimization_df = new DataFrame(optimization_filePath, target_name, columns_not_use);
    let testing_df = new DataFrame(testing_filePath, target_name, columns_not_use);
    feature_names = training_df.featureNames;

    await training_df.preprocess();
    await optimization_df.preprocess();
    await testing_df.preprocess();

    let training_set = helper.JSONtoArray(training_df.dataframe);
    let optimization_set = helper.JSONtoArray(optimization_df.dataframe);
    let testing_set = helper.JSONtoArray(testing_df.dataframe);
    let y_testing = helper.getColumn(testing_set, feature_names.length-1);
    let x_testing = helper.removeColumn(testing_set, feature_names.length-1);

    trainings.push(training_set);
    optimizations.push(optimization_set);
    x_testings.push(x_testing);
    y_testings.push(y_testing);
}



console.log("\n========== Finished Parsing Dataset ==========");
console.log(trainings.length, x_testings.length);

let max_depth = 5;
let min_split = 10;

function decision_tree_benchmark(training_sets, testing_sets, testing_targets, feature_names, index, n){
    console.log("\n========== Training Regular Decision Tree ==========");
    let start = performance.now();
    let time = []
    let regular_dts = [];
    for(let i = 0; i < n; i++){
        let training_set = training_sets[i];
        let start_time = performance.now();
        let dt = new DecisionTree(max_depth, min_split);
        dt.fit(training_set, feature_names, feature_names, indexes);
        let end_time = performance.now();
        let duration_each = end_time - start_time;
        time.push(duration_each)
        regular_dts.push(dt);
    }
    let end = performance.now();
    let duration = end - start;
    console.log("Call to Training Regular Decision Tree took", duration/1000, "seconds.");
    console.log("Each training phase: ", time);
}

function fast_decision_tree_benchmark(training_sets, testing_sets, testing_targets, feature_names, index, n){
    console.log("\n========== Training Fast Decision Tree ==========");
    let start = performance.now();
    let fast_dts = [];
    let time = [];
    for(let i = 0; i < n; i++){
        let training_set = training_sets[i];
        let fast_DT = new FastDecisionTree(max_depth, min_split);
        let start_time = performance.now();
        fast_DT.fit(training_set, feature_names, feature_names, indexes);
        let end_time = performance.now();
        let duration_each = end_time - start_time;
        time.push(duration_each);
        fast_dts.push(fast_DT);
    }
    let end = performance.now();
    let duration = end - start;
    console.log("Call to Training Fast Decision Tree took", duration/1000, "seconds.");
    console.log("Each training phase: ", time);
}


async function benchmark_test(){
    const datasetConfig = JSON.parse(await Deno.readTextFile("./config/cleaned_dataset.json"));

    let dataset = datasetConfig["mlbench_Satellite"];

    const training_filenames = dataset.training_filenames;
    const optimization_filenames = dataset.validation_filenames;
    const testing_filenames = dataset.testing_filenames;

    const columns_not_use = dataset.columns_not_use;
    const target_name = dataset.target_name;
    const directory = dataset.directory;



    let index = 0;
    let training_filename = training_filenames[index];
    let testing_filename = testing_filenames[index]
    let optimization_filename = optimization_filenames[index]

    let training_filePath = directory + training_filename;
    let optimization_filePath = directory + optimization_filename;
    let testing_filePath = directory + testing_filename;

    console.log("\n========== Finished Configuration ==========");
    console.log("Training file: ", training_filePath);
    console.log("Optimization file: ", optimization_filePath);
    console.log("Testing file: ", testing_filePath);

    let training_df = new DataFrame(training_filePath, target_name, columns_not_use);
    let optimization_df = new DataFrame(optimization_filePath, target_name, columns_not_use);
    let testing_df = new DataFrame(testing_filePath, target_name, columns_not_use);

    await training_df.preprocess();
    await optimization_df.preprocess();
    await testing_df.preprocess();
    let feature_names = training_df.featureNames;


    let training_set = helper.JSONtoArray(training_df.dataframe);
    let optimization_set = helper.JSONtoArray(optimization_df.dataframe);
    let testing_set = helper.JSONtoArray(testing_df.dataframe);
    let y_testing = helper.getColumn(testing_set, feature_names.length-1);
    let x_testing = helper.removeColumn(testing_set, feature_names.length-1);


    console.log("\n========== Finished Raw Data Parsing ==========");
    console.log("Training dataset: ", training_set.length);
    console.log("Optimization dataset: ", optimization_set.length);
    console.log("Testing dataset: ", testing_set.length);


    let indexes = [];
    for (let i = 0; i < feature_names.length; i ++){
        indexes.push(i);
    }

    let Fast_DT = new FastDecisionTree(20, 10 );
    let DT = new DecisionTree(20, 10);

    console.log("\n========== Training Fast Decision Tree ==========");
    const start_time_fast_dt = performance.now();
    Fast_DT.fit(training_set, feature_names, indexes);
    const end_time_fast_dt = performance.now();
    const duration_fast_dt = end_time_fast_dt - start_time_fast_dt;
    console.log("Call to Fast Decision Tree took", duration_fast_dt/1000, "seconds.");



    console.log("\n========== Training Regular Decision Tree ==========");
    const start_time_dt = performance.now();
    DT.fit(training_set, feature_names, feature_names, indexes);
    const end_time_dt = performance.now();
    const duration_dt = end_time_dt - start_time_dt;
    console.log("Call to Regular Decision Tree took", duration_dt/1000, "seconds.");


    console.log();
    console.log("Call to Fast Decision Tree took", duration_fast_dt, "milliseconds.");
    console.log("Call to Regular Decision Tree took", duration_dt, "milliseconds.");
    console.log();


    console.log("\n========== Predictions From Decision Tree ==========");
    const start_time_predict = performance.now();
    let predicted1 = Fast_DT.predict(x_testing);
    const end_time_predict = performance.now();
    let predicted2 = DT.predict(x_testing);
    const end_time_predict_dt = performance.now();

    console.log("Call to Predict Fast Decision Tree took", end_time_predict - start_time_predict, "milliseconds.");
    console.log("Call to Predict Regular Decision Tree took", end_time_predict_dt - end_time_predict, "milliseconds.");

    console.log("Fast Decision Tree Accuracy: ", evaluation.accuracy_metric(predicted1, y_testing))
    console.log("Decision Tree Accuracy: ", evaluation.accuracy_metric(predicted2, y_testing))

}


let indexes = [];
for (let i = 0; i < feature_names.length; i ++){
    indexes.push(i);
}


// decision_tree_benchmark(trainings, x_testings, y_testings, feature_names, indexes, 5);
// fast_decision_tree_benchmark(trainings, x_testings, y_testings, feature_names, indexes, 5);
