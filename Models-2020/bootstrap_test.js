import {ArrayHelper} from "./array_helper.js"
import {DataFrame} from "./data_frame.js";
import {Parser} from "./parser.js";
import {Evaluation} from "./evaluation.js";
import {FastRandomForest} from "./fast_random_forest.js"
import {RandomForest} from "./random_forest.js"


let helper = new ArrayHelper();
let evaluation = new Evaluation();
let parser = new Parser();


const datasetConfig = JSON.parse(await Deno.readTextFile("./config/cleaned_dataset.json"));
let dataset = datasetConfig["breast_cancer_dataset"];

const training_filenames = dataset.training_filenames;
const optimization_filenames = dataset.validation_filenames;
const testing_filenames = dataset.testing_filenames;

const columns_not_use = dataset.columns_not_use;
const target_name = dataset.target_name;
const directory = dataset.directory;



let index = 0;
let training_filename = training_filenames[index];
let testing_filename = testing_filenames[index]
let optimization_filename = optimization_filenames[index]

let training_filePath = directory + training_filename;
let optimization_filePath = directory + optimization_filename;
let testing_filePath = directory + testing_filename;

console.log("\n========== Finished Configuration ==========");
console.log("Training file: ", training_filePath);
console.log("Optimization file: ", optimization_filePath);
console.log("Testing file: ", testing_filePath);

let training_df = new DataFrame(training_filePath, target_name, columns_not_use);
let optimization_df = new DataFrame(optimization_filePath, target_name, columns_not_use);
let testing_df = new DataFrame(testing_filePath, target_name, columns_not_use);

await training_df.preprocess();
await optimization_df.preprocess();
await testing_df.preprocess();
let feature_names = training_df.featureNames;


let training_set = helper.JSONtoArray(training_df.dataframe);
let optimization_set = helper.JSONtoArray(optimization_df.dataframe);
let testing_set = helper.JSONtoArray(testing_df.dataframe);
let y_testing = helper.getColumn(testing_set, feature_names.length-1);
let x_testing = helper.removeColumn(testing_set, feature_names.length-1);


console.log("\n========== Finished Raw Data Parsing ==========");
console.log("Training dataset: ", training_set.length);
console.log("Optimization dataset: ", optimization_set.length);
console.log("Testing dataset: ", testing_set.length);

