import {DecisionTree} from "./decision_tree.js";
import {RandomForest} from "./random_forest.js";
import {ArrayHelper} from "./array_helper.js";
import {Evaluation} from "./evaluation.js";
import {Parser} from "./parser.js";
import {DecisionTreeAnalyzer} from "./decision_tree_analyzer.js";

/*
params:
     selection_rate:  selection ratio
     crossover_rate:  crossover ratio
     mutation_rate:   mutation ratio
     crossover_type:  type of the crossover operator
     max_generation:  maximum number of generation/epochs
*/

class GeneticAlgorithm{
    constructor(dataset, feature_names) {
        // full dataset and feature names
        this.dataset = dataset;
        this.feature_names = feature_names;

        this.generation_counter = 0;
        this.initial_population = null;


        this.training_set = []
        this.optimization_set = []
        this.optimization_target = []

        this.testing_set = []
        this.testing_target = []

        // each generation of Random Forest is stored in here
        this.generations = [];
        this.current_population = null;
        this.current_testing_set = [];
        this.current_testing_target = [];
        this.accuracies = [];
        this.local_max_counter = 0;



        // Config files for GA and RF
        this.GA_CONFIG = null;
        this.RF_CONFIG = null;

        this.helper = new ArrayHelper();
        this.evaluation = new Evaluation();
        this.parser = new Parser();
    }

    set_params(GA_CONFIG, RF_CONFIG){
        this.GA_CONFIG = GA_CONFIG;
        this.RF_CONFIG = RF_CONFIG;

        // parameters for the Genetic Algorithm model
        this.selection_rate = GA_CONFIG.selection_rate;
        this.crossover_rate = GA_CONFIG.crossover_rate;
        this.mutation_rate  = GA_CONFIG.mutation_rate;
        this.crossover_type = GA_CONFIG.crossover_type;
        this.max_generation = GA_CONFIG.max_generation;
        this.termination_condition = GA_CONFIG.termination_condition;
    }


    Run(GA_CONFIG, RF_CONFIG){
        // set hyper-parameters for Genetic Algorithm and Random Forest
        this.set_params(GA_CONFIG, RF_CONFIG)

        // initialize our initial population
        this.Initialize();
        while(!this.Terminate()){
            this.Evaluation(false);
            this.Fitness();
            this.Selection();
            this.Accuracy(false, false);
            this.Populate();
        }
        this.Statistic();
        this.Compare();
    }



    Test(GA_CONFIG, RF_CONFIG, trainings, x_validations, y_validations, x_testings, y_testings){
        // set hyper-parameters for Genetic Algorithm and Random Forest
        this.set_params(GA_CONFIG, RF_CONFIG)

        // initialize our initial population
        this.Init(trainings, x_validations, y_validations, x_testings, y_testings);
        // while(!this.Terminate()){
            this.Evaluation(false);
            this.Fitness();
            this.Accuracy(true);
            this.Populate();
            // this.Build_Forest();
            // this.Statistic();
        // }
        //
        this.Statistic();
        // this.Compare();
    }


    Init(training, x_validations, y_validations, x_testings, y_testings){

        // let [training, validation, validation_target] = this.helper.train_test_split(training_set, 0.5, 0.5);
        // create train test split

        // store our initial training set
        this.training_set = JSON.parse(JSON.stringify(training));

        this.optimization_set = JSON.parse(JSON.stringify(x_validations));
        this.optimization_target = JSON.parse(JSON.stringify(y_validations));

        this.testing_set = JSON.parse(JSON.stringify(x_testings));
        this.testing_target = JSON.parse(JSON.stringify(y_testings));

        console.log("training_set: ", this.training_set.length);
        console.log("testing_set: ", this.testing_set.length, " target: ", this.testing_target.length);
        console.log("optimization_set: ", this.optimization_set.length, " target: ", this.optimization_target.length);

        // create the first population
        let randomForestClassifier = new RandomForest(this.RF_CONFIG);
        // train the Random Forest model
        randomForestClassifier.fit(this.training_set, this.feature_names);

        // assign it as the initial population
        this.initial_population = randomForestClassifier;

        // assign it as the current population
        let forest_copy = new RandomForest(this.RF_CONFIG);
        forest_copy.parse_forest(randomForestClassifier);
        this.current_population = forest_copy;
    }






    TrainTestValidate(GA_CONFIG, RF_CONFIG){
        // set hyper-parameters for Genetic Algorithm and Random Forest
        this.set_params(GA_CONFIG, RF_CONFIG)
        // initialize our initial population
        this.Start();

        while(this.Terminate() === false){
            this.Evaluation_Optimization();
            this.Fitness();
            this.Selection();
            this.Validate();
            this.Populate();
            // this.Feature_Importance();
        }
    }

    Filter(){
        for(let i = 0; i < this.current_population.trees.length; i++){
            if(this.current_population.trees[i].performance ===0){
                let id = this.current_population.trees[i].id;
                this.current_population.pop_tree_by_id(id);
            }
        }
    }

    RunWithRestart_old(GA_CONFIG, RF_CONFIG){
        // set hyper-parameters for Genetic Algorithm and Random Forest
        this.set_params(GA_CONFIG, RF_CONFIG)
        // initialize our initial population
        this.Initialize();
        while(this.Terminate() === false){
            this.Evaluation_Optimization();
            this.Fitness();
            this.Selection();
            let accuracy = this.get_validate();
            let tries = 0
            if(this.generation_counter > 1){
                this.Filter();
                let accuracy = this.get_validate();
                while (accuracy <= this.accuracies[this.accuracies.length-1].performance && tries < 100){
                    let new_rf = this.Build_Forest();
                    let new_genes = new_rf.find_best_trees(this.selection_rate);
                    this.replace_bad_trees_with_new_trees(new_genes)
                    accuracy = this.get_validate();
                    tries += 1;
                    this.Evaluation_Optimization();
                    this.Fitness();
                    // console.log("current accuracy: ", accuracy, "previous accuracy: ",this.accuracies[this.accuracies.length-1].performance);
                }
                // console.log("uses random restart", this.current_population.trees.length);
            }
            this.Validate();
            this.Populate();
        }
    }



    RunWithRestart(GA_CONFIG, RF_CONFIG){
        // set hyper-parameters for Genetic Algorithm and Random Forest
        this.set_params(GA_CONFIG, RF_CONFIG)
        // initialize our initial population
        this.Initialize();
        while(this.Terminate() === false){
            this.Evaluation_Optimization();
            this.Fitness();
            let accuracy_before_selection = this.get_validate();
            this.Selection();
            let accuracy = this.get_validate();
            if(accuracy === 100){
                console.log("Accuracy: 100%!");
                Deno.exit(1)
            }
            // console.log("accuracy before selection: ", accuracy_before_selection, " after:", accuracy);
            if(this.generation_counter > 1){
                if(accuracy <= this.accuracies[this.accuracies.length-1].performance){
                    this.local_max_counter += 1;
                }else{
                    this.local_max_counter = 0;
                }
                if(this.local_max_counter > 2){
                    console.log("local maximal - trying restart");
                    let tries = 0;
                    let loop = false
                    while (tries < 100 && !loop){
                        this.Evaluation_Optimization();
                        let new_rf = this.Build_Forest();
                        let new_genes = new_rf.find_best_trees(this.selection_rate);
                        let [new_accuracy, test_acc] = this.try_replace_bad_trees_with_new_trees(new_genes);
                        let old_accuracy = this.accuracies[this.accuracies.length-1].performance;
                        if (test_acc > old_accuracy){
                            console.log("[T] prev: ", old_accuracy, "new (opt):", new_accuracy, "new (test):", test_acc);
                            this.replace_bad_trees_with_new_trees(new_genes);
                            loop = true;
                        }else{
                            // console.log("[X] prev: ", old_accuracy, "new (opt):", new_accuracy, "new (test):", test_acc);
                        }

                        tries += 1;
                        this.Evaluation_Optimization();
                        this.Fitness();
                        // console.log("worst tree id:", this.current_population.worstTreesIDs);
                        this.local_max_counter = 0
                    }
                    console.log("uses random restart - tries: ",tries);
                }
            }
            this.Validate();
            // console.log(this.current_population.worstTreesIDs)
            this.Populate();
            // if(this.accuracies[this.accuracies.length-1].performance === 100){
            //     console.log("Accuracy: 100%!");
            //     Deno.exit(1);
            // }
        }
    }


    replace_bad_trees_with_new_trees(newTrees){
        let bad_tree_ids = this.current_population.worstTreesIDs;
        this.current_population.remove_bad_trees(bad_tree_ids);
        this.current_population.add_trees_withids(newTrees, bad_tree_ids);
        // console.log("highest performance tree:", this.current_population.bestTrees[0].performance);
    }



    try_replace_bad_trees_with_new_trees(newTrees){
        let current_forest_copy = new RandomForest(this.RF_CONFIG);
        current_forest_copy.parse_forest(this.current_population);
        let bad_tree_ids = this.current_population.worstTreesIDs;
        current_forest_copy.remove_bad_trees(bad_tree_ids);
        current_forest_copy.add_trees_withids(newTrees, bad_tree_ids);
        current_forest_copy.evaluate(this.optimization_set, this.optimization_target);
        let optimization_accuracy = current_forest_copy.get_accuracy(this.optimization_set, this.optimization_target);
        let testing_accuracy = current_forest_copy.get_accuracy(this.testing_set, this.testing_target);
        return [optimization_accuracy, testing_accuracy];
    }


    async Output(filename = "output.csv"){
        let text = "Generation, Accuracy \n";
        for(let i = 0; i < this.accuracies.length; i++){
            text += i + ", " + this.accuracies[i].performance + "\n";
        }

        await Deno.writeTextFile(filename, text);
        console.log("Genetic algorithms" + filename + ".dot");
    }


    KFoldValidation(n_folds = 10, verbose = false){
        console.log("K-Fold Validation");
        let k_Fold_Cross_Validation = this.helper.cross_validation_split(this.dataset, 10);
        let results = [];
        for(let k = 0; k < k_Fold_Cross_Validation.length; k++){
            let [training, testing, testing_target] = [k_Fold_Cross_Validation[k].training_set, k_Fold_Cross_Validation[k].testing_set, k_Fold_Cross_Validation[k].testing_target];
            let randomForestClassifier = new RandomForest(this.RF_CONFIG);
            randomForestClassifier.fit(training, this.feature_names, verbose);

            let accuracy = randomForestClassifier.test_performance(testing, testing_target, verbose);
            let oob_score = randomForestClassifier.calculate_oob_score(testing, testing_target, verbose);
            results.push({accuracy: accuracy, oob_score: oob_score});
        }
        console.log(results);
    }


    Initialize(verbose = false){
        // create train test split
        let [training, testing, testing_target, validation, validation_target] = this.helper.train_test_validation_split(this.dataset,
            0.4,
            0.3,
            0.3,
            true);

        console.log("training: ", training[0]);
        console.log("testing: ", testing[0]);
        console.log("validation: ", validation[0]);
        console.log("validation_target: ", validation_target[0]);

        // store our initial training set
        this.training_set = JSON.parse(JSON.stringify(training));

        this.optimization_set = JSON.parse(JSON.stringify(validation));
        this.optimization_target = JSON.parse(JSON.stringify(validation_target));

        this.testing_set = JSON.parse(JSON.stringify(testing));
        this.testing_target = JSON.parse(JSON.stringify(testing_target));

        // create the first population
        let randomForestClassifier = new RandomForest(this.RF_CONFIG);
        // train the Random Forest model
        randomForestClassifier.fit(training, this.feature_names, verbose);

        // assign it as the initial population
        this.initial_population = randomForestClassifier;

        // assign it as the current population
        let forest_copy = new RandomForest(this.RF_CONFIG);
        forest_copy.parse_forest(randomForestClassifier);
        this.current_population = forest_copy;
    }


    Start(verbose = false){
        // create train test split
        let [training, testing, testing_target, validation, validation_target] = this.helper.train_test_validation_split(this.dataset,
            0.3,
            0.3,
            0.3,
            false);

        // store our initial training set
        this.initial_training_set = JSON.parse(JSON.stringify(training));
        this.optimization_set = validation;
        this.optimization_target = validation_target;
        this.testing_set = testing;
        this.testing_target = testing_target;


        // create the first population
        let randomForestClassifier = new RandomForest(this.RF_CONFIG);

        // train the Random Forest model
        randomForestClassifier.fit(training, this.feature_names, verbose);

        // assign it as the initial population
        this.initial_population = randomForestClassifier;

        // assign it as the current population
        let forest_copy = new RandomForest(this.RF_CONFIG);
        forest_copy.parse_forest(randomForestClassifier);
        this.current_population = forest_copy;
    }

    Build_Forest(){
        // create the first population
        let randomForestClassifier = new RandomForest(this.RF_CONFIG);
        // train the Random Forest model
        randomForestClassifier.fit(this.training_set, this.feature_names);
        randomForestClassifier.evaluate(this.optimization_set, this.optimization_target);
        return randomForestClassifier;
    }


    Feature_Importance(){
        let counter = {}
        let DecisonTreeAnalyzer = new DecisionTreeAnalyzer();
        for(let i = 0; i < this.current_population.trees.length; i++){
            let features = DecisonTreeAnalyzer.find_node_with_highest_gini_gain(this.current_population.trees[i].model)[0];
            if (counter[features]){
                // Add 1 to the counter
                counter[features] += 1
            } else {
                // Otherwise, create a new counter for that new letter
                counter[features] = 1
            }
        }
        console.log(counter);
    }



    // evaluate the Random Forest model performance
    Evaluation(verbose = false){
        this.current_population.evaluate(this.optimization_set,
                                         this.optimization_target,
                                         verbose);
    }

    // using optimization set to evaluate
    Evaluation_Optimization(verbose = false){
        this.current_population.evaluate(this.optimization_set,
                                         this.optimization_target,
                                         verbose);
    }


    Fitness(verbose = false){
        this.current_population.find_best_trees(this.selection_rate, false);
        this.current_population.find_worst_trees(this.selection_rate, false);
    }



    Selection(verbose = false){
        this.Crossover();
        this.Mutation();
    }


    Populate(verbose = false){
        let nextGeneration = new RandomForest(this.RF_CONFIG);
        nextGeneration.parse_forest(JSON.parse(JSON.stringify(this.current_population)));

        // this.generations.push({epoch: this.generation_counter, population: JSON.parse(JSON.stringify(this.current_population))});
        this.generation_counter += 1;
        this.current_population = nextGeneration;
    }

    Validate(verbose = false){
        let accuracy = this.current_population.test_performance(this.testing_set, this.testing_target, verbose);
        let oobScore = this.current_population.calculate_oob_score();
        this.accuracies.push({epoch: this.generation_counter, performance: accuracy,  oob_score: oobScore});
        console.log("Generation-"+this.generation_counter+",",  accuracy);
    }

    get_validate(verbose = false){
        let accuracy = this.current_population.test_performance(this.testing_set, this.testing_target, verbose);
        let oobScore = this.current_population.calculate_oob_score();
        // this.accuracies.push({epoch: this.generation_counter, performance: accuracy,  oob_score: oobScore});
        if (verbose){
            console.log("Generation", this.generation_counter, "accuracy:", accuracy, "oob:",oobScore);
        }
        return accuracy;
    }



    Accuracy(verbose = false){
        let accuracy = this.current_population.get_accuracy(this.testing_set, this.testing_target, verbose);
        let oobScore = this.current_population.calculate_oob_score();
        this.accuracies.push({epoch: this.generation_counter, performance: accuracy, oob_score: oobScore});
        console.log("Generation", this.generation_counter, "-", accuracy);
        return accuracy;
    }


    Terminate(verbose = false){
        // we need at least 2 populations
        if (this.generation_counter < 3){
            return false;
        }

        // if the maximum number of generation equals to the number of generations we have
        if (this.generation_counter >= this.max_generation){
            return true;
        }

        // Termination condition 1: Compare the average performance from [0, previous population] and the average performance from [initial_population, current population]
        // Continue only if Avg_Accuracy[i] > Avg_Accuracy[i-1]
        if (this.termination_condition === "OPTIMAL"){
            let prev_avg = this.helper.get_average_from_objects(this.accuracies.slice(0, this.generation_counter-1), "performance");
            let cur_avg = this.helper.get_average_from_objects(this.accuracies, "performance");
            return prev_avg > cur_avg;
        }

        if(this.termination_condition === "NONE"){
            return false;
        }

        // Termination condition 2: an Integer shows the maximum number of decreasing population we are allowed
        // Terminate if Accuracy[i] < Accuracy[i-1] < .... < Accuracy[i-n]
        // check if our genetic algorithm stop improve our current population
        if(this.accuracies.length > this.termination_condition){
            let decrease_counter = 0
            for(let i = 0; i < this.termination_condition; i++){
                if(this.accuracies[this.accuracies.length-i-1].performance > this.accuracies[this.accuracies.length-i-2].performance){
                    decrease_counter += 1;
                }
            }
            if (decrease_counter === this.termination_condition){
                return true;
            }
        }
        return false;
    }



    // mutation operator
    Mutation(verbose = false){
        // getting the number of mutation on the current forest
        let forest_size = this.current_population.trees.length;
        let number_of_mutation = forest_size * this.mutation_rate;

        // for each mutation:
        if(verbose){
            console.log("Mutating", number_of_mutation, "genes from the current population")
        }

        let mutatedIndex = []
        for(let i = 0; i < number_of_mutation; i++){
            // console.log("mutating: Gen:", this.generation_counter, " - ", i, number_of_mutation)
            let mutate_from_index = Math.floor(Math.random() * this.current_population.trees.length);
            let mutate_from_tree = this.current_population.pop_tree_by_index(mutate_from_index);

            let mutate_to_index = Math.floor(Math.random() * this.current_population.trees.length);
            let mutate_to_tree = this.current_population.pop_tree_by_index(mutate_to_index);

            mutatedIndex.push(mutate_to_index);
            let[tree1, tree2] = this.random_mutation(mutate_from_tree, mutate_to_tree, false);
            tree1.performance = tree1.model.get_performance(this.optimization_set, this.optimization_target);
            tree2.performance = tree2.model.get_performance(this.optimization_set, this.optimization_target);
            this.current_population.add_trees([tree1, tree2]);
        }
        if (verbose){
            console.log("Mutated Genes: ", mutatedIndex);
        }
    }


    random_mutation(tree1, tree2, verbose = false){
        // now we find where do we swap branches

        let non_leaf_nodes_tree1 = tree1.model.find_available_nodes();
        let non_leaf_nodes_tree2 = tree2.model.find_available_nodes();


        // shuffle the possible split point
        non_leaf_nodes_tree1 = this.helper.shuffle(non_leaf_nodes_tree1);
        non_leaf_nodes_tree2 = this.helper.shuffle(non_leaf_nodes_tree2);

        // decide which tree should be the mutated tree
        let tree1_split_node_index = Math.floor(Math.random() * non_leaf_nodes_tree1.length);
        let tree2_split_node_index = Math.floor(Math.random() * non_leaf_nodes_tree2.length);

        let tree1_split_node = non_leaf_nodes_tree1[tree1_split_node_index];
        let tree2_split_node = non_leaf_nodes_tree2[tree2_split_node_index];

        // choose tree1 as the mutated tree if random number is [0,0.5)
        // choose tree2 as the mutated tree if random number is [0.5,1)
        let improve = false
        let max_try = 100;
        let counter = 0;

        while(!improve && counter < max_try){
            // making a copy of both trees
            let tree1_copy = new DecisionTree();
            let tree2_copy = new DecisionTree();
            tree1_copy.parse(tree1.model);
            tree2_copy.parse(tree2.model);

            let choice = Math.random();
            if (choice < 0.5){
                tree1_copy.find_and_replace_node(tree1_copy.root, tree1_split_node, tree2_split_node, true);
                improve = this.evaluation.compare_two_trees(this.optimization_set, this.optimization_target, tree1.model, tree1_copy, verbose);
                // improve = this.evaluation.compare_two_trees(this.optimization_set, this.optimization_target, tree1.model, tree1_copy, verbose);
                if(improve){
                    tree1.model.find_and_replace_node(tree1.model.root, tree1_split_node, tree2_split_node, true);
                    break;
                }
            }else{
                tree2_copy.find_and_replace_node(tree2_copy.root, tree2_split_node, tree1_split_node, true);
                improve = this.evaluation.compare_two_trees(this.optimization_set, this.optimization_target, tree2.model, tree2_copy, verbose);
                if(improve){
                    tree2.model.find_and_replace_node(tree2.model.root, tree2_split_node, tree1_split_node, true);
                    break;
                }
            }
            counter += 1;
        }
        return [tree1, tree2];
    }


    // Crossover operator
    Crossover(verbose = false){
        let current_gen_best_trees = this.current_population.bestTrees;
        let current_gen_worst_trees = this.current_population.worstTrees;

        let crossover_number = Math.floor(this.current_population.trees.length * this.crossover_rate)
        let children = [];


        if(verbose){
            console.log("Crossover", crossover_number, "genes from the current population")
        }


        for(let i = 0; i < crossover_number; i++){
            let child = null;
            // using single crossover technique
            if(this.crossover_type === "SINGLE"){
                let improve = false
                let max_try = 100;
                let counter = 0;
                let good_tree = this.current_population.pop_tree_by_id(current_gen_best_trees[i].id);
                let bad_tree = this.current_population.pop_tree_by_id(current_gen_worst_trees[i].id);
                while(!improve && counter < max_try){
                    child = this.single_crossover(good_tree, bad_tree, this.crossover_type);
                    improve = this.evaluation.compare_two_trees(this.optimization_set, this.optimization_target, bad_tree.model, child.model);
                    // improve = this.evaluation.compare_two_trees(this., this.optimization_target, bad_tree.model, child.model);
                    counter += 1;
                }
                let child_performance = child.model.get_performance(this.optimization_set, this.optimization_target);
                let good_tree_performance = good_tree.model.get_performance(this.optimization_set, this.optimization_target);
                child.performance = child_performance;
                good_tree.performance = good_tree_performance;
                this.current_population.add_trees([child, good_tree]);
            }
        }
        return children;
    }


    Compare(){
        let initial_accuracies = [];

        let end_accuracies = [];
        let initial_oobs = [];
        let end_oobs = [];

        for(let i = 0; i < 10; i++){
            let [training, testing, testing_target] = this.helper.train_test_split(this.dataset,
                                                                                    this.GA_CONFIG.training_percentage,
                                                                                    this.GA_CONFIG.testing_percentage,
                                                                                    false);


            let initial_accuracy = this.initial_population.get_accuracy(testing, testing_target, false);
            let initial_oob = this.initial_population.calculate_oob_score(testing, testing_target, false);

            initial_accuracies.push(initial_accuracy);
            initial_oobs.push(initial_oob);


            let current_accuracy = this.current_population.get_accuracy(testing, testing_target, false);
            let current_oob = this.current_population.calculate_oob_score(testing, testing_target, false);

            end_accuracies.push(current_accuracy)
            end_oobs.push(current_oob);
        }

        const average = arr => arr.reduce((a,b) => a + b, 0) / arr.length;

        console.log("\nInitial Population: Accuracy - ", average(initial_accuracies), "  OOB Scores:", average(initial_oobs));
        console.log("End Population: Accuracy - ", average(end_accuracies), "  OOB Scores:", average(end_oobs));
        console.log();
    }


    Statistic(){
        console.log("Accuracies: ");
        console.log(this.accuracies);
    }





    remove_bad_trees(){
        for(let i = 0; i < this.current_population.worstTrees.length; i++){
            this.current_population.pop_tree_by_id(this.current_population.worstTrees[i].id);
        }
        this.current_population.worstTrees = []
        let ids = this.current_population.worstTreesIDs;
        this.current_population.worstTreesIDs = []
        return ids;
    }

    double_crossover(tree1, tree2){
        // now we find where do we swap branches
        let non_leaf_nodes_tree1 = tree1.model.find_available_nodes();
        let non_leaf_nodes_tree2 = tree2.model.find_available_nodes();

        // shuffle the possible split point
        non_leaf_nodes_tree1 = this.helper.shuffle(non_leaf_nodes_tree1);
        non_leaf_nodes_tree2 = this.helper.shuffle(non_leaf_nodes_tree2);

        // decide which tree should be the mutated tree
        // let tree1_split_node_index = Math.floor(Math.random() * non_leaf_nodes_tree1.length);
        // let tree2_split_node_index = Math.floor(Math.random() * non_leaf_nodes_tree2.length);
        //
        // let tree1_split_node = non_leaf_nodes_tree1[tree1_split_node_index];
        // let tree2_split_node = non_leaf_nodes_tree2[tree2_split_node_index];
        //
        // let child = {id: tree2.id, model: new DecisionTree(), performance: 0.0};
        // let tree2_copy = JSON.parse(JSON.stringify(tree2.model));
        // child.model.parse(tree2_copy)
        // child.model.find_and_replace_node(child.model.root, tree2_split_node, tree1_split_node, true);
        // return child;

    }




    // tree1 is the good tree and tree2 is the bad tree
    single_crossover(tree1, tree2, criteria = "RANDOM") {

        // now we find where do we swap branches
        let non_leaf_nodes_tree1 = tree1.model.find_available_nodes();
        let non_leaf_nodes_tree2 = tree2.model.find_available_nodes();

        // shuffle the possible split point
        non_leaf_nodes_tree1 = this.helper.shuffle(non_leaf_nodes_tree1);
        non_leaf_nodes_tree2 = this.helper.shuffle(non_leaf_nodes_tree2);

        // decide which tree should be the mutated tree
        let tree1_split_node_index = Math.floor(Math.random() * non_leaf_nodes_tree1.length);
        let tree2_split_node_index = Math.floor(Math.random() * non_leaf_nodes_tree2.length);

        let tree1_split_node = non_leaf_nodes_tree1[tree1_split_node_index];
        let tree2_split_node = non_leaf_nodes_tree2[tree2_split_node_index];

        let child = {id: tree2.id, model: new DecisionTree(), performance: 0.0};
        let tree2_copy = JSON.parse(JSON.stringify(tree2.model));
        child.model.parse(tree2_copy)
        child.model.find_and_replace_node(child.model.root, tree2_split_node, tree1_split_node, true);
        return child;
    }


    output_forest(){
        for(let i = 0; i < this.current_population.trees.length; i++){
            let cur_tree = this.current_population.trees[i].model.model;
            let output_name = String(this.generation_counter) +"-"+ String(this.current_population.trees[i].id)
            this.parser.parseToFile(output_name, this.current_population.trees[i].model);
        }
    }

}
export {GeneticAlgorithm}