class RandomForestAnalyzer{
    constructor(random_forest) {
        this.random_forest = random_forest
    }

    find_features_from_best_trees(){
        let featureMap = {};
        for(let i = 0; i < this.random_forest.bestTrees.length; i++){
            let features_on_tree = this.random_forest.bestTrees[i].feature_indexes;
            for(const feature of features_on_tree){
                featureMap[feature] = featureMap[feature] ? featureMap[feature] + 1 : 1;
            }
        }
        return featureMap;
    }

    find_features_from_worst_trees(){
        let featureMap = {};
        for(let i = 0; i < this.random_forest.worstTrees.length; i++){
            let features_on_tree = this.random_forest.worstTrees[i].feature_indexes;
            for(const feature of features_on_tree){
                featureMap[feature] = featureMap[feature] ? featureMap[feature] + 1 : 1;
            }
        }
        return featureMap;
    }



}

export {RandomForestAnalyzer};