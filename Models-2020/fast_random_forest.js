import "https://deno.land/x/lodash@4.17.19/vendor/underscore/underscore.js";
import {ArrayHelper} from "./array_helper.js";
import {FastDecisionTree} from "./fast_decision_tree.js";
import {Evaluation} from "./evaluation.js";
import {Bootstrap} from "./bootstrap.js";

class FastRandomForest{
    constructor(RF_CONFIG){
        this.n_estimators = RF_CONFIG.n_estimators;
        this.max_features = RF_CONFIG.max_features;
        this.max_depth = RF_CONFIG.max_depth;
        this.min_sample_split = RF_CONFIG.min_sample_split;

        this.max_bootstrap_samples = RF_CONFIG.max_bootstrap_samples;
        this.bootstrap_method = RF_CONFIG.bootstrap_with_replacement;

        this.RF_CONFIG = RF_CONFIG;
        this.training_set = [];
        this.trees = []

        this.helper = new ArrayHelper();
        this.evaluation = new Evaluation();
    }

    parse_forest(rawForestData){
        this.RF_CONFIG = rawForestData.RF_CONFIG;
        this.training_set = rawForestData.training_set;

        for(let i = 0; i < rawForestData.trees.length; i++){
            let dt = new FastDecisionTree();
            dt.parse(rawForestData.trees[i].model);
            this.trees.push({
                id: rawForestData.trees[i].id,
                model: dt,
                performance: rawForestData.trees[i].performance
            });
        }
    }


    build_forest(training_set, feature_names, verbose = false){
        // iteratively build each decision tree
        let [bootstrap_data, oob_data, features, feature_indexes] = this.get_bootstrap_data(training_set, feature_names);
        for(let i = 0; i < this.n_estimators; i++) {
            // using bootstrap and get the out-of-bag samples
            let [cur_sampled_data, cur_oob] = [bootstrap_data[i], oob_data[i]];
            let [cur_feature_names, cur_feature_indexes] = [features[i], feature_indexes[i]];

            // get the input ready for bootstrap sampling
            let decisionTree = new FastDecisionTree(this.max_depth, this.min_sample_split);
            decisionTree.fit(cur_sampled_data, cur_feature_names, cur_feature_indexes);
            this.trees.push({"id": i, "model": decisionTree, "performance": 0.0});
        }
    }


    fit(training_set, feature_names, verbose = false){
        this.build_forest(training_set, feature_names, verbose);
    }


    get_bootstrap_data(training_set, feature_names){
        let all_feature_names = [...feature_names]
        training_set = this.helper.shuffle(training_set);
        let bootstrap_helper = new Bootstrap(training_set, all_feature_names, this.max_features, this.max_bootstrap_samples, this.n_estimators);
        return bootstrap_helper.run(this.bootstrap_method, false);
    }


    calculate_oob_score(tree, oob_data){
        let y_oob = this.helper.getColumn(oob_data);
        let predicted = tree.predict(oob_data);
        let oob_score = this.evaluation.accuracy_metric(predicted, y_oob);
        return oob_score;
    }


    evaluate(x_optimization, y_optimization, verbose){
        for(let i = 0; i < this.trees.length; i++){
            let curTreePrediction = this.trees[i].model.predict(x_optimization)
            this.trees[i].performance = this.evaluation.accuracy_metric(curTreePrediction, y_optimization);
        }

        if(verbose){
            for(let i = 0; i < this.trees.length; i++){
                console.log("id: ", this.trees[i].id, " - performance: ", this.trees[i].performance, " - oob score:: ", this.trees[i].oob_score);
            }
        }
    }


    predict(x_test){
        let predictions = [];
        for(const row of x_test){
            let predictions_from_trees = []
            for (const treeObj of this.trees){
                let tree = treeObj.model
                let prediction = tree.predict_single_row(row);
                predictions_from_trees.push(prediction);
            }

            const result = predictions_from_trees.reduce(function (acc, curr) {
                return acc[curr] ? ++acc[curr] : acc[curr] = 1, acc
            }, {});
            let predictionName = this.majority_vote(result);
            predictions.push(predictionName);
        }
        return predictions;
    }

    majority_vote(results) {
        let maxCount = 0;
        let predictionName = null;
        for (const key of Object.keys(results)){
            if (results[key] > maxCount){
                predictionName = key
                maxCount = results[key]
            }
        }
        return predictionName
    }




    get_accuracy(x_test, y_test){
        let y_predict = this.predict(x_test);
        let accuracy = this.evaluation.accuracy_metric(y_predict, y_test);
        this.accuracy = accuracy;
        return accuracy;
    }

    pop_tree_by_index(index){
        let selected_tree = this.trees[index];
        this.trees.splice(index,1)
        return selected_tree;
    }

    pop_tree_by_id(id){
        for(let i = 0; i < this.trees.length; i++){
            if(this.trees[i].id === id){
                let selected_tree = this.trees[i];
                this.trees.splice(i,1)
                return selected_tree;
            }
        }
        return -1;
    }

    remove_bad_trees(ids){
        for(let i = 0; i < ids.length; i++){
            this.pop_tree_by_id(ids[i]);
        }
    }

    add_trees(addedTrees){
        for(const t of addedTrees){
            // let newTree = new FastDecisionTree();
            let x = {"id": t.id, "performance": t.performance};
            console.log("Adding: ",x);
            this.trees.push({"id": t.id, "model": t.model, "performance": t.performance});
        }
        this.trees = _.sortBy(this.trees, "id");
    }

    add_trees_with_ids(trees, ids){
        for(let i = 0; i < trees.length; i++){
            this.trees.push({"id": ids[i], "model": trees[i].model, "performance": trees[i].performance});
        }
        this.trees = _.sortBy(this.trees, "id");
    }

    add_new_trees(addedTrees){
        let first_tree_id = this.trees[this.trees.length-1].id + 1;
        for(let i = 0; i < addedTrees.length; i++){
            this.trees.push({"id": first_tree_id + i, "model": addedTrees[i].model, "performance": addedTrees[i].performance});
        }
        // this.trees = _.sortBy(this.trees, "id");
    }

    get_sorted_trees(){
        return _.sortBy(this.trees, 'performance');
    }
}


export {FastRandomForest}