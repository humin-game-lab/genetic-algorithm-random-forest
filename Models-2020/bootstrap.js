/*
Bootstrapping resamples the original dataset with replacement many thousands of times to create simulated datasets.
This process involves drawing random samples from the original dataset.
Here’s how it works:
    - The bootstrap method has an equal probability of randomly drawing each original data point for inclusion in the resampled datasets.
    - The procedure can select a data point more than once for a resampled dataset. This property is the “with replacement” aspect of the process.
    - The procedure creates resampled datasets that are the same size as the original dataset.
 */

import {ArrayHelper} from "./array_helper.js";

class Bootstrap{
    constructor(dataset, feature_names, max_features, max_bootstrap_samples, n_observations){
        this.data = dataset;
        this.feature_names = feature_names;
        this.max_features = max_features;
        this.max_bootstrap_samples = Math.floor(dataset.length * max_bootstrap_samples);
        this.n_observations = n_observations;

        this.sampled_data = []
        this.sampled_oob_data = []
        this.features = []
        this.feature_indexes = []
        this.helper = new ArrayHelper();
    }


    run(sample_with_replacement = true,verbose = false){
        for(let i = 0; i < this.n_observations; i++){
            let [bootstrap_sample, oob_sample] = [[], []];
            if(sample_with_replacement){
                [bootstrap_sample, oob_sample] = this.sample_with_replacement();
            }else{
                [bootstrap_sample, oob_sample]= this.sample_without_replacement(verbose);
            }


            let [sample, feature_names, feature_indexes]= this.get_random_features(bootstrap_sample);
            this.sampled_data.push(sample);
            this.sampled_oob_data.push(oob_sample);
            this.features.push(feature_names);
            this.feature_indexes.push(feature_indexes);
        }

        return [this.sampled_data, this.sampled_oob_data, this.features, this.feature_indexes]
    }


    getRandomIndex() {
        let max = this.data.length;
        return Math.floor(Math.random() * max); //The maximum is exclusive and the minimum is inclusive
    }


    sample_with_replacement(verbose = false){
        let cur_bootstrap_sample = [];
        let cur_bootstrap_sample_index = [];
        for(let i = 0; i < this.max_bootstrap_samples; i++){
            let index = this.getRandomIndex();
            cur_bootstrap_sample.push(this.data[index]);
            cur_bootstrap_sample_index.push(index);
        }

        let cur_oob_sample = []
        let all_feature_indexes = [...Array(this.data.length).keys()];
        let cur_oob_index = all_feature_indexes.filter(x =>!cur_bootstrap_sample_index.includes(x));
        for(let i = 0; i < cur_oob_index.length; i++){
            cur_oob_sample.push(this.data[cur_oob_index[i]]);
        }

        if (verbose){
            console.log("Bootstrap: training sample:", cur_bootstrap_sample.length, " OOB samples:", cur_oob_sample.length);
            console.log("bootstrap index:", cur_bootstrap_sample_index);
            console.log("oob data index:", cur_oob_index);
        }
        // return training data for building our tree and the OOB data for OOB score
        return [cur_bootstrap_sample, cur_oob_sample];
    }



    sample_without_replacement(verbose = false){
        let cur_bootstrap_sample = [];
        let cur_bootstrap_sample_index = [];


        while(cur_bootstrap_sample.length < this.max_bootstrap_samples){
            let index = this.getRandomIndex();
            if (cur_bootstrap_sample_index.indexOf(index) === -1){
                cur_bootstrap_sample.push(this.data[index]);
                cur_bootstrap_sample_index.push(index);
            }
        }

        let cur_oob_sample = []
        let all_feature_indexes = [...Array(this.data.length).keys()];
        let cur_oob_index = all_feature_indexes.filter(x =>!cur_bootstrap_sample_index.includes(x));
        for(let i = 0; i < cur_oob_index.length; i++){
            cur_oob_sample.push(this.data[cur_oob_index[i]]);
        }

        if (verbose){
            cur_oob_index.sort((a, b) => a - b);
            cur_bootstrap_sample_index.sort((a, b) => a - b);
            console.log("Bootstrap: training sample:", cur_bootstrap_sample.length, " OOB samples:", cur_oob_sample.length);
            console.log("bootstrap index:", cur_bootstrap_sample_index);
            console.log("oob data index:", cur_oob_index);
        }
        // return training data for building our tree and the OOB data for OOB score
        return [cur_bootstrap_sample, cur_oob_sample];
    }


    get_random_features(bootstrap_data, verbose = false){
        let number_of_features_per_tree = 0;
        // determine the number of feature per tree;
        if (this.max_features === "sqrt"){
            number_of_features_per_tree = Math.sqrt(this.feature_names.length) + 1;
        }else if (this.max_features === "log2"){
            number_of_features_per_tree = Math.log2(this.feature_names.length) + 1;
        }else{
            number_of_features_per_tree = this.feature_names.length-1;
        }

        // getting input for our decision tree model
        let [training, feature_names, feature_indexes] = this.get_input_for_decision_tree(
            bootstrap_data,
            Math.floor(number_of_features_per_tree),
        );

        if (verbose){
            console.log("feature names: ", feature_names);
            console.log("feature indexes: ", feature_indexes);
            console.log("training count: ", training.length);
        }

        return [training, feature_names, feature_indexes];
    }


    get_input_for_decision_tree(training, number_of_features_per_tree){
        // the actual number of feature on each decision tree
        let feature_count = this.feature_names.length - number_of_features_per_tree;

        // map each feature name to an index
        let cur_feature_indexes = [...Array(this.feature_names.length).keys()];
        let cur_feature_names = [...this.feature_names];

        for(let i = 0; i < feature_count; i++){
            // randomly select an index as the feature to remove
            let index = Math.floor(Math.random() * (training[0].length-1));
            // remove the column
            training = this.helper.removeColumn(training, index);

            // remove the feature name from the feature names array
            cur_feature_names.splice(index, 1);

            // remove the feature index from the feature index array
            cur_feature_indexes.splice(index,1)
        }

        // return training set, feature names in this training set, and feature indexes
        return [training, cur_feature_names, cur_feature_indexes]
    }


}

export {Bootstrap}
