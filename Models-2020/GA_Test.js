import {ArrayHelper} from "./array_helper.js"
import {DataFrame} from "./data_frame.js";
import {FastGeneticAlgorithm} from "./fast_genetic_algorithm.js";

let helper = new ArrayHelper();
const datasetConfig = JSON.parse(await Deno.readTextFile("config/config.json"));


/*
    Testing for GARF when RF is considered a population
 */

let RF_CONFIG = {
    n_estimators: 100,
    max_features: "sqrt",
    max_depth: 20,
    min_sample_split: 2,
    max_bootstrap_samples: 0.7,
    bootstrap_with_replacement: true
}

let GA_CONFIG = {
    selection_rate: 0.2,
    crossover_rate: 0.2,
    mutation_rate: 0.1,
    diversity_percentage: 0.2,
    crossover_type: "SINGLE",
    max_generation: 50,
    termination_condition: "NONE"
}


let dataset_names = [
    "breastCancer",
    "johnwise_cleaned"
]

for(let x = 1; x < 2; x++){
let dataset_name = dataset_names[x]

const columns_not_use = datasetConfig.columns_not_use;
const target_name = datasetConfig.target_name;
const directory1 = datasetConfig.mlBench_directory;
const directory2 = datasetConfig.dataset_directory;
const directory3 = datasetConfig.breast_cancer_dataset_directory
const directory = directory3 + dataset_name + "/";

let train = [];
let x_optimization = [];
let y_optimization = [];
let x_test = [];
let y_test = [];
let feature_names = [];

async function read(){
    const training_filenames = datasetConfig.training_filenames_large;
    const testing_filenames = datasetConfig.testing_filenames_large;
    const optimization_filenames = datasetConfig.optimization_filenames_large;

    for(let index = 0; index < 100; index++){
        let training_filename = training_filenames[index];
        let testing_filename = testing_filenames[index]
        let optimization_filename = optimization_filenames[index]

        let training_filePath = directory + training_filename;
        let optimization_filePath = directory + optimization_filename;
        let testing_filePath = directory + testing_filename;


        let training_df = new DataFrame(training_filePath, target_name, columns_not_use);
        let optimization_df = new DataFrame(optimization_filePath, target_name, columns_not_use);
        let testing_df = new DataFrame(testing_filePath, target_name, columns_not_use);

        await training_df.preprocess();
        await optimization_df.preprocess();
        await testing_df.preprocess();
        feature_names = training_df.featureNames;

        let training_set = helper.JSONtoArray(training_df.dataframe);
        let optimization_set = helper.JSONtoArray(optimization_df.dataframe);
        let testing_set = helper.JSONtoArray(testing_df.dataframe);

        let y_opt = helper.getColumn(optimization_set, feature_names.length-1);
        let x_opt = helper.removeColumn(optimization_set, feature_names.length-1);

        let y_testing = helper.getColumn(testing_set, feature_names.length-1);
        let x_testing = helper.removeColumn(testing_set, feature_names.length-1);

        train.push(training_set);
        x_test.push(x_testing);
        y_test.push(y_testing);
        x_optimization.push(x_opt);
        y_optimization.push(y_opt);
    }
}



function run(i,training_data, x_test, y_test, x_opt, y_opt, feature_names){
    let GA = new FastGeneticAlgorithm(GA_CONFIG, RF_CONFIG);
    // let accuracies = GA.start2(training_data, feature_names, x_opt, y_opt, x_test, y_test);
    // let end_accuracy = Math.max(...accuracies);
    // let end_index = [];
    // for(let j = 0; j < accuracies.length; j++){
    //     if (accuracies[j] === end_accuracy){
    //         end_index.push(j);
    //     }
    // }
    // console.log(i,",",accuracies[0],",",end_accuracy,",",end_index);
    GA.start(training_data, feature_names, x_opt, y_opt, x_test, y_test);
}


await read();
console.log("\ndataset name: ", dataset_name);
for(let i = 0; i < 1; i++){
    // console.log("\n========== Running", i, " ==========");
    let idx = i;
    let training_data = train[idx];
    let testing_data = x_test[idx];
    let testing_target = y_test[idx];
    let optimization_data = x_optimization[idx];
    let optimization_target = y_optimization[idx];
    run(i, training_data, testing_data, testing_target, optimization_data, optimization_target, feature_names);
}
console.log("========== Finished ==========\n");
}