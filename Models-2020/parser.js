class Parser {
    constructor(feature_names = []) {
        this.relations = [];
        this.nodes = [];
        this.feature_names = feature_names
        this.output_directory = "/Users/ericm/Documents/Github/GARF-Testing/Models/graphviz/";
    }

    clear(){
        this.relations = [];
        this.nodes = [];
    }

    async parseToFile(fileName, tree, ifFast = false){
        this.clear();
        let root = tree.root;
        // this.feature_names = tree.features_names;
        let text = ""

        if(ifFast){
            this.process_FAST_tree(root, 0);
            text += ("digraph FastDecisionTree { ")
        }else{
            this.process(root, 0);
            text += ("digraph DecisionTree { ")
        }


        for (let i = 0; i < this.nodes.length; i++) {
            text += (this.nodes[i])
        }

        for (let i = 0; i < this.relations.length; i++) {
            text += (this.relations[i]);
        }
        text+= ("}")

        await Deno.writeTextFile(this.output_directory+fileName+ ".dot", text);
        console.log("Parser: .dot Graph file written to "+ this.output_directory + fileName + ".dot");

        this.clear();
    }

    parse(tree, ifFast = false) {
        let root = tree.root;
        // this.feature_names = tree.features_names;

        if(ifFast){
            this.process_FAST_tree(root, 0);
            console.log("digraph FastDecisionTree { ")
        }else{
            this.process(root, 0);
            console.log("digraph DecisionTree { ")
        }

        for (let i = 0; i < this.nodes.length; i++) {
            console.log(this.nodes[i])
        }

        for (let i = 0; i < this.relations.length; i++) {
            console.log(this.relations[i]);
        }
        console.log("}\n")
        this.clear();
    }

    process(tree, nodeNumber) {
        if (typeof (tree) != 'object') {
            // reach the decision node and create a category node (TRUE OR FALSE)
            let node = nodeNumber + "[shape=box, style=filled, label=\"" + tree + "\"]";
            this.nodes.push(node);
            return (tree.index);
        }
        let node = nodeNumber + "[shape=box, style=filled, label=\" " + this.feature_names[tree.index] + "\n gini: " + tree.gini + "\"]";
        this.nodes.push(node);

        let seed1 = Math.random();
        let seed2 = Math.random();
        return [
            // getting child nodes on the left branch (yes)
            this.relations.push(nodeNumber + " -> " + [(nodeNumber + seed1) + "[label = \"value" + "<" + tree.value + "\n samples: " + tree.groups[0].length + "\"]"]),
            this.process(tree.left, nodeNumber + seed1),

            // getting child nodes on the right branch (no)
            this.relations.push(nodeNumber + " -> " + [(nodeNumber + seed2) + "[label = \"value" + ">" + tree.value + "\n samples: " + tree.groups[1].length + "\"]"]),
            this.process(tree.right, nodeNumber + seed2)
        ]
    }


    process_FAST_tree(tree, nodeNumber) {
        if (typeof (tree) != 'object') {
            // reach the decision node and create a category node (TRUE OR FALSE)
            let node = nodeNumber + "[shape=box, style=filled, label=\"" + tree + "\"]";
            this.nodes.push(node);
            return (tree.index);
        }
        let node = nodeNumber + "[shape=box, style=filled, label=\" " + this.feature_names[tree.index] + "\n gini: " + tree.gini + "\"]";
        this.nodes.push(node);

        let seed1 = Math.random();
        let seed2 = Math.random();

        return [
            // getting child nodes on the left branch (yes)
            this.relations.push(nodeNumber + " -> " + [(nodeNumber + seed1) + "[label = \"value" + "<" + tree.value + "\n groups: " + tree.groups[0] + "\"]"]),
            this.process_FAST_tree(tree.left, nodeNumber + seed1),

            // getting child nodes on the right branch (no)
            this.relations.push(nodeNumber + " -> " + [(nodeNumber + seed2) + "[label = \"value" + ">" + tree.value + "\n groups: " + tree.groups[1] + "\"]"]),
            this.process_FAST_tree(tree.right, nodeNumber + seed2)
        ]
    }
}


export {Parser}